﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllRef 
{
    public static Action getSphereParticle;
    public static ObjectPoolController ObjectPoolController_ref;
    public static Circuit_WayPoints Circuit_WayPoints_ref;
    public static List<GameObject> AllCircuitPool;
    
    public static  int a = 0;


    public GameObject getCircuitObj()
    {
        GameObject go;
        for(int i = 0; i < AllCircuitPool.Count; i++)
        {
            if (!AllCircuitPool[i].activeInHierarchy)
            {
               return AllCircuitPool[i];
                break;
            } 
        }
       
            return AllCircuitPool[0];
    }
}
