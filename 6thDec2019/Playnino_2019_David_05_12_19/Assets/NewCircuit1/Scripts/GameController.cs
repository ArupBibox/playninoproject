﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public bool startCircuit, endCircuit;
    public GameObject ObjectPoolController_Obj;
    public Circuit_WayPoints circuit_WayPoints_ref;
    [SerializeField]
    private ObjectPoolController ObjectPoolController_ref;
    public GameObject sphere1;
    circuitManager dataHandler;
    bool instantiated = false;
    public Transform startPos, targetImage;
    Battery batteryHandler;
    public bool lostTarget = false;
    public int projectNumber;


    public List<GameObject> AllCircuitPool;
    public GameObject getCircuitObj()
    {
        GameObject go;
        for (int i = 0; i < AllCircuitPool.Count; i++)
        {
            Debug.LogError(AllCircuitPool[i].activeInHierarchy);
            if (AllCircuitPool[i].activeInHierarchy==false)
            {
                return AllCircuitPool[i];

            }
        }

        return null;
    }
    public void disp()
    {
        for (int i = 0; i < AllCircuitPool.Count; i++)
        {
            Debug.LogError(AllCircuitPool[i].activeInHierarchy);
        }
    }
    public void setCircuitObj(GameObject go)
    {
        AllCircuitPool.Add(go);
    }
    //-----------------------





    // Start is called before the first frame update
    void Start()
    {
        AllPoolObjRef.GameController_ref = this;
        ObjectPoolController_ref = GetComponent<ObjectPoolController>();
        AllRef.Circuit_WayPoints_ref = this.circuit_WayPoints_ref;
        AllRef.ObjectPoolController_ref = ObjectPoolController_ref;
        dataHandler = FindObjectOfType<circuitManager>() as circuitManager;
        batteryHandler = FindObjectOfType<Battery>() as Battery;
        sphere1 = Resources.Load("Sphere 1") as GameObject;
        startCircuit = false;
        endCircuit = false;
        projectNumber = 1;
       // sphere1 = GameObject.FindGameObjectWithTag("Electron1");
    }

    void createElectrons()
    {

    }
   
    void Update()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            lostTarget = false;
        }
        else if (!DefaultTrackableEventHandler.IsTracked)
        {
            lostTarget = true;
        }
        if (startCircuit)
        {
            if (!instantiated)
            {
              //  if ((batteryHandler.Battery1.batteryVoltage > 0.78f) || (batteryHandler.Battery2.batteryVoltage > 0.78f))
                    //if any one of the battery has minimum Voltage
               // {
                    if ((batteryHandler.Battery1.batteryON) || (batteryHandler.Battery2.batteryON))// if Any one Battery is active
                    {
                   // Debug.LogError("---------------------------------------------->>>>>> once");
                   // circuitManager temp = ObjectPoolController_ref.getSphereParticle();
                   // temp.transform.SetParent(targetImage.transform);
                   // temp.isActiveonScene = true;
                   // Time.timeScale = .3f;
                   // Actions_Custom.a += 1;

                       GameObject go= Instantiate(sphere1, startPos.position, Quaternion.identity, targetImage.transform);
                  //  AllPoolObjRef.GameController_ref.setCircuitObj(go);
                        instantiated = true;
                    }
              //  }
            }
        }
        else if (endCircuit)
        {
            // ended in electron script
            instantiated = false;
            
        }
    }
   public void StartElectronFlow()
    {
        startCircuit = true;
        endCircuit = false;
    }
    public void EndElectronFlow()
    {
        if (instantiated)
        {
            startCircuit = false;
            endCircuit = true;
        }
    }
   
}
