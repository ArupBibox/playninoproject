﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class circuitManager : MonoBehaviour
{
    public bool isActiveonScene=false;
    public bool islife = false;
    public Battery Battery1;
    public Battery Battery2;
    public Transform[] target;
    public Transform newSpawnPoint;
    public float speed;
    public int current;
    Vector3 startPos;
    bool Spawned, SpawnElectron2 = false;
    public GameObject circuitTarget, sphere2;
    
    GameController dataHandler;
    // Start is called before the first frame update

        public bool isActiveOnScreen()
    {
        return isActiveonScene;
    }
    int total=0;
    public void setTo(bool val)
    {
        islife = val;
    }
    public bool getTo( )
    {
        return islife;
    }
    void Start()
    {
        startPos = gameObject.transform.position;
        dataHandler = FindObjectOfType<GameController>() as GameController;
        GameObject go = GameObject.FindGameObjectWithTag("battery1");
        if (go != null)
        {
            Battery1 = go.GetComponent<Battery>();
        }
         go = GameObject.FindGameObjectWithTag("battery2");
        if (go != null)
        {
            Battery2 = go.GetComponent<Battery>();
        }

        if (this.gameObject.tag == "Electron1")
        {
            target = new Transform[8];
            target = AllRef.Circuit_WayPoints_ref.Circuit1_WayPoints_1;
            /*
            target[0] = GameObject.Find("wp1").transform;
            target[1] = GameObject.Find("wp2").transform;
            target[2] = GameObject.Find("wp3").transform;
            target[3] = GameObject.Find("wp9").transform;
            target[4] = GameObject.Find("wp10").transform;
            target[5] = GameObject.Find("wp4").transform;
            target[6] = GameObject.Find("wp5").transform;
            target[7] = GameObject.Find("wp6").transform;

            */
            newSpawnPoint =GameObject.Find("newSpawn").transform;
            circuitTarget = GameObject.Find("CircuitComponents");
            // sphere2 = GameObject.FindGameObjectWithTag("Electron2");
            sphere2 = Resources.Load("sphere2") as GameObject;

        }
        else if(this.gameObject.tag == "Electron2")
 
            {
            target = new Transform[8];
            target = AllRef.Circuit_WayPoints_ref.Circuit1_WayPoints_2;
            /*
            target[0] = GameObject.Find("wp3").transform;
                target[1] = GameObject.Find("wp7").transform;
                target[2] = GameObject.Find("wp11").transform;
                target[3] = GameObject.Find("wp12").transform;
                target[4] = GameObject.Find("wp8").transform;
                target[5] = GameObject.Find("wp4").transform;
                target[6] = GameObject.Find("wp5").transform;
                target[7] = GameObject.Find("wp6").transform;
                */
                newSpawnPoint = GameObject.Find("newSpawn").transform;
                circuitTarget = GameObject.Find("CircuitComponents");
                //sphere2 = GameObject.FindGameObjectWithTag("Electron2");

            }

    }

    // Update is called once per frame
    void Update()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
    
            MoveThroughPoints();
            SpawnNewElectron();
           // gameObject.SetActive(false);
            DestroyElectrons();
            HideElectrons();
            if (dataHandler.endCircuit)
            {
                Destroy(gameObject);
            }
            
          

        }
    }
    void MoveThroughPoints()
    {
        if (transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else
        {
            current = current + 1 % target.Length;
        }
       
    }
    
    void SpawnNewElectron()
    {
        //if (getTo() == true)
       // {
       //     return;
       // }

        if (gameObject.tag == "Electron1")
        {
            if (!Spawned)
            {
             
                if (gameObject.transform.position.z > newSpawnPoint.transform.position.z)
                    {
                    // Debug.LogError("its calling");
                    // circuitManager tem = Actions_Custom.ObjectPoolController_ref.getSphereParticle();


                    // if(tem==null)
                    //  {

                    //   return;
                    //}
                    // tem.gameObject.SetActive(true);
                    // tem.transform.position = target[0].transform.position;
                    //  tem.transform.SetParent(circuitTarget.transform);
                    //  tem.isActiveonScene = true;
                    GameObject go;//= AllPoolObjRef.GameController_ref.getCircuitObj();
                   // Debug.LogError("eeeeee" + go);
                    //if (go!=null )
                   // {

                      //  Debug.LogError("not null");
                    //}
                   // else
                    {
                        GameObject sphereClone = Instantiate(gameObject, target[0].transform.position, Quaternion.identity, circuitTarget.transform) as GameObject;
                      //  AllPoolObjRef.GameController_ref.setCircuitObj(sphereClone);
                    }
                   
                    Spawned = true;
                    // islife = true;
                }
             }
            if (current == 3)
                {
                   if (!SpawnElectron2)
                    {
                        Debug.Log("crossed");
                    GameObject go ;//= AllPoolObjRef.GameController_ref.getCircuitObj();
                   // if (go == null)
                    {
                        go = Instantiate(sphere2, target[2].transform.position, Quaternion.identity, circuitTarget.transform);
                     //   AllPoolObjRef.GameController_ref.setCircuitObj(go);
                    }
                    SpawnElectron2 = true;


                }
               // islife = true;
                }
        }
    }
    void DestroyElectrons()
    {
        if (gameObject.tag == "Electron1")
        {
            if (current == target.Length)
            {
                if (Battery1.batteryON)
                {
                    Battery1.DrainBattery();
                }
                if (Battery2.batteryON)
                {
                    Battery2.DrainBattery();
                }

               // isActiveonScene = false;
                gameObject.SetActive(false);

                    Destroy(gameObject);
            }
            
        }
        if (gameObject.tag == "Electron2")
        {
            if (current == 6)
            {
                gameObject.SetActive(false);
                Destroy(gameObject);
            }
        }
    }
    void HideElectrons()
    {
        if (gameObject.tag == "Electron1")
        {
            if (current == 4)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            else if (current != 4)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
        }
        if (gameObject.tag == "Electron2")
        {
            if (current == 3)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            else if (current != 3)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
        }

    }
        
}
