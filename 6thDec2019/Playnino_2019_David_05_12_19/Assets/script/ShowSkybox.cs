﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ShowSkybox : MonoBehaviour
{

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(DefaultTrackableEventHandler.TrackImageName == "ShipTarget")
        {
            if (DefaultTrackableEventHandler.IsTracked)
                Camera.main.clearFlags = CameraClearFlags.Skybox;
            else
                Camera.main.clearFlags = CameraClearFlags.SolidColor;
        }
        else
        {
            Camera.main.clearFlags = CameraClearFlags.SolidColor;
        }
        
    }
}
