﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turnoff : MonoBehaviour {
    public GameObject sp,newpointLight;
    //public GameObject switchbuttonoff;
    public Material material1;
	void Start () {
        //switchbuttonoff.SetActive(false);
       // GameObject.Find("switchbuttonoff").GetComponent<MeshRenderer>().enabled = false;
        //GameObject.Find("switchbuttonoff").GetComponent<BoxCollider>().enabled = false;
    }

	void Update () {
        if (!newpointLight.active)
        {
            material1.DisableKeyword("_EMISSION");
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                /*if (hit.transform.name == "switchbuttonOn")
                {
                    GetComponent<BoxCollider>().enabled = false;
                    GameObject.Find("switchbuttonoff").GetComponent<MeshRenderer>().enabled = true;
                    GameObject.Find("switchbuttonoff").GetComponent<BoxCollider>().enabled = true;
                    //switchbuttonoff.SetActive(true);
                    GetComponent<MeshRenderer>().enabled = false;
                    sp.SetActive(true);
                    newpointLight.SetActive(true);
                    //light1.SetActive(true);
                    //light11.SetActive(true);
                }
                if (hit.transform.name == "switchbuttonoff")
                {
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    //switchbuttonoff.SetActive(true);
                    GetComponent<MeshRenderer>().enabled = true;
                    GetComponent<BoxCollider>().enabled = true;
                    sp.SetActive(false);
                    newpointLight.SetActive(false);
                    //light1.SetActive(false);
                    //light11.SetActive(false);
                }*/
                if (hit.collider.gameObject.name == "Capsule")
                {
                    if (newpointLight.GetComponent<Light>().intensity >= 10)
                    {
                        material1.EnableKeyword("_EMISSION");
                        newpointLight.GetComponent<Light>().intensity = 5;
                        
                    }
                    else
                    {
                        material1.DisableKeyword("_EMISSION");
                        newpointLight.GetComponent<Light>().intensity = 10;
                    }
                    //newpointLight.GetComponent<Light>().intensity -= 6;
                    //if (newpointLight.GetComponent<Light>().intensity<=0)
                    //{
                    //    newpointLight.GetComponent<Light>().intensity = 10;
                    //}
                }
            }
        }
	}
}
