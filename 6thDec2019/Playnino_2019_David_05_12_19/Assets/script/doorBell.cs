﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorBell : MonoBehaviour {
    Animator anim;
    public AudioClip playthis;
    AudioSource audsor;

 
    public GameObject spark;

    // Use this for initialization
    void Start () {

        audsor = this.gameObject.AddComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {


	}

    private void OnMouseDown()
    {
        audsor.clip = playthis;
        audsor.Play();
        anim.Play("belloff");
        spark.SetActive(true);
    }
    private void OnMouseUp()
    {
        anim.Play("idle");
        spark.SetActive(false);
    }
}
