﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour {
    public GameObject redLight;
    public GameObject yellowLight;
    public GameObject sp;

	void Start () {
        GameObject.Find("switchoff").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("switchoff").GetComponent<BoxCollider>().enabled = false;
    }
	
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if(Physics.Raycast(ray,out hit))
            {
                if (hit.transform.name == "switchon")
                {
                    GetComponent<BoxCollider>().enabled = false;
                    GameObject.Find("switchoff").GetComponent<MeshRenderer>().enabled = true;
                    GameObject.Find("switchoff").GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = false;
                    sp.SetActive(true);
                    redLight.SetActive(true);
                    yellowLight.SetActive(true);

                }
                if (hit.transform.name == "switchoff")
                {
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = true;
                    sp.SetActive(false);
                    redLight.SetActive(false);
                    yellowLight.SetActive(false);
                }
            }
        }
    }
}
