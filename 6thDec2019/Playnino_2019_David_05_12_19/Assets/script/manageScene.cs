﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class manageScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onclick(Text text) {

        if(text.text== "Power Up")
        {
            SceneManager.LoadScene("powerUp");
        }
        if(text.text=="Table Lamp")
        {
            SceneManager.LoadScene("TableLamp");
        }
        if(text.text== "Decorative lights")
        {
            SceneManager.LoadScene("DecorativeLights");
        }
        if(text.text=="Car Indicator")
        {
            SceneManager.LoadScene("carIndicator");
        }
        if(text.text=="Wire your Lab")
        {
            SceneManager.LoadScene("one");
        }
        if(text.text=="Night Lamp")
        {
            SceneManager.LoadScene("circuit2");
        }
        if(text.text=="Party Horn")
        {
            SceneManager.LoadScene("PartyHorn");
        }
        
    }
}
