﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchRInd : MonoBehaviour {

    public GameObject RightIndicator;
    public GameObject Rightspark;

    void Start()
    {

        //GameObject.Find("switchoffR").GetComponent<MeshRenderer>().enabled = false;
        //GameObject.Find("switchoffR").GetComponent<BoxCollider>().enabled = false;
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "switchonR")
                {
                    GetComponent<BoxCollider>().enabled = false;
                    GameObject.Find("switchoffR").GetComponent<MeshRenderer>().enabled = true;
                    GameObject.Find("switchoffR").GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = false;
                    RightIndicator.SetActive(true);
                    Rightspark.SetActive(true);
                }
                if (hit.transform.name == "switchoffR")
                {
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = true;
                    RightIndicator.SetActive(false);
                    Rightspark.SetActive(false);
                }
            }
        }
    }
}
