﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartHomeLighting : MonoBehaviour {

    public GameObject Sp1, Sp2, pointL1, pointL2,direct,windw;
    public GameObject spark;
    public bool day, night,goback;

    // Use this for initialization
    void Start () {
        day = night =false;
        windw.SetActive(false);
        

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (day)
        {
            direct.GetComponent<Light>().intensity = Mathf.Lerp(direct.GetComponent<Light>().intensity, 1, Time.deltaTime);
            if (direct.GetComponent<Light>().intensity > 0.9f)
            {
                Debug.Log("switch to 1");
                direct.GetComponent<Light>().intensity = 1;
                windw.SetActive(false);
                //Sp1.GetComponent<Light>().enabled = false;
                //Sp2.GetComponent<Light>().enabled = false;
                //pointL1.GetComponent<Light>().enabled = false;
                //pointL2.GetComponent<Light>().enabled = false;
                //Sp1.SetActive(false);
                //Sp2.SetActive(false);
                //pointL1.SetActive(false);
                //pointL2.SetActive(false);

            }
        }
        else if (night)
        {
            direct.GetComponent<Light>().intensity = Mathf.Lerp(direct.GetComponent<Light>().intensity, 0, Time.deltaTime);
            if (direct.GetComponent<Light>().intensity < 0.05f)
            {
                Debug.Log("switch to zero");
                direct.GetComponent<Light>().intensity = 0;
                windw.SetActive(true);
                //Sp1.GetComponent<Light>().enabled = true;
                //Sp2.GetComponent<Light>().enabled = true;
                //pointL1.GetComponent<Light>().enabled = true;
                //pointL2.GetComponent<Light>().enabled = true;
                //Sp1.SetActive(true);
                //Sp2.SetActive(true);
                //pointL1.SetActive(true);
                //pointL2.SetActive(true);

            }
        }
        
		
	}
    public void DayLight()
    {
        day = true;
        night = false;
        Debug.Log("daylight");
        /*Sp1.SetActive(false);
        Sp2.SetActive(false);
        pointL1.SetActive(false);
        pointL2.SetActive(false);
        direct.GetComponent<Light>().intensity = 1;*/
        spark.SetActive(false);
    }
    public void NightLight()
    {
        night = true;
        day = false;
        Debug.Log("nightlight");
        /*Sp1.SetActive(true);
        Sp2.SetActive(true);
        pointL1.SetActive(true);
        pointL2.SetActive(true);
        direct.GetComponent<Light>().intensity = 0;*/
        spark.SetActive(true);
       
    }
}
