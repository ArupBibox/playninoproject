﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour {

    public AudioClip playthis;
    AudioSource audsor;


   
    public GameObject sparkone;
    public GameObject sparktwo;
    public GameObject sparkthree;
    private float timer = 0;
    bool waiting = false;

    private float timerMax = 0;

    void Start()
    {
        audsor = this.gameObject.AddComponent<AudioSource>();
        GameObject.Find("switchoff").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("switchoff").GetComponent<BoxCollider>().enabled = false;
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "switchon")
                {
                    GetComponent<BoxCollider>().enabled = false;
                    GameObject.Find("switchoff").GetComponent<MeshRenderer>().enabled = true;
                    GameObject.Find("switchoff").GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = false;
                    audsor.clip = playthis;
                    audsor.Play();
                    sparkone.SetActive(true);
                    sparktwo.SetActive(true);
                    sparkthree.SetActive(false);
                }
                if (hit.transform.name == "switchoff")
                {
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = true;
                    audsor.volume = 1f;
                    if (audsor.isPlaying)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            callfade();
                            audsor.Play();
                            sparkone.SetActive(false);
                            sparktwo.SetActive(false);
                            sparkthree.SetActive(true);
                            StartCoroutine(waitingtoclose());
                        }
                    }
                    else
                    {
                        sparkone.SetActive(false);
                        sparktwo.SetActive(false);
                        sparkthree.SetActive(false);
                        audsor.Pause();
                    }
                        
                    
                    //callfade();
                    
                    
                    //audioo.Pause();
                    //Rightspark.SetActive(false);
                }
            }
        }
    }
    void callfade()
    {
        if (audsor.volume > 0.5)
        {
            audsor.volume=audsor.volume-0.3f;
        }
    }
    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }
    IEnumerator waitingtoclose()
    {
        yield return new WaitForSeconds(5);
        sparkthree.SetActive(false);

    }


}
