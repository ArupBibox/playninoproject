﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchLInd : MonoBehaviour {

    public GameObject LeftIndicator;
    public GameObject Leftspark;

	void Start () {

        //GameObject.Find("switchoffL").GetComponent<MeshRenderer>().enabled = false;
        //GameObject.Find("switchoffL").GetComponent<BoxCollider>().enabled = false;
    }
	
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "switchonL")
                {
                    GetComponent<BoxCollider>().enabled = false;
                    GameObject.Find("switchoffL").GetComponent<MeshRenderer>().enabled = true;
                    GameObject.Find("switchoffL").GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = false;
                    LeftIndicator.SetActive(true);
                    Leftspark.SetActive(true);
                }
                if (hit.transform.name == "switchoffL")
                {
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = true;
                    LeftIndicator.SetActive(false);
                    Leftspark.SetActive(false);
                }
            }
        }
    }
}
