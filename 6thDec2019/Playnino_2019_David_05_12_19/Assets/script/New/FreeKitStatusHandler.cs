﻿using UnityEngine;

public class FreeKitStatusHandler : MonoBehaviour
{
    public int brain, ship, plane, freeKitScore, points;

    //delegate
    public delegate void ChangeEvent(int score);
    public event ChangeEvent changeEvent;

    //For Script Access
    FBDataUpload fBDataUploadScriptAccess;
    ProfileHandler profileHandlerScriptAccess;
    ZohoCRM zohoCRMScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        fBDataUploadScriptAccess = FindObjectOfType<FBDataUpload>() as FBDataUpload;
        profileHandlerScriptAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        zohoCRMScriptAccess = FindObjectOfType<ZohoCRM>() as ZohoCRM;

        points = 5;
        
        FetchFromPlayerPref();

        //Delagate Subscribe start
        //TODO

        //changeEvent += fBDataUploadScriptAccess.UploadFreeKitScoreData;
    }

    // Update is called once per frame
    void Update()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            TargetCheck();
        }
        
    }

    void FetchFromPlayerPref()
    {
        brain = PlayerPrefs.GetInt("BrainScanned");
        ship = PlayerPrefs.GetInt("ShipScanned");
        plane = PlayerPrefs.GetInt("PlaneScanned");

        freeKitScore = PlayerPrefs.GetInt("FreeKitScore");
    }

    void TargetCheck()
    {
        if(DefaultTrackableEventHandler.TrackImageName == "BrainTarget" && PlayerPrefs.GetInt("BrainScanned") == 0)
        {
            PlayerPrefs.SetInt("BrainScanned", 1);
            PlayerPrefs.Save();
            fBDataUploadScriptAccess.UploadScannedImageData("Brain", 1);
            UpdateFreeKitScore();
            zohoCRMScriptAccess.UpdateLeadInCRM("'Brain':true");
        }
        else if (DefaultTrackableEventHandler.TrackImageName == "ShipTarget" && PlayerPrefs.GetInt("ShipScanned") == 0)
        {
            PlayerPrefs.SetInt("ShipScanned", 1);
            PlayerPrefs.Save();
            fBDataUploadScriptAccess.UploadScannedImageData("Ship", 1);
            UpdateFreeKitScore();
            zohoCRMScriptAccess.UpdateLeadInCRM("'Yacht':true");
        }
        else if (DefaultTrackableEventHandler.TrackImageName == "PlaneTarget" && PlayerPrefs.GetInt("PlaneScanned") == 0)
        {
            PlayerPrefs.SetInt("PlaneScanned", 1);
            PlayerPrefs.Save();
            fBDataUploadScriptAccess.UploadScannedImageData("Plane", 1);
            UpdateFreeKitScore();
            zohoCRMScriptAccess.UpdateLeadInCRM("'Plane':true");
        }

    }
    void UpdateFreeKitScore()
    {
        freeKitScore += points;
        PlayerPrefs.SetInt("FreeKitScore", freeKitScore);
        PlayerPrefs.Save();
        if (changeEvent != null)
            changeEvent(freeKitScore);

        //UpdateLevel();
    }
    void UpdateLevel()
    {
        if (PlayerPrefs.GetInt("BrainScanned") != 0 || PlayerPrefs.GetInt("ShipScanned") != 0 || PlayerPrefs.GetInt("PlaneScanned") != 0)
        {
            int tempLevel = PlayerPrefs.GetInt("currentUnlockedPoint");
            if (tempLevel <= 0)
            {
                PlayerPrefs.SetInt("currentUnlockedPoint", 1);
                PlayerPrefs.Save();
                profileHandlerScriptAccess.SaveLevelInDB();
            }
        }
    }
}
