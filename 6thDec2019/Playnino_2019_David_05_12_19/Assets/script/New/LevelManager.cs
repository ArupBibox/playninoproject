﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    //int kit1P1Ans, kit1P2Ans, kit1P3Ans, kit1P4Ans;
    public int freeKitFinished, kit1TotQues, kit1FinishedQues, tempKit1Score, currentLevl, point;
    public int[] kit1AnsArry;

    //Script Access
    InternetCheck internetCheckScriptAccess;

    private void Awake()
    {
        //Need to write this in awake because using var in another script start also..
        kit1TotQues = 15;
    }
    
    void Start()
    {
        internetCheckScriptAccess = FindObjectOfType<InternetCheck>() as InternetCheck;

        tempKit1Score = 0;
        point = 5;
        
        kit1AnsArry = new int[kit1TotQues + 1];

        internetCheckScriptAccess.CheckInternetConnection();
        if (!internetCheckScriptAccess.internetAvailable)
        {
            StartCalculateLvl();
        }

    }
    
    void Update()
    {
        
    }

    //Call when data retrieve complete from FB and internet = true..
    public void StartCalculateLvl()
    {
        //FreeKitLevel();
        Kit1Level();
    }

    void FreeKitLevel()
    {
        if(PlayerPrefs.GetInt("BrainScanned")!=0 || PlayerPrefs.GetInt("ShipScanned") != 0 || PlayerPrefs.GetInt("PlaneScanned") != 0)
        {
            freeKitFinished = 1;
        }
    }
    void Kit1Level()
    {
        for (int i = 1; i <= kit1TotQues; i++)
        {
            kit1AnsArry[i] = PlayerPrefs.GetInt("Kit1P" + i.ToString() + "Ans");
        }
        for (int i = 1; i <= kit1TotQues; i++)
        {
            if (kit1AnsArry[i] == 0)
            {
                kit1FinishedQues = i - 1;
                break;
            }
            else if(kit1AnsArry[i] == 1)
            {
                tempKit1Score += point;
            }

            //Finished ques 4 Check
            if(kit1AnsArry[i] == 1 || kit1AnsArry[i] == -1 && i == kit1TotQues)
            {
                kit1FinishedQues = i;
            }
        }
        if(kit1FinishedQues == kit1TotQues)
            PlayerPrefs.SetInt("answered", 1);

        PlayerPrefs.SetInt("Kit1Score", tempKit1Score);
        PlayerPrefs.Save();

        //CalculateCurrentLevel();
    }

    void CalculateCurrentLevel()
    {
        currentLevl = kit1FinishedQues;
        PlayerPrefs.SetInt("currentUnlockedPoint", currentLevl);
        PlayerPrefs.Save();
    }
}
