﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dummy : MonoBehaviour
{
    public static int value;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SceneChange(int sceneValue)
    {
        value = sceneValue;
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
    }
}
