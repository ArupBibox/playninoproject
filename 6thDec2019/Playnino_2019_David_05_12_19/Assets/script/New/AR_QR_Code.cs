﻿using System;
using System.Collections;
using System.Threading;

using UnityEngine;
using UnityEngine.UI;
using Vuforia;

using ZXing;

[AddComponentMenu("System/BarcodeScanner")]
public class AR_QR_Code : MonoBehaviour
{
    public Text OutputText;

    private bool cameraInitialized, mFormatRegistered;

    private BarcodeReader barCodeReader;

    private PIXEL_FORMAT mPixelFormat = PIXEL_FORMAT.UNKNOWN_FORMAT;

    public bool isQrCodeDetected;

    //Vuforia.Image cameraFeed;

    // Use this for initialization
    void Start()
    {
        //Initialize();
    }

    public void Initialize()
    {
#if UNITY_EDITOR
        mPixelFormat = PIXEL_FORMAT.GRAYSCALE; // Need Grayscale for Editor
#else
      mPixelFormat = PIXEL_FORMAT.RGB888; // Use RGB888 for mobile
#endif

        //Auto Focus
        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
        vuforia.RegisterOnPauseCallback(OnPaused);

        //Barcode reader instance
        barCodeReader = new BarcodeReader();
        StartCoroutine(InitializeCamera());
    }

    //Connect the camera with the barcode scanner
    private IEnumerator InitializeCamera()
    {
        // Waiting a little seem to avoid the Vuforia's crashes.
        yield return new WaitForSeconds(1f);

        // Try register camera image format
        if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
        {
            Debug.Log("Successfully registered pixel format " + mPixelFormat.ToString());
            mFormatRegistered = true;
            cameraInitialized = true;
        }
        else
        {
            Debug.LogError(
                "\nFailed to register pixel format: " + mPixelFormat.ToString() +
                "\nThe format may be unsupported by your device." +
                "\nConsider using a different pixel format.\n");
            mFormatRegistered = false;
        }

        //var isFrameFormatSet = CameraDevice.Instance.SetFrameFormat(PIXEL_FORMAT.RGB888, true);

        /*// Force autofocus.
        var isAutoFocus = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        if (!isAutoFocus)
        {
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
        }
        cameraInitialized = true;*/
    }



    #region Camera Format Handle

    private void OnVuforiaStarted()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnPaused(bool paused)
    {
        if (paused) // resumed
        {
            Debug.Log("App was paused");
            UnregisterFormat();
        }
        else
        {
            Debug.Log("App was resumed");
            RegisterFormat();

            // Set again autofocus mode when app is resumed
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
            Debug.Log("Focus Setted");
        }
    }
    /// <summary>
    /// Register the camera pixel format
    /// </summary>
    void RegisterFormat()
    {
        if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
        {
            Debug.Log("Successfully registered camera pixel format " + mPixelFormat.ToString());
            mFormatRegistered = true;

            cameraInitialized = true;
        }
        else
        {
            Debug.LogError("Failed to register camera pixel format " + mPixelFormat.ToString());
            mFormatRegistered = false;
        }
    }
    /// <summary>
    /// Unregister the camera pixel format (e.g. call this when app is paused)
    /// </summary>
    void UnregisterFormat()
    {
        Debug.Log("Unregistering camera pixel format " + mPixelFormat.ToString());
        CameraDevice.Instance.SetFrameFormat(mPixelFormat, false);
        mFormatRegistered = false;
        cameraInitialized = false;
    }

    #endregion

    // Update is called once per frame
    void Update()
    {
        if (cameraInitialized && !isQrCodeDetected)
        {
            if (mFormatRegistered)
            {
                try
                {
                    Debug.Log("trying");
                    Vuforia.Image cameraFeed = CameraDevice.Instance.GetCameraImage(mPixelFormat);
                    //var cameraFeed = CameraDevice.Instance.GetCameraImage(PIXEL_FORMAT.RGB888);
                    if (cameraFeed == null)
                    {
                        return;
                    }
                    var data = barCodeReader.Decode(cameraFeed.Pixels, cameraFeed.BufferWidth, cameraFeed.BufferHeight, RGBLuminanceSource.BitmapFormat.RGB24);
                    if (data != null)
                    {
                        // QRCode detected.
                        OutputText.text = data.Text;
                        isQrCodeDetected = true;
                        Debug.Log("Data" + "" + data.Text);
                        data = null;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError(e.Message, this);
                }
            }
            else
                RegisterFormat();

        }
        else if (!cameraInitialized && !mFormatRegistered)
        {
            Debug.Log("Failed");
        }
    }

    void OnTrackablesUpdated()
    {

    }

    public void ScanAgain()
    {
        if (cameraInitialized && isQrCodeDetected)
        {
            OutputText.text = "";
            isQrCodeDetected = false;
        }
    }
}
