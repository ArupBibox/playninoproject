﻿using System.IO;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObbExtractor : MonoBehaviour
{

    PopUpHandler popUpHandlerScriptAccess;
    KitsHandler kitsHandlerScriptAccess;
    void Start()
    {
        popUpHandlerScriptAccess = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        kitsHandlerScriptAccess = FindObjectOfType<KitsHandler>() as KitsHandler;

        StartCoroutine(ExtractObbDatasets());
    }

    private IEnumerator ExtractObbDatasets()
    {
        string[] filesInOBB = { "TestDataBase.dat", "TestDataBase.xml", "Circuits.dat", "Circuits.xml" };
        foreach (var filename in filesInOBB)
        {
            string uri = Application.streamingAssetsPath + "/Vuforia/" + filename;

            string outputFilePath = Application.persistentDataPath + "/Vuforia/" + filename;
            if (!Directory.Exists(Path.GetDirectoryName(outputFilePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(outputFilePath));

            
            var www = new WWW(uri);
            yield return www;

            Save(www, outputFilePath);
            yield return new WaitForEndOfFrame();
        }

        // When done extracting the datasets, Start Vuforia AR scene
        //SceneManager.LoadScene("0-Splash");
        if(kitsHandlerScriptAccess.bttnClick == 1)
        {
            kitsHandlerScriptAccess.ChangeScene(2);
        }
        else
            popUpHandlerScriptAccess.ChangeScene(2);
    }

    private void Save(WWW w, string outputPath)
    {
        File.WriteAllBytes(outputPath, w.bytes);

        // Verify that the File has been actually stored
        if (File.Exists(outputPath))
        {
            Debug.Log("File successfully saved at: " + outputPath);
        }
        else
        {
            Debug.Log("Failure!! - File does not exist at: " + outputPath);
        }
    }
}
