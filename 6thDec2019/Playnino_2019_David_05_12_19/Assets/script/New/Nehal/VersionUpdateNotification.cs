﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using TMPro;
using System;

public class VersionUpdateNotification : MonoBehaviour
{

    private string appCurrentVersion;
    private string appVersionFromDB;
    private string forceNotify;
    public bool isAppUpdated, versionFetched, haveOldVersion;

    public GameObject notificationInAboutPanel,forceUpdateNotification;
    public TMP_Text currentAppVersionText,messageAboutNewVersion,forceUpdateMessage;
    public Toggle showForceNotification;

    ProfileHandler profileHandlerScriptAccess;
   
    // Start is called before the first frame update
    void Start()
    {
        profileHandlerScriptAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;

        appCurrentVersion = Application.version;

        if (PlayerPrefs.GetString("SavedAppVersion") != appCurrentVersion)
        {
            isAppUpdated = true;
            AppUpdated();
        }

        Debug.Log(appCurrentVersion);
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");

        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        FirebaseDatabase.DefaultInstance.GetReference("UpdateNotification").ValueChanged += HandleValueChanged;
        notificationInAboutPanel.SetActive(false);
        forceUpdateNotification.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (versionFetched)
        {
            //if new version avaiable show in about panel
            if (haveOldVersion)
            {
                notificationInAboutPanel.SetActive(true);
                messageAboutNewVersion.text = "New version Available v" + appVersionFromDB;
            }
            else
            {
                notificationInAboutPanel.SetActive(false);
            }
        }
        currentAppVersionText.text = "v"+appCurrentVersion;//this always shows under about us panel
        
    }

    public void CheckForceUpdate()
    {
        Debug.Log("Checking for update!");
        if (versionFetched)
        {
            if (forceNotify == "True" && haveOldVersion)
            {
                string temp = PlayerPrefs.GetString("Date");
                string date = System.DateTime.Now.Date.ToString();
                Debug.Log(temp);
                if (temp == "" || temp != date)
                {
                    Debug.Log("inside");
                    showForceNotification.isOn = false;
                    forceUpdateNotification.SetActive(true);
                }
                forceUpdateMessage.text = "New version Available \nv" + appVersionFromDB + ".\nPlease Update!";
            }
            else
            {
                forceUpdateNotification.SetActive(false);
            }
        }
        else
        {
            Invoke("CheckForceUpdate", 2f);
        }
        
    }

    void HandleValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        else
        {
            appVersionFromDB = args.Snapshot.Child("versionNum").Value.ToString();
            forceNotify = args.Snapshot.Child("forceNotify").Value.ToString();
            versionFetched = true;

            CompareVersion();
        }
    }

    void CompareVersion()
    {
        var V1 = new Version(appVersionFromDB);
        var V2 = new Version(appCurrentVersion);

        var result = V1.CompareTo(V2);

        if (result > 0)
        {
            Debug.Log("version1 is greater");
            haveOldVersion = true;
        }
        else if (result < 0)
        {
            Debug.Log("version2 is greater");
        }
        else
        {
            Debug.Log("versions are equal");
            haveOldVersion = false;
        }
    }

    public void CloseButtonClick()
    {
        if(showForceNotification.isOn)
        {
            PlayerPrefs.SetString("Date", System.DateTime.Now.Date.ToString());
            PlayerPrefs.Save();
        }
        
        //string temp = PlayerPrefs.GetString("Date");
        
    }

    void AppUpdated()
    {
        if (isAppUpdated)
        {
            //App version Set
            PlayerPrefs.SetString("SavedAppVersion", Application.version);
            PlayerPrefs.Save();

            UpdateInDB();

            isAppUpdated = false;
        }
        profileHandlerScriptAccess.fetchUserDetails = true;
        profileHandlerScriptAccess.FetchUserDetails();
    }

    void UpdateInDB()
    {
        if (profileHandlerScriptAccess.gotParentKey)
        {
            DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
            mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("AppVersion")
                .Child("version").SetValueAsync(PlayerPrefs.GetString("SavedAppVersion"));
            mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("AppVersion")
                .Child("date").SetValueAsync(DateTime.Now.Date.ToString());
        }
        else
        {
            Invoke("UpdateInDB", 2f);
        }
    }
}
