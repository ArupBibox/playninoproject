﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class swipe : MonoBehaviour,IPointerUpHandler,IBeginDragHandler
{


    bool isSwiping = false;
    Vector2 lastPosition;
    public int SwipeDirection=0;//0=not swipe 1=up -1=down
    [Range(0.1f,2)]
    public float SwipeSmoothValue=1;
    Vector2 direction;

    public Image bottomUi,topUi;
    public ScrollRect scroll;

    public float waitTime;
    float uiScaleAlongY, uiScaleAlongY2;
    bool fingerDown;
    float time,time2;
    bool timeLimitReached, timeLimitReached2;
    public int counter = 0;
    public float verValue;
    // Start is called before the first frame update
    void Start()
    {
        //imageHolder.transform.localPosition = new Vector3(0, 452, 0);
        uiScaleAlongY = 0;
        time = 0;
    }


    // Update is called once per frame
    void Update()
    {
        verValue = scroll.verticalNormalizedPosition;
        if(Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log(Mathf.Clamp01(12.01f));
        }
       

        #region detectSwipe
        if (Input.touchCount > 0)
            fingerDown = true;
        else
            fingerDown = false;
        if (Input.touchCount != 0)
        {
            
            if (Input.GetTouch(0).deltaPosition.sqrMagnitude != 0)
            {
                if (isSwiping == false)
                {
                    isSwiping = true;
                    lastPosition = Input.GetTouch(0).position;
                    return;
                }
                else
                {
                    direction = Input.GetTouch(0).position - lastPosition;

                    if (direction.y > 0)
                    {

                        //Debug.Log("UP!");
                        SwipeDirection = 1;
                    }
                    else
                    {
                        //Debug.Log("DOWN!");
                        SwipeDirection = -1;
                    }


                }

            }
            else
            {
                isSwiping = false;
                SwipeDirection = 0;
            }
        }



        #endregion
        
       #region Growing UI Down
       float amount = direction.magnitude;
       float alpha;
       if(SwipeDirection==1 &&fingerDown &&!timeLimitReached)
       {

               Debug.Log("upup!");
               if (uiScaleAlongY < 1)
               {
                   uiScaleAlongY += Time.deltaTime * SwipeSmoothValue * (amount);
                   //Debug.Log(uiScaleAlongY);
               }
               else
                   uiScaleAlongY = 1;
           
       }
       else
       {
           if (uiScaleAlongY > 0)
               uiScaleAlongY -= Time.deltaTime * 0.95f;
           else
               uiScaleAlongY = 0;
       }
       uiScaleAlongY = Mathf.Clamp01(uiScaleAlongY);
       bottomUi.rectTransform.localScale = new Vector3(1, uiScaleAlongY, 1);
       Color tempColor = bottomUi.color;
       alpha = uiScaleAlongY - 0.25f;
       tempColor.a = alpha;
       bottomUi.color= tempColor;
       if(timeLimitReached)
       {
           time += Time.deltaTime;
           if(time==waitTime)
           {
               timeLimitReached = false;
               time = 0;
           }
       }
       #endregion

       #region Growing UI Up
       float amount2 = direction.magnitude;
       float alpha2;
       if (scroll.verticalNormalizedPosition >= 1 && SwipeDirection == -1 && fingerDown && !timeLimitReached2)
       {
           Debug.Log("downdown!");
           if (uiScaleAlongY2 < 1)
           {
               uiScaleAlongY2 += Time.deltaTime * SwipeSmoothValue * (amount);
             //  Debug.Log(uiScaleAlongY2);
           }
           else
               uiScaleAlongY2 = 1;
       }
       else
       {
           if (uiScaleAlongY2 > 0)
               uiScaleAlongY2 -= Time.deltaTime * 0.95f;
           else
               uiScaleAlongY2 = 0;
       }
       uiScaleAlongY2 = Mathf.Clamp01(uiScaleAlongY2);

       topUi.rectTransform.localScale = new Vector3(1, uiScaleAlongY2, 1);
       Color tempColor2 = bottomUi.color;
       alpha2 = uiScaleAlongY2 - 0.25f;
       tempColor2.a = alpha2;
       topUi.color = tempColor2;
       if (timeLimitReached2)
       {
           time2 += Time.deltaTime;
           if (time2 == waitTime)
           {
               timeLimitReached2 = false;
               time2 = 0;
           }
       }

       #endregion


   
       


    }
    

    public void OnPointerUp(PointerEventData eventData)
    {
     
        
    }
    public void OnBeginDrag(PointerEventData pointerData)
    {
        Debug.Log("I am Dragging!");
        
    }
}
