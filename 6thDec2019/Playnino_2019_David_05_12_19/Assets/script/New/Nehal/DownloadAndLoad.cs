﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using Vuforia;

public class DownloadAndLoad : MonoBehaviour
{
    string url;
    string folderName = "AppData";
    string fileName = "sceneBundle";
    public string nameOfScene; 
    string path;
    UnityWebRequest www;
    bool isDownloading = false;
    bool isTimerOn=false;
    float time = 0;
    bool sceneFound = false;
    string[] scenePaths;
    int loaded;
    public UnityEngine.UI.Image progressBar;
    public AssetBundle myLoadedAssetBundle;

    public TMP_Text downloadStatusText;
    public GameObject tryAgainButton;

    //ScriptAccess
    //SimpleDemo simpleDemoScriptAccess;
    //public Slider downloadPercentage;
    void Start()
    {
        //UnloadBundle();
        // simpleDemoScriptAccess = FindObjectOfType<SimpleDemo>() as SimpleDemo;

        nameOfScene = "StarterKit1_v1";
       url = "https://www.dropbox.com/s/56jgfbfw29urr1q/kitone.one?dl=1";
        // LoadSceneFromBundle(nameOfScene);
        path = Path.Combine(Application.persistentDataPath, folderName);
        path = Path.Combine(path, fileName + ".unity3d");
        Debug.Log(path);
        if (!File.Exists(path))
        {
            DownloadBundle();
        }
        else
        {
            LoadSceneFromBundle(nameOfScene);
        }

        loaded = 0;
       



       
        if (tryAgainButton != null)
            tryAgainButton.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (isTimerOn)
            time += Time.deltaTime;
        if (isDownloading)///we get download progress here
        {
            // Debug.Log(www.downloadProgress);
            //downloadPercentage.value = www.downloadProgress;
            if(progressBar!=null)
                    progressBar.fillAmount = www.downloadProgress;
        }
           
       
    }
    #region Download File From Server
    public void DownloadBundle()
    {
        downloadStatusText.text = "Downloading...";

        if (tryAgainButton != null)
            tryAgainButton.SetActive(false);

        isTimerOn = true;
        
        StartCoroutine(download());

    }
    private IEnumerator download()
    {
        www= UnityWebRequest.Get(url);
        isDownloading = true;
        DownloadHandler handle = www.downloadHandler;

        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log("Network Error");

                //networkErrorPanel.SetActive(true);
                if(downloadStatusText!=null)
                    downloadStatusText.text = "Network Error!";
                if (tryAgainButton != null)
                    tryAgainButton.SetActive(true);
            

            //try again pop with button
        }
        else
        {
            path = Path.Combine(Application.persistentDataPath, folderName);
            path = Path.Combine(path, fileName + ".unity3d");
            save(handle.data, path);


        }

        void save(byte[] data,string path)
        {
            isTimerOn = false;

            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }

  
                File.WriteAllBytes(path, data);

            Debug.Log("Data Stored!");
            if (downloadStatusText != null)
                downloadStatusText.text = "Downloaded";
            //tryAgainButton.SetActive(false);
            //kitOneMainScript.ToArScene();
            
            
            LoadSceneFromBundle(nameOfScene);
                
            
        }
    }
    #endregion


    #region Load Scene From Bundle
    public void LoadSceneFromBundle(string sceneName)
    {
        nameOfScene = sceneName;
        AssetBundle[] bundles = Resources.FindObjectsOfTypeAll<AssetBundle>();
        if (bundles.Length <= 1)
        {
            loaded = 0;
        }
        else
        {
            loaded = 1;
        }
        StartCoroutine(loadscene());
        

    }


    //load functions 
    public IEnumerator loadscene()
    {
        if(loaded==0)
        {
            AssetBundleCreateRequest bundle = null;

            path = Path.Combine(Application.persistentDataPath, folderName);
            path = Path.Combine(path, fileName + ".unity3d");

            bundle = AssetBundle.LoadFromFileAsync(path);
            
            yield return bundle;


            myLoadedAssetBundle = bundle.assetBundle;
            if (myLoadedAssetBundle == null)
            {
                DownloadBundle();
                yield break;
            }


            scenePaths = myLoadedAssetBundle.GetAllScenePaths();
            for (int i = 0; i < scenePaths.Length; i++)
            {
                string sceneName = Path.GetFileNameWithoutExtension(scenePaths[i]);

                if (nameOfScene == sceneName)
                {
                    Debug.Log(sceneName);
                    SceneManager.UnloadSceneAsync("DownloadScene");
                    VuforiaRuntime.Instance.InitVuforia();
                    Screen.orientation = ScreenOrientation.Landscape;
                    KitsHandler.circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);
                   
                    SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
                    /*if(myLoadedAssetBundle!=null)
                    {
                        myLoadedAssetBundle.Unload(false);

                    }*/
                    /*if (SceneManager.GetSceneByName("qrCodeScanner").isLoaded)
                    {
                        simpleDemoScriptAccess.ChangeScene();
                        // SceneManager.UnloadSceneAsync("qrCodeScanner");
                    }*/

                    sceneFound = true;
                }

            }
            if (!sceneFound)///when there is no scene in the name provided
            {
                DownloadBundle();
            }
            loaded = 1;
        }
        else
        {
            AssetBundle[] bundles = Resources.FindObjectsOfTypeAll<AssetBundle>();
            scenePaths = bundles[1].GetAllScenePaths();
            for (int i = 0; i < scenePaths.Length; i++)
            {
                string sceneName = Path.GetFileNameWithoutExtension(scenePaths[i]);
                Debug.Log(sceneName + "namwe");
                if (nameOfScene == sceneName)
                {
                    SceneManager.UnloadSceneAsync("DownloadScene");
                    //SceneManager.UnloadSceneAsync("AssestsBundleDownload");
                    VuforiaRuntime.Instance.InitVuforia();
                    Screen.orientation = ScreenOrientation.Landscape;
                    KitsHandler.circuitNumber= Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);
                    SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
                    /*if (myLoadedAssetBundle != null)
                    {
                        myLoadedAssetBundle.Unload(false);
                    }*/
                    sceneFound = true;

                }

            }
            if (!sceneFound)///when there is no scene in the name provided
            {

                DownloadBundle();
            }
        }
            
           
       
        
    }

    public void UnloadBundle()
    {
        /*if(myLoadedAssetBundle!=null)
                myLoadedAssetBundle.Unload(false);*/
        AssetBundle.UnloadAllAssetBundles(true);
        //myLoadedAssetBundle = null;
    }
    public void UnloadAllLoadedBundles()
    {
        //AssetBundle bundle = AssetBundle.GetAllLoadedAssetBundles();
    }
    #endregion
    public void TryAgainButtonClick()
    {
        if (tryAgainButton != null)
            tryAgainButton.SetActive(false);
        progressBar.fillAmount = 0;
        DownloadBundle();
    }





}

