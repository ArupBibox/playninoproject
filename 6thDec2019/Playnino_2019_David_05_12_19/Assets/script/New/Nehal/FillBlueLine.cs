﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class FillBlueLine : MonoBehaviour, IBeginDragHandler
{
    public Image blueImage;
    public float[] checkPoints;
    public bool toFill,canceled,grow;
    public float growAmount;
    public ScrollRect scrollRectComponent;
    public int currentPoint;

    // Start is called before the first frame update
    void Start()
    {
        toFill = false;
        grow = false;
        canceled = false;
    }

    // Update is called once per frame
    void Update()
    {
        currentPoint = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint"), 0, checkPoints.Length-1);
        if (blueImage.fillAmount >= checkPoints[currentPoint])
        {
            grow = false;
        }
        if (grow)
        {
            if (currentPoint != 0)
            {
                if (blueImage.fillAmount <= checkPoints[currentPoint])
                {
                    //canceled = false;
                    toFill = true;
                }
            }
            else
            {
                if (blueImage.fillAmount <= checkPoints[0])
                {
                    toFill = true;
                    canceled = true;
                }
            }
            if (toFill)
            {
                blueImage.fillAmount += growAmount * Time.deltaTime;
                if (!canceled)
                {
                    scrollRectComponent.verticalNormalizedPosition = blueImage.fillAmount;
                }
            }
           
        }
        
    }
   

    public void OnBeginDrag(PointerEventData _EventData)
    {
        Debug.Log("OnBeginDrag");
        canceled = true;
    }
}
