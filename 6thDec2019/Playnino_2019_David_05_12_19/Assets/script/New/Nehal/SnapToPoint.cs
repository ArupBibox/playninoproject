﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
public class SnapToPoint : MonoBehaviour,IBeginDragHandler,IPointerUpHandler,IPointerDownHandler,IEndDragHandler,IDragHandler
{
    
    public ScrollRect scrollRectComponent;
    public bool isPointerDown,isDragging;
    public Transform[] points;
    public float[] distance;//this has to be changed during runtime
    public float minimumDistance,smoothValue;
    public float[] verValue;
    public Transform snapPoint;
    public GameObject[] popUp;
    public float hidePopupOffset;
    public int currentPoint;
    public SlidingPanel slidingPanelScript;
    public swipe swipeScript;
    public bool isLock;
    public Vector2 velocity;
    public float stopingVelocityUp, stopingVelocityDown;
    public int currentUnlockedPoint;
    // Start is called before the first frame update
    void Start()
    {
        slidingPanelScript = FindObjectOfType<SlidingPanel>() as SlidingPanel;
        scrollRectComponent.verticalNormalizedPosition =0;
       
        for (int i=0;i<popUp.Length;i++)
        {
            popUp[i].SetActive(false);

        }
        swipeScript = FindObjectOfType<swipe>() as swipe;
        isLock = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        velocity = scrollRectComponent.velocity;
        if (currentUnlockedPoint != PlayerPrefs.GetInt("currentUnlockedPoint"))
            CloseAllPopups();
        currentUnlockedPoint = PlayerPrefs.GetInt("currentUnlockedPoint");

        if (currentUnlockedPoint==0)
        {
            if (slidingPanelScript.isReachedExp && !isLock)
            {
                if (velocity.y < stopingVelocityUp)
                {
                    FindNearestPointToSnap();
                }
                else if (velocity.y > stopingVelocityDown)
                {
                    FindNearestPointToSnap();
                }
            }
        }
        if(currentUnlockedPoint==1)
        {
            if (slidingPanelScript.isKit1Reached && !isLock)
            {
                if (velocity.y < stopingVelocityUp)
                {
                    FindNearestPointToSnap();
                }
                else if (velocity.y > stopingVelocityDown)
                {
                    FindNearestPointToSnap();
                }
            }
        }
        
        
    }
    void FindNearestPointToSnap()
    {
        if (!isDragging && !isPointerDown)
        {
            for (int i = 0; i < points.Length; i++)
            {
                distance[i] = Mathf.Abs(points[i].transform.position.y - snapPoint.transform.position.y);
            }
            minimumDistance = Mathf.Min(distance);
            for (int i = 0; i < points.Length; i++)
            {
                if (minimumDistance == distance[i])
                {

                    scrollRectComponent.verticalNormalizedPosition = Mathf.Lerp(scrollRectComponent.verticalNormalizedPosition, verValue[i], smoothValue * Time.deltaTime);
                    if (slidingPanelScript.isReachedExp)
                    {
                        //popUp[currentPoint].SetActive(false);
                        currentPoint = i;
                        for (int j = 0; j < popUp.Length; j++)
                        {
                            if(currentPoint!=j)
                            {
                                popUp[j].SetActive(false);

                            }
                            else
                            {
                                popUp[currentPoint].SetActive(true);//show popup only after reaching that point
                            }

                        }



                    }

                }
                
            }

        }

        
    }



    public void OnBeginDrag(PointerEventData eventData)
    {
        isDragging = true;
       //popUp[currentPoint].SetActive(false);

    }


    void SetDraggingFalse()
    {
        
        isDragging = false;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDown = false;
        Invoke("SetDraggingFalse", 0.2f);
        if (slidingPanelScript.isReachedExp)
        {
            
        if (scrollRectComponent.verticalNormalizedPosition < 0.01f)
            {
                isLock = true;
            }
            else
            {
                isLock = false;
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPointerDown = true;
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Invoke("SetDraggingFalse", 0.2f);

    }

    public void OnDrag(PointerEventData eventData)
    {
        
        float distanceFromSnapPoint = points[currentPoint].transform.position.y - snapPoint.transform.position.y;

        if(swipeScript.SwipeDirection==-1 && distanceFromSnapPoint<-hidePopupOffset)
        {

            popUp[currentPoint].SetActive(false);
        }
        else if(swipeScript.SwipeDirection == 1 && distanceFromSnapPoint > hidePopupOffset)
        {
            popUp[currentPoint].SetActive(false);

        }
    }
    
    public void CloseAllPopups()
    {
        for (int i = 0; i < popUp.Length; i++)
        {
            popUp[i].SetActive(false);

        }
    }

}
