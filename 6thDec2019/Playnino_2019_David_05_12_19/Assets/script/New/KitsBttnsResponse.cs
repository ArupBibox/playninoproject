﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KitsBttnsResponse : MonoBehaviour {

    GameObject[] starterKitChildBttns;
    public GameObject advancedKitBttn, computerKitBttn, scanQrPopUpPanel;
    bool isExpanded;
    //public QrScan QrScanScriptAccess;
    //screenSlideConrollr SlideScriptAccess;

    public GameObject arBttn, enableArBttn;


    void Start () {

        scanQrPopUpPanel.SetActive(false);

        starterKitChildBttns = GameObject.FindGameObjectsWithTag("StarterKitChildBttns");
        //Disable the gameobjects 
        for(int i =0; i < starterKitChildBttns.Length; i++)
        {
            starterKitChildBttns[i].SetActive(false);
        }
        isExpanded = false;
	}
	
	void Update () {
        /*if (SimpleDemo.codeVerified)
        {
            enableArBttn.SetActive(false);
            arBttn.SetActive(true);
        }
        else
        {
            arBttn.SetActive(false);
            enableArBttn.SetActive(true);
        }*/
	}

    public void StarterKitBttnClick()
    {
        if (!isExpanded)
        {
            advancedKitBttn.SetActive(false);
            computerKitBttn.SetActive(false);

            for (int i = 0; i < starterKitChildBttns.Length; i++)
            {
                starterKitChildBttns[i].SetActive(true);
            }
            isExpanded = true;
        }
        else
        {
            for (int i = 0; i < starterKitChildBttns.Length; i++)
            {
                starterKitChildBttns[i].SetActive(false);
            }

            advancedKitBttn.SetActive(true);
            computerKitBttn.SetActive(true);
            isExpanded = false;
        }
        
    }

    public void EnableArBttnClick()
    {
        scanQrPopUpPanel.SetActive(true);
    }
    public void ScanQrCodeBttnClick()
    {
        scanQrPopUpPanel.SetActive(false);

        //Open Qr Code Scan Scene

        SceneManager.LoadScene("qrCodeScanner", LoadSceneMode.Additive);

        //QrScanScriptAccess.StartQrProcess();

    }
    public void ScanQrCodeCloseBttnClick()
    {
        scanQrPopUpPanel.SetActive(false);
    }
    
    public void AdvancedKitBttnClick()
    {

    }
    public void ComputerKitBttnClick()
    {

    }
}
