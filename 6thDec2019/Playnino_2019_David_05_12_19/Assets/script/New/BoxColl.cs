﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxColl : MonoBehaviour
{
    public BoxCollider2D bc;
    public SlidingPanel slidingPanelScriptAccess;
    public Sprite blueCircle;
    public GameObject demoPoint, kit1;
    //public Button demoBttn;
    public Image imageBlue;
    //SnapToPoint snapToPointScript;
    ProfileHandler profileHandlerScript;
    bool flag=false;
    float Value=0;
    int pointNow;
    //ScrollRect scrollRect;

    // Start is called before the first frame update
    void Start()
    {
        bc = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;
        //snapToPointScript = FindObjectOfType<SnapToPoint>() as SnapToPoint;
        slidingPanelScriptAccess = FindObjectOfType<SlidingPanel>() as SlidingPanel;
        profileHandlerScript = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        //scrollRect = snapToPointScript.scrollRectComponent;
    }

    // Update is called once per frame
    void Update()
    {
        if (flag)
        {
            Value += Time.deltaTime;
            imageBlue.fillAmount = Value;
        }
        if (Value >= 1 && flag)
        {
            //demoBttn.interactable = true;
            flag = false;
        }
       

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        int currentPoint = PlayerPrefs.GetInt("currentUnlockedPoint");
        Debug.Log("GameObject1 collided with " + col.name);
        if(col.name == "DemoPoint" &&currentPoint==0)
        {
            
            slidingPanelScriptAccess.moveBlueImg = false;
            //demoPoint.GetComponent<Image>().sprite = blueCircle;
            //demoPoint.GetComponent<RectTransform>().sizeDelta = new Vector2(70, 70);
            //changeColor();
            slidingPanelScriptAccess.isReachedExp = true;
        }
        else if(col.name== "Kit1" &&currentPoint==1)
        {

            slidingPanelScriptAccess.moveBlueImg = false;

            //kit1.GetComponent<Image>().sprite = blueCircle;
            //kit1.GetComponent<RectTransform>().sizeDelta = new Vector2(70, 70);
            //changeColor();
            /*if (!slidingPanelScriptAccess.isKit1Reached)
            {
                ///scrollRect.verticalNormalizedPosition = Mathf.Lerp(scrollRect.verticalNormalizedPosition, snapToPointScript.verValue[1], Time.deltaTime * 20f);
                scrollRect.verticalNormalizedPosition = snapToPointScript.verValue[1];
            }*/
               // scrollRect.verticalNormalizedPosition = snapToPointScript.verValue[1];
                    //scrollRect.verticalNormalizedPosition = Mathf.Lerp(scrollRect.verticalNormalizedPosition,snapToPointScript.verValue[1],Time.deltaTime*20f);
            //PlayerPrefs.SetInt("currentUnlockedPoint", 1);
            //PlayerPrefs.Save();
            slidingPanelScriptAccess.isKit1Reached = true;
            profileHandlerScript.SaveLevelInDB();
        }

        else if(col.name== "Project1" &&currentPoint==2)
        {
           
            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.project1Reached = true;
           // PlayerPrefs.SetInt("currentUnlockedPoint", 2);
            //PlayerPrefs.Save();
        }
        else if (col.name == "Project2" &&currentPoint==3)
        {
           
            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.project2Reached = true;
            //PlayerPrefs.SetInt("currentUnlockedPoint", 3);
           //PlayerPrefs.Save();
        }
        else if (col.name == "Project3"&&currentPoint==4)
        {

            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.project3Reached = true;
           // PlayerPrefs.SetInt("currentUnlockedPoint", 4);
            //PlayerPrefs.Save();
        }
        else if (col.name == "Kit2" &&currentPoint==5)
        {
            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.moveBlueImg = false;
            slidingPanelScriptAccess.kit2Reached = true;
            //PlayerPrefs.SetInt("currentUnlockedPoint", 5);
            //PlayerPrefs.Save();
        }
    }
    
    void changeColor()
    {
        flag = true;
    }
}
