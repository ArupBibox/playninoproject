﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;

public class FBDataRetrieve : MonoBehaviour
{
    public int totalScore, retrievedFreeKitScore, retrievedKit1Score;
    //public int brainScannedPrefVal, ShipScannedPrefVal, PlaneScannedPrefVal;

    public int[] kit1ProjAnsArr;
    bool kit1InitPrefSet, kit1DataRetrieved, setPrefsBool, isCR_Runnig;

    int tempInt1, tempInt2, tempInt3;
    
    //For Script Access
    ProfileHandler profileHandlerScriptAccess;
    LevelManager LevelManagerScriptAccess;
    
    void Start()
    {
        //To assign / get script
        profileHandlerScriptAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        LevelManagerScriptAccess = FindObjectOfType<LevelManager>() as LevelManager;

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

        //Array size set
        kit1ProjAnsArr = new int[LevelManagerScriptAccess.kit1TotQues + 1];

        //RetrieveFreeKitScannedImageData();

        StartCoroutine("StartSetAllPrefsCoroutine");

        StartRetrieveData();
    }
    

    IEnumerator StartSetAllPrefsCoroutine()
    {
        isCR_Runnig = true;
        yield return new WaitUntil(() => setPrefsBool);

        //Free Kit
        if (tempInt1 > PlayerPrefs.GetInt("BrainScanned"))
        {
            PlayerPrefs.SetInt("BrainScanned", tempInt1);
        }
        if (tempInt2 > PlayerPrefs.GetInt("ShipScanned"))
        {
            PlayerPrefs.SetInt("ShipScanned", tempInt2);
        }
        if (tempInt3 > PlayerPrefs.GetInt("PlaneScanned"))
        {
            PlayerPrefs.SetInt("PlaneScanned", tempInt3);
        }

        //Kit 1
        if (kit1InitPrefSet)
        {
            for (int i = 1; i < kit1ProjAnsArr.Length; i++)
            {
                PlayerPrefs.SetInt("Kit1P" + i.ToString() + "Ans", 0); //PlayerPrefs.SetInt("Kit1P1Ans", 0);
            }
            kit1InitPrefSet = false;
        }
        if (kit1DataRetrieved)
        {
            for (int i = 1; i < kit1ProjAnsArr.Length; i++)
            {
                if (kit1ProjAnsArr[i] != PlayerPrefs.GetInt("Kit1P" + i.ToString() + "Ans"))
                    PlayerPrefs.SetInt("Kit1P" + i.ToString() + "Ans", kit1ProjAnsArr[i]);
            }
            LevelManagerScriptAccess.StartCalculateLvl();
            kit1DataRetrieved = false;
        }

        PlayerPrefs.Save();
        setPrefsBool = false;
        isCR_Runnig = false;
        Debug.Log("saved");
    }

    void StartRetrieveData()
    {
        if (profileHandlerScriptAccess.gotParentKey)
        {
            FirebaseDatabase.DefaultInstance
            .GetReference("Users").Child(profileHandlerScriptAccess.userParentKeyInDB)
            .Child("Kits").Child("FreeKit").Child("score")
            .ValueChanged += RetrieveFreeKitScoreData;

            FirebaseDatabase.DefaultInstance
            .GetReference("Users").Child(profileHandlerScriptAccess.userParentKeyInDB)
            .Child("Kits").Child("Kit1").Child("score")
            .ValueChanged += RetrieveKit1ScoreData;

        Debug.Log("Watch Started");
            RetrieveFreeKitScannedImageData();
            
        }
        else
            Invoke("StartRetrieveData", 1f);
    }

    public void RetrieveFreeKitScannedImageData()
    {

        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Users")
            .Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child("FreeKit");

        reference.GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...

                Invoke("RetrieveFreeKitScannedImageData", 2);
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;

                //int tempInt1, tempInt2, tempInt3;
                string tempString;

                if (snapshot.Child("brain").Exists)
                {
                    tempString = snapshot.Child("brain").Value.ToString();
                    tempInt1 = Convert.ToInt32(tempString);
                }
                else
                {
                    reference.Child("brain").SetValueAsync(0);
                }

                if (snapshot.Child("ship").Exists)
                {
                    tempString = snapshot.Child("ship").Value.ToString();
                    tempInt2 = Convert.ToInt32(tempString);
                }
                else
                {
                    reference.Child("ship").SetValueAsync(0);
                }

                if (snapshot.Child("plane").Exists)
                {
                    tempString = snapshot.Child("plane").Value.ToString();
                    tempInt3 = Convert.ToInt32(tempString);
                }
                else
                {
                    reference.Child("plane").SetValueAsync(0);
                }
                
                RetrieveKit1ProjData();
                
            }
        });
    }

    
    void RetrieveKit1ProjData()
    {
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Users")
            .Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child("Kit1");

        reference.GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...

                Invoke("RetrieveKit1ProjData", 2);
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;

                string temp;
                for (int i = 1; i < kit1ProjAnsArr.Length; i++)
                {
                    if (snapshot.Child("p" + i.ToString()).Exists)
                    {
                        temp = snapshot.Child("p" + i.ToString()).Value.ToString();
                        kit1ProjAnsArr[i] = Convert.ToInt32(temp);
                        
                        if (i == kit1ProjAnsArr.Length-1)
                        {
                            kit1DataRetrieved = true;
                        }
                        
                    }
                    else
                    {
                        reference.Child("p" + i.ToString()).SetValueAsync(0);
                        if (i == kit1ProjAnsArr.Length - 1)
                        {
                            kit1InitPrefSet = true;
                        }
                    }
                    
                }
                //This will start the Coroutine and set all prefabs..
                if (isCR_Runnig)
                {
                    setPrefsBool = true;
                }
                else
                {
                    StartCoroutine("StartSetAllPrefsCoroutine");
                    setPrefsBool = true;
                }

            }
        });
    }

    //value change call
    void RetrieveFreeKitScoreData(object sender, ValueChangedEventArgs args)
    {
        string tempString = args.Snapshot.Value.ToString();
        retrievedFreeKitScore = Convert.ToInt32(tempString);

        if(retrievedFreeKitScore > PlayerPrefs.GetInt("FreeKitScore"))
        {
            PlayerPrefs.SetInt("FreeKitScore", retrievedFreeKitScore);
            PlayerPrefs.Save();
        }
        AddTotalScore();
    }
    void RetrieveKit1ScoreData(object sender, ValueChangedEventArgs args)
    {
        string tempString = args.Snapshot.Value.ToString();
        retrievedKit1Score = Convert.ToInt32(tempString);

        if (retrievedKit1Score > PlayerPrefs.GetInt("Kit1Score"))
        {
            PlayerPrefs.SetInt("Kit1Score", retrievedKit1Score);
            PlayerPrefs.Save();
        }
        AddTotalScore();
    }

    void AddTotalScore()
    {
        //totalScore = PlayerPrefs.GetInt("Kit1Score");
        totalScore = PlayerPrefs.GetInt("FreeKitScore") + PlayerPrefs.GetInt("Kit1Score");
        PlayerPrefs.SetInt("TotalScore", totalScore);
        PlayerPrefs.Save();
        profileHandlerScriptAccess.UpdateTotalScoreInDB();
    }
}
