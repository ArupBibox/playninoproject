﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GObjReference : MonoBehaviour {

    public static GObjReference Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    // Use this for initialization
    void Start () {
        DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
