﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingAnim : MonoBehaviour
{
    public GameObject LoadingPanel, loadingImg;
    public float loadingSpeed;
    public bool loadAnim;

    // Start is called before the first frame update
    void Start()
    {
        LoadingPanel.SetActive(false);
        loadingSpeed = 30f;
    }

    // Update is called once per frame
    void Update()
    {
        if (loadAnim)
        {
            loadingImg.transform.Rotate(new Vector3(0, 0, -20) * Time.deltaTime * loadingSpeed);
        }
    }

    public void PlayLoadingAnimation()
    {
        loadAnim = true;
        LoadingPanel.SetActive(true);
    }
    public void StopLoadingAnimation()
    {
        loadAnim = false;
        LoadingPanel.SetActive(false);
        loadingImg.transform.Rotate(new Vector3(0, 0, 0));
    }
}
