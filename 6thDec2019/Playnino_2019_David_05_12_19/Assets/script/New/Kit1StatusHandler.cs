﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;
using TMPro;

public class Kit1StatusHandler : MonoBehaviour
{
    public int kitP1Ans, kitP2Ans, kitP3Ans, kitP4Ans, kitScore, points;

    public int[] kit1ProjsAnsArry;

    public delegate void ChangeEvent(string kitName,int projectNumber,int ans);
    public event ChangeEvent changeEvent;
    public delegate void ChangeEvent1(string kitName,int score);
    public event ChangeEvent1 changeEvent1;
    public Animator scoreSlideAnimator;
    public GameObject wrongIndicator, correctIndicator;
    public Slider slideScoreSlider;
    public TMP_Text scoreTxt;
    public Transform sliderHandle, scoreTextTrans;

    string kitName;


    //For Script Access
    CircuitsUiHandler circuitsUiHandlerScript;
    FBDataUpload fBDataUploadScriptAccess;
    CircuitManager circuitManagerScript;
    SlideScoreAnim slidescoreAnimScriptAccess;
    ProfileHandler profileHandlerScript;
    
    void Start()
    {
        circuitsUiHandlerScript = FindObjectOfType<CircuitsUiHandler>();
        fBDataUploadScriptAccess = FindObjectOfType<FBDataUpload>() as FBDataUpload;
        circuitManagerScript = FindObjectOfType<CircuitManager>() as CircuitManager;
        slidescoreAnimScriptAccess = FindObjectOfType<SlideScoreAnim>() as SlideScoreAnim;
        profileHandlerScript = FindObjectOfType<ProfileHandler>() as ProfileHandler;

        //scoreTextTrans.transform.position = sliderHandle.transform.position;

        //please change this according to your kit name while using/coping this same script.
        kitName = "Kit1";

        int totQues = 15;
        kit1ProjsAnsArry = new int[totQues + 1];

        FetchFromPlayerPref();
        points = 5;

        

        
        //Delagate Subscribe start
        if (fBDataUploadScriptAccess != null)
        {
            changeEvent += fBDataUploadScriptAccess.UploadProjectAnsweredData;
            changeEvent1 += fBDataUploadScriptAccess.UploadScoreData;
        }
        else
        {
            fBDataUploadScriptAccess = FindObjectOfType<FBDataUpload>() as FBDataUpload;
            changeEvent += fBDataUploadScriptAccess.UploadProjectAnsweredData;
            changeEvent1 += fBDataUploadScriptAccess.UploadScoreData;
        }

    }
    
    void Update()
    {
        /*if(DefaultTrackableEventHandler.IsTracked)
        {
            circuitManagerScript.lostTarget = false;
        }*/
    }

    void FetchFromPlayerPref()
    {
        kitP1Ans = PlayerPrefs.GetInt(kitName + "P1Ans");
        kitP2Ans = PlayerPrefs.GetInt(kitName + "P2Ans");
        kitP3Ans = PlayerPrefs.GetInt(kitName + "P3Ans");
        kitP4Ans = PlayerPrefs.GetInt(kitName + "P4Ans");

        kitScore = PlayerPrefs.GetInt(kitName + "Score");

        for (int i = 0; i < kit1ProjsAnsArry.Length; i++)
        {
            kit1ProjsAnsArry[i] = PlayerPrefs.GetInt(kitName + "P" + i + "Ans");
            
        }

        //DelayScoreUpdate();
    }

    #region Answer Check
    public void RightAnswerClick()
    {
        switch(circuitsUiHandlerScript.curProjNum) //(circuitManagerScript.projectNumber)
        {
            case 1:
                {
                    
                    if (PlayerPrefs.GetInt(kitName + "P1Ans") == 0)
                    {
                        UpdateScore(kitName);
                        //kitP1Ans = 1; 
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    //TickAnimation();
                    break;
                }
            case 2:
                {
                    if (PlayerPrefs.GetInt(kitName + "P2Ans") == 0)
                    {
                        UpdateScore(kitName);
                        //kitP2Ans = 1;
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                   // TickAnimation();
                    break;
                }
            case 3:
                {
                    if (PlayerPrefs.GetInt(kitName + "P3Ans") == 0)
                    {
                        UpdateScore(kitName);
                        //kitP3Ans = 1;
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    //TickAnimation();
                    break;
                }
            case 4:
                {
                    if (PlayerPrefs.GetInt(kitName + "P4Ans") == 0)
                    {
                        UpdateScore(kitName);
                        //kitP4Ans = 1;
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);

                        /*circuitManagerScript.answered = true;
                        PlayerPrefs.SetInt("answered", 1);
                        PlayerPrefs.Save();*/
                    }
                    //TickAnimation();
                    break;
                }
            case 5:
                {
                    if (PlayerPrefs.GetInt(kitName + "P5Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 6:
                {
                    if (PlayerPrefs.GetInt(kitName + "P6Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 7:
                {
                    if (PlayerPrefs.GetInt(kitName + "P7Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 8:
                {
                    if (PlayerPrefs.GetInt(kitName + "P8Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 9:
                {
                    if (PlayerPrefs.GetInt(kitName + "P9Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 10:
                {
                    if (PlayerPrefs.GetInt(kitName + "P10Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 11:
                {
                    if (PlayerPrefs.GetInt(kitName + "P11Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 12:
                {
                    if (PlayerPrefs.GetInt(kitName + "P12Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 13:
                {
                    if (PlayerPrefs.GetInt(kitName + "P13Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 14:
                {
                    if (PlayerPrefs.GetInt(kitName + "P14Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
            case 15:
                {
                    if (PlayerPrefs.GetInt(kitName + "P15Ans") == 0)
                    {
                        UpdateScore(kitName);
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, 1);
                    }
                    break;
                }
        }
       /* Invoke("Reload", 0.75f);
        circuitManagerScript.lostTarget = false;*/
    }
    public void WrongAnswerClick()
    {
        switch (circuitsUiHandlerScript.curProjNum)//(circuitManagerScript.projectNumber)
        {
            case 1:
                {
                    if (PlayerPrefs.GetInt(kitName + "P1Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        /*kitP1Ans = -1;
                        kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;*/
                    }
                    //WrongAnimtion();
                    break;
                }
            case 2:
                {
                    if (PlayerPrefs.GetInt(kitName + "P2Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        /*kitP2Ans = -1;
                        kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;*/
                    }
                    //WrongAnimtion();
                    break;
                }
            case 3:
                {
                    if (PlayerPrefs.GetInt(kitName + "P3Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        /*kitP3Ans = -1;
                        kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;*/
                    }
                    //WrongAnimtion();
                    break;
                }
            case 4:
                {
                    if (PlayerPrefs.GetInt(kitName + "P4Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        /*kitP4Ans = -1;
                        kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;*/

                        /*circuitManagerScript.answered = true;
                        PlayerPrefs.SetInt("answered", 1);
                        PlayerPrefs.Save();*/
                    }
                    //WrongAnimtion();
                    break;
                }
            case 5:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 6:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                       // kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 7:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 8:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 9:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 10:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 11:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 12:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 13:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 14:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
            case 15:
                {
                    if (PlayerPrefs.GetInt(kitName + "P" + circuitsUiHandlerScript.curProjNum + "Ans") == 0)
                    {
                        UpdateAnswer(kitName, circuitsUiHandlerScript.curProjNum, -1);
                        //kit1ProjsAnsArry[circuitsUiHandlerScript.curProjNum] = -1;
                    }
                    break;
                }
        }
        /*Invoke("Reload", 0.75f);
        circuitManagerScript.lostTarget = false;*/
    }

    #endregion

    void Reload()
    {
        circuitManagerScript.Reload();
        ChangeLevel();
    }
    void ChangeLevel()
    {
        circuitManagerScript.projectNumber++;
        circuitManagerScript.SetProject(circuitManagerScript.projectNumber);
        int temp = PlayerPrefs.GetInt("currentUnlockedPoint");

        if(temp == 0)
        {
            PlayerPrefs.SetInt("currentUnlockedPoint", Mathf.Clamp(temp + 2, 3, 6));
            PlayerPrefs.Save();
        }
        else
        {
            PlayerPrefs.SetInt("currentUnlockedPoint", Mathf.Clamp(temp + 1, 3, 6));
            PlayerPrefs.Save();
        }
        
        if (temp != PlayerPrefs.GetInt("currentUnlockedPoint"))

            profileHandlerScript.SaveLevelInDB();
    }

    #region Animation Handle
    void ScoreSlideAnim()
    {
        scoreSlideAnimator.Play("progressBarAnim 0");
        slidescoreAnimScriptAccess.isSlidinScoreOpen = true;
        Invoke("DelayScoreUpdate",0.35f);
    }
    void DelayScoreUpdate()
    {
        slideScoreSlider.value = kitScore;
        scoreTextTrans.transform.position = sliderHandle.transform.position;
        scoreTxt.text = kitScore.ToString();
    }
    void TickAnimation()
    {
        correctIndicator.SetActive(true);
        Invoke("HideTickAnimtion", 0.50f);
    }
    void HideTickAnimtion()
    {
        correctIndicator.SetActive(false);
    }
    void WrongAnimtion()
    {
        wrongIndicator.SetActive(true);
        Invoke("HideWrongAnimtion", 0.50f);
    }
    void HideWrongAnimtion()
    {
        wrongIndicator.SetActive(false);
    }

    #endregion

    #region Delegate response

    public void UpdateAnswer(string kitName,int projectNumber,int ans)
    {
        switch(projectNumber)
        {
            case 1:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 2:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 3:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 4:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 5:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 6:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 7:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 8:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 9:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 10:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 11:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 12:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 13:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 14:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }
            case 15:
                {
                    kit1ProjsAnsArry[projectNumber] = ans;
                    PlayerPrefs.SetInt(kitName + "P" + projectNumber + "Ans", ans);
                    PlayerPrefs.Save();
                    break;
                }

        }
        if (changeEvent != null)
            changeEvent(kitName, projectNumber, ans);
    }
    public void UpdateScore(string kitName)
    {
        kitScore += points;
        PlayerPrefs.SetInt(kitName + "Score", kitScore);
        PlayerPrefs.Save();
        //ScoreSlideAnim();
        if (changeEvent1 != null)
            changeEvent1(kitName, kitScore);
    }

    #endregion
}
