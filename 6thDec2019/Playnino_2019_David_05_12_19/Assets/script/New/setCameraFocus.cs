﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class setCameraFocus : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnVuforiaStarted()
    {
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        Debug.Log("mode:CONTINUOUSAUTO'started");
    }
    public void OnPaused(bool paused)
    {
        if(!paused)
        {
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
            Debug.Log("mode:CONTINUOUSAUTO'afterPaused");

        }
    }
}
