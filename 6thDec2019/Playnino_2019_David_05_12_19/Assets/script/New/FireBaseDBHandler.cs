﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;


public class FireBaseDBHandler : MonoBehaviour
{
    public string youtubeLink;
    public bool youtubeLinkFetched;

    InternetCheck internetScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        internetScriptAccess = FindObjectOfType<InternetCheck>() as InternetCheck;

        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

       // FetchYouTubeVideoLink();
        internetScriptAccess.CheckInternetConnection();
    }

    // Update is called once per frame
    void Update()
    {
        if (internetScriptAccess.internetAvailable && !youtubeLinkFetched)
        {
            FetchYouTubeVideoLink();
            //Debug.Log("fetching");
        }
        else if(!internetScriptAccess.internetAvailable && !youtubeLinkFetched)
        {
            youtubeLink = "https://www.youtube.com/";
            internetScriptAccess.CheckInternetConnection();
            Debug.Log("internetChecking");
        }
    }

    void FetchYouTubeVideoLink()
    {
        FirebaseDatabase.DefaultInstance.GetReference("YoutubeVideo").GetValueAsync().ContinueWith(task => 
        {
          if (task.IsFaulted)
          {
                // Handle the error...

                FetchYouTubeVideoLink();
                Debug.Log("task failed");
          }
          else if (task.IsCompleted)
          {
              DataSnapshot snapshot = task.Result;
                // Do something with snapshot...

                youtubeLink = snapshot.Child("link").Value.ToString();

                youtubeLinkFetched = true;
          }
        });
    }
}
