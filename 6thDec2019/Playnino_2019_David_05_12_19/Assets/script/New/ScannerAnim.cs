﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerAnim : MonoBehaviour
{
    public GameObject innerImg, middleImg, outerImg;
    public float speed1, speed2, speed3, clockWise, antiClockWise;

    // Start is called before the first frame update
    void Start()
    {
        //Invoke("DisableWaringPanel", 2f);
        clockWise = -1f;
        antiClockWise = 1f;

        speed1 = 1f;
        speed2 = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        innerImg.transform.Rotate(0, 0, clockWise * speed1);
        //middleImg.transform.Rotate(0, 0, antiClockWise * speed2);
        //outerImg.transform.Rotate(0, 0, clockWise * speed3);
    }
}
