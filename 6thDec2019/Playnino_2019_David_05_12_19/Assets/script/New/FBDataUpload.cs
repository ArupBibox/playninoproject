﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;

public class FBDataUpload : MonoBehaviour
{

    //For Script Access
    ProfileHandler profileHandlerScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        //To assign / get script
        profileHandlerScriptAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        
    }

    public void UploadScannedImageData(string targetName, int value)
    {
        if (profileHandlerScriptAccess.gotParentKey)
        {
            DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
            switch (targetName)
            {
                case "Brain":
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits")
                        .Child("FreeKit").Child("brain").SetValueAsync(value);
                        break;
                    }
                case "Ship":
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits")
                        .Child("FreeKit").Child("ship").SetValueAsync(value);
                        break;
                    }
                case "Plane":
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits")
                        .Child("FreeKit").Child("plane").SetValueAsync(value);
                        break;
                    }
            }
            
            
        }
        else
        {
            Invoke("UploadScannedImageData", 1f);
        }
        
    }
    public void UploadFreeKitScoreData(int score)
    {
        if (profileHandlerScriptAccess.gotParentKey)
        {
            DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
            mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child("FreeKit").Child("score").SetValueAsync(score);
        }
        else
        {
            Invoke("UploadFreeKitScoreData", 1f);
        }
        
    }

    public void UploadProjectAnsweredData(string kitName,int projectNumber,int ans)
    {
        Debug.Log("UploadingAnswer" + ans);
        if (profileHandlerScriptAccess.gotParentKey)
        {
            DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

            switch (projectNumber)
            {
                case 1:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p1").SetValueAsync(ans);
                        break;
                    }
                case 2:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p2").SetValueAsync(ans);
                        break;
                    }
                case 3:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p3").SetValueAsync(ans);
                        break;
                    }
                case 4:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p4").SetValueAsync(ans);
                        break;
                    }
                case 5:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p5").SetValueAsync(ans);
                        break;
                    }
                case 6:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p6").SetValueAsync(ans);
                        break;
                    }
                case 7:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p7").SetValueAsync(ans);
                        break;
                    }
                case 8:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p8").SetValueAsync(ans);
                        break;
                    }
                case 9:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p9").SetValueAsync(ans);
                        break;
                    }
                case 10:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p10").SetValueAsync(ans);
                        break;
                    }
                case 11:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p11").SetValueAsync(ans);
                        break;
                    }
                case 12:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p12").SetValueAsync(ans);
                        break;
                    }
                case 13:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p13").SetValueAsync(ans);
                        break;
                    }
                case 14:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p14").SetValueAsync(ans);
                        break;
                    }
                case 15:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p15").SetValueAsync(ans);
                        break;
                    }
                case 16:
                    {
                        mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("p16").SetValueAsync(ans);
                        break;
                    }

            }
        }
        else
        {
            Invoke("UploadProjectAnsweredData", 1f);
        }
        
    }
    public void UploadScoreData(string kitName,int score)
    {
        Debug.Log("UploadingDB" + score);

        if (profileHandlerScriptAccess.gotParentKey)
        {
            DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
            mDatabaseRef.Child("Users").Child(profileHandlerScriptAccess.userParentKeyInDB).Child("Kits").Child(kitName).Child("score").SetValueAsync(score);
        }
        else
        {
            Invoke("UploadKit1ScoreData", 1f);
        }

    }
}
