﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileBttnsResponse : MonoBehaviour {

    public GameObject rateUsPopUpPanel;
    public Sprite[] smileySprites;
    public Slider ratingSlider;
    float currentSliderValue;
    public Image smilieImg;
    float ratedValue;
    
	void Start () {
        rateUsPopUpPanel.SetActive(false);
    }
	
	void Update () {

        currentSliderValue = ratingSlider.value;

        for (int i = 1; i <= smileySprites.Length; i++)
        {
            if (currentSliderValue == i)
            {
                smilieImg.sprite = smileySprites[i-1];
            }
        }

        ratedValue = currentSliderValue;

    }

    public void RateUsBttnClicked()
    {
        rateUsPopUpPanel.SetActive(true);
    }

    public void RateUsCloseBttnClicked()
    {
        rateUsPopUpPanel.SetActive(false);
    }

    public void SubmitRatingButtonClicked()
    {

        rateUsPopUpPanel.SetActive(false);
    }

    public void SignOutBttnClicked()
    {

    }
}
