﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;
using Vuforia;
using UnityEngine.Android;
using UnityEngine.UI;

public class KitsHandler : MonoBehaviour
{
    private IEnumerator coroutine;

    //Script Access
    ProfileHandler profileHandlerScriptAccess;
    PopUpHandler popupHandlerScriptAccess;
    AnimationHandler animHandlerScriptAccess;

    public static int circuitNumber;
    string activeStatus, userParentKeyInDB;
    public GameObject sceneCamera;
    public bool sceneLoadingInBackground, loadArScene, sceneAlreadyLoaded, isSceneLoaded;
    public Button kitOneButton;

    public int bttnClick;

    AsyncOperation loadSceneAsync;
    // Start is called before the first frame update
    void Start()
    {
        profileHandlerScriptAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        popupHandlerScriptAccess = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        animHandlerScriptAccess = FindObjectOfType<AnimationHandler>() as AnimationHandler;
        //downloadScriptAccess = FindObjectOfType<DownloadAndLoad>() as DownloadAndLoad;

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;
        PlayerPrefs.SetInt("loaded", 0);
        PlayerPrefs.Save();
    }

    // Update is called once per frame
    void Update()
    {
        if (sceneLoadingInBackground)
        {
            if (loadSceneAsync.progress >= 0.9f && !isSceneLoaded)
            {

                loadSceneAsync.allowSceneActivation = false;
                if (loadArScene)
                {
                    loadSceneAsync.allowSceneActivation = true;
                    //popupHandlerScriptAccess.mainSceneCanvas.SetActive(false);
                    isSceneLoaded = true;
                }
            }

        }
    }
    /*public void KitYesBttnClick()
    {
        if(profileHandlerScriptAccess.kit1ActiveStatus == "true")
        {

        }
        else
        {
            SceneManager.LoadScene(3, LoadSceneMode.Additive);
        }
        //sceneCamera.GetComponent<VuforiaBehaviour>().enabled = false;
        
        popupHandlerScriptAccess.mainUI.SetActive(false);
        popupHandlerScriptAccess.isArSceneOpened = true;
        //SceneManager.LoadSceneAsync("StarterKit1UI_v1.0.7", LoadSceneMode.Additive);
        //SceneManager.LoadSceneAsync("StarterKit1", LoadSceneMode.Additive);
         VuforiaBehaviour vuforiaBehaviour = sceneCamera.GetComponent<VuforiaBehaviour>();
         if (vuforiaBehaviour != null)
         {
             vuforiaBehaviour.enabled = false;
             Debug.Log("inside vuforia disable");
         }

        CheckKitActiveStatus();


    }*/

    public void StarterKit1ButtonClick(int fromScene)
    {
        bttnClick = 1;
        coroutine = CheckPermission(fromScene);
        StartCoroutine(coroutine);
        
        /*kitOneButton.interactable = false;
        //LoadSceneInBackGround();
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
            Invoke("LoadScene", 1);
        }
        else
        {
            LoadScene();
        }*/
    }

    IEnumerator CheckPermission(int fromScene)
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera) ||
            !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
            yield return new WaitForSeconds(.3f);
            if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            }
            yield return new WaitForSeconds(.3f);
            ChangeScene(fromScene);
        }
        else
        {
            ChangeScene(fromScene);
        }
    }
    public void ChangeScene(int fromScene)
    {
        if (Permission.HasUserAuthorizedPermission(Permission.Camera) &&
            Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            if (fromScene == 1)
            {
                kitOneButton.interactable = false;
                SceneManager.LoadScene("ObbExtractorScene", LoadSceneMode.Additive);
            }
            else
            {
                SceneManager.UnloadSceneAsync("ObbExtractorScene");

                bttnClick = 0;//Resetting

                kitOneButton.interactable = false;

                CheckKitActiveStatus();

                /*VuforiaRuntime.Instance.InitVuforia();
                popupHandlerScriptAccess.mainUI.SetActive(false);
                Screen.orientation = ScreenOrientation.Landscape;
                popupHandlerScriptAccess.loadingPanel.SetActive(true);
                kitOneButton.interactable = true;
                SceneManager.LoadSceneAsync("StarterKit1_v1", LoadSceneMode.Additive);*/

            }
        }
        else
        {
            kitOneButton.interactable = true;
        }

    }

    void LoadScene()
    {
        if (Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            /*popupHandlerScriptAccess.mainUI.SetActive(false);
            loadArScene = true;
            popupHandlerScriptAccess.isArSceneOpened = true;
            popupHandlerScriptAccess.loadingPanel.SetActive(false);*/

            //downloadScriptAccess.LoadSceneFromBundle("StarterKit1_v1");//added
            //SceneManager.LoadSceneAsync("DownloadScene", LoadSceneMode.Additive);

            /*VuforiaRuntime.Instance.InitVuforia();
            Screen.orientation = ScreenOrientation.Landscape;
            KitsHandler.circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);*/

            /*VuforiaRuntime.Instance.InitVuforia();
            popupHandlerScriptAccess.mainUI.SetActive(false);
            Screen.orientation = ScreenOrientation.Landscape;
            popupHandlerScriptAccess.loadingPanel.SetActive(true);
            kitOneButton.interactable = true;
            //VuforiaRuntime.Instance.InitVuforia();
            SceneManager.LoadSceneAsync("StarterKit1_v1", LoadSceneMode.Additive);*/
            
            CheckKitActiveStatus();
        }
        else
        {
            kitOneButton.interactable = true;
            /*popupHandlerScriptAccess.loadingPanel.SetActive(false);
            popupHandlerScriptAccess.mainUI.SetActive(true);*/

        }
    }
    public void KitNoBttnClick()
    {

    }
    public void StarterKit2ButtonClick()
    {
        animHandlerScriptAccess.SubscribeBttnClick();
    }
    public void StarterKit3ButtonClick()
    {
        animHandlerScriptAccess.ShowCommimgSoonAnim();
        
    }

    void CheckKitActiveStatus()
    {

        if(PlayerPrefs.GetString("UserKit1ActiveStatus") == "true")
        {
            popupHandlerScriptAccess.mainUI.SetActive(false);
            Screen.orientation = ScreenOrientation.Landscape;
            popupHandlerScriptAccess.loadingPanel.SetActive(true);
            kitOneButton.interactable = true;

            VuforiaRuntime.Instance.InitVuforia();
            
            //KitsHandler.circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);

            SceneManager.LoadSceneAsync("StarterKit1_v1", LoadSceneMode.Additive);
        }
        else
        {
            popupHandlerScriptAccess.mainUI.SetActive(false);
            popupHandlerScriptAccess.loadingPanel.SetActive(false);//added
            kitOneButton.interactable = true;
            SceneManager.LoadSceneAsync("qrCodeScanner", LoadSceneMode.Additive);
        }


      // SceneManager.LoadSceneAsync("qrCodeScanner", LoadSceneMode.Additive);
       /*DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Users");
        if(profileHandlerScriptAccess.gotParentKey)
        {
            userParentKeyInDB = profileHandlerScriptAccess.userParentKeyInDB;
            reference.Child(userParentKeyInDB).GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...

                    Invoke("CheckKitActiveStatus", 2);
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    activeStatus = snapshot.Child("KitActivation").Child("Starterkit1").Child("activeStatus").Value.ToString();
                    
                    if (activeStatus=="true")
                    {
                        popupHandlerScriptAccess.mainUI.SetActive(false);
                        //VuforiaRuntime.Instance.InitVuforia();
                        //Screen.orientation = ScreenOrientation.Landscape;
                        //circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);

                        //popupHandlerScriptAccess.loadingPanel.SetActive(false);//added
                        //downloadScriptAccess.LoadSceneFromBundle("StarterKit1_v1");//added
                        //SceneManager.LoadSceneAsync("DownloadScene", LoadSceneMode.Additive);

                        

                        //loadArScene = true;
                        popupHandlerScriptAccess.isArSceneOpened = true;
                        popupHandlerScriptAccess.loadingPanel.SetActive(false);

                        VuforiaRuntime.Instance.InitVuforia();
                        Screen.orientation = ScreenOrientation.Landscape;
                        KitsHandler.circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);

                        SceneManager.LoadSceneAsync("StarterKit1_v1", LoadSceneMode.Additive);


                    }
                    else
                    {
                        popupHandlerScriptAccess.mainUI.SetActive(false);
                        popupHandlerScriptAccess.loadingPanel.SetActive(false);//added
                        kitOneButton.interactable = true;
                        SceneManager.LoadSceneAsync("qrCodeScanner", LoadSceneMode.Additive);
                    }
                }

            });
        }
        else
        {
            Invoke("CheckKitActiveStatus", 2);
        }*/
        
    }
}
