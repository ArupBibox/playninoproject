﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class smartcard : MonoBehaviour {
    //public GameObject Led;
    public Material mat;
    public GameObject llight;
    public GameObject door;
    public GameObject spark;
    Animator anim;

    // Use this for initialization
    void Start () {
        anim = door.GetComponent<Animator>();
        spark.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Player")
                {
                    mat.EnableKeyword("_EMISSION");
                    llight.SetActive(true);
                    anim.Play("openDoor");
                    spark.SetActive(true);
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            mat.DisableKeyword("_EMISSION");
            llight.SetActive(false);
            anim.Play("closeDoor");
            spark.SetActive(false);
        }
	}
}
