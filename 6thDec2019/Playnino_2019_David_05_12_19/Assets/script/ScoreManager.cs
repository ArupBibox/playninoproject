﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    int ShipPoints = 0, JetPoints = 0, BrainPoints = 0;
    public int DemoScore, scorePoint, totalScore, currentScore;
    int ShipTrackedState = 0, PlaneTrackedState = 0, BrainTrackedState = 0;

    bool flag;
    //Script Access
    public ProfileHandler profileHandlerScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        flag = false;

        scorePoint = 5;
        CheckAndSetScore();

        profileHandlerScriptAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        totalScore = PlayerPrefs.GetInt("TotalArScore");
        
    }
    void CheckAndSetScore()
    {
        if (PlayerPrefs.GetInt("ShipTrackedState") == 1)
        {
            PlayerPrefs.SetInt("ShipPoints", scorePoint);
            PlayerPrefs.Save();
        }
        if (PlayerPrefs.GetInt("PlaneTrackedState") == 1)
        {
            PlayerPrefs.SetInt("JetPoints", scorePoint);
            PlayerPrefs.Save();
        }
        if (PlayerPrefs.GetInt("BrainTrackedState") == 1)
        {
            PlayerPrefs.SetInt("BrainPoints", scorePoint);
            PlayerPrefs.Save();
        }
    }
    // Update is called once per frame
    void Update()
    {
        currentScore = DemoScore;
        setPoints();
        GetPoints();
        TotalScoreCalculation();
        if(!flag)
        {
            if (PlayerPrefs.GetInt("ShipTrackedState") == 1 || PlayerPrefs.GetInt("PlaneTrackedState") == 1 || PlayerPrefs.GetInt("BrainTrackedState") == 1)
            {

                int tempLevel = PlayerPrefs.GetInt("currentUnlockedPoint");
                if (tempLevel <= 0)
                {
                    PlayerPrefs.SetInt("currentUnlockedPoint", 1);
                    PlayerPrefs.Save();
                    profileHandlerScriptAccess.SaveLevelInDB();
                    flag = true;
                    Debug.Log("level saved!");
                }
            }
        }
    }
    void setPoints()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            if (PlayerPrefs.GetInt("ShipTrackedState") == 0)
            {
                if (DefaultTrackableEventHandler.TrackImageName == "ShipTarget")
                {
                    PlayerPrefs.SetInt("ShipPoints", scorePoint);
                    PlayerPrefs.SetInt("ShipTrackedState", 1);
                    PlayerPrefs.Save();
                    TotalScoreCalculation();
                    /*profileHandlerScriptAccess.UpdateArScoreInDB();
                    profileHandlerScriptAccess.UpdateArScannedTargetShip();*/
                }
            }
            if (PlayerPrefs.GetInt("PlaneTrackedState") == 0)
            {
                if (DefaultTrackableEventHandler.TrackImageName == "PlaneTarget")
                {
                    PlayerPrefs.SetInt("JetPoints", scorePoint);
                    PlayerPrefs.SetInt("PlaneTrackedState", 1);
                    PlayerPrefs.Save();
                    TotalScoreCalculation();
                   /* profileHandlerScriptAccess.UpdateArScoreInDB();
                    profileHandlerScriptAccess.UpdateArScannedTargetPlane();*/
                }
            }
            if (PlayerPrefs.GetInt("BrainTrackedState") == 0)
            {
                if (DefaultTrackableEventHandler.TrackImageName == "BrainTarget")
                {
                    PlayerPrefs.SetInt("BrainPoints", scorePoint);
                    PlayerPrefs.SetInt("BrainTrackedState", 1);
                    PlayerPrefs.Save();
                    TotalScoreCalculation();
                    /*profileHandlerScriptAccess.UpdateArScoreInDB();
                    profileHandlerScriptAccess.UpdateArScannedTargetBrain();*/

                    //Debug.Log("called");
                }
            }
        }
     }
    void GetPoints()
    {
        DemoScore = PlayerPrefs.GetInt("ShipPoints") + PlayerPrefs.GetInt("JetPoints") + PlayerPrefs.GetInt("BrainPoints");
        totalScore = DemoScore;
    }

    public void TotalScoreCalculation()
    {
        GetPoints();

        if (currentScore > PlayerPrefs.GetInt("TotalArScore"))
        {
            PlayerPrefs.SetInt("TotalArScore", totalScore);
            PlayerPrefs.Save();
            int tScore = totalScore + PlayerPrefs.GetInt("TotalKitScore");//calculating total score
            PlayerPrefs.SetInt("TotalScore", tScore);
            PlayerPrefs.Save();

            //profileHandlerScriptAccess.UpdateArScoreInDB();

        }
    }

}
