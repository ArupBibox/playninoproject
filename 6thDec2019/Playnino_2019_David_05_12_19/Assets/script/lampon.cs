﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lampon : MonoBehaviour {

    public GameObject pointLight;
    public GameObject spotLight;
    public GameObject spark;

    void Start()
    {

        GameObject.Find("LampSwitch_Whiteoff").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("LampSwitch_Whiteoff").GetComponent<BoxCollider>().enabled = false;
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "LampSwitch_Whiteon")
                {
                    GetComponent<BoxCollider>().enabled = false;
                    GameObject.Find("LampSwitch_Whiteoff").GetComponent<MeshRenderer>().enabled = true;
                    GameObject.Find("LampSwitch_Whiteoff").GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = false;
                    pointLight.SetActive(true);
                    spotLight.SetActive(true);
                    spark.SetActive(true);
                }
                if (hit.transform.name == "LampSwitch_Whiteoff")
                {
                    hit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    GetComponent<BoxCollider>().enabled = true;
                    GetComponent<MeshRenderer>().enabled = true;
                    pointLight.SetActive(false);
                    spotLight.SetActive(false);
                    spark.SetActive(false);
                }
            }
        }
    }
}
