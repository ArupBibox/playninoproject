﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;


public class circuitParent : MonoBehaviour
{
    //public GameObject Circuit1, Circuit2, Circuit3, Circuit4;
    public GameObject project1Center;
    GameObject CircuitClone;
    DefaultTrackableEventHandler dtHandler;
    bool clone;
    public Vector3 OriginalCircuitPos;
    public Camera gameCamera;
    CircuitsTargetController circuitMangerScript;


    private void Awake()
    {
        //TrackerManager.Instance.GetTracker<PositionalDeviceTracker>().Stop();
    }

    // Start is called before the first frame update
    void Start()
    {
        dtHandler = GameObject.FindObjectOfType<DefaultTrackableEventHandler>();
        circuitMangerScript = FindObjectOfType<CircuitsTargetController>() as CircuitsTargetController;
        //project1Center.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {
        /* if (dtHandler.reload)
         {
             // Circuit.transform.localPosition = new Vector3(-0.07320001f, 0, 0);
             Circuit1.transform.localPosition = OriginalCircuitPos;
             Circuit1.transform.localRotation = Quaternion.Euler(0, 0, 0);

             Circuit2.transform.localPosition = OriginalCircuitPos;
             Circuit2.transform.localRotation = Quaternion.Euler(0, 0, 0);

             Circuit3.transform.localPosition = OriginalCircuitPos;
             Circuit3.transform.localRotation = Quaternion.Euler(0, 0, 0);

             Circuit4.transform.localPosition = OriginalCircuitPos;
             Circuit4.transform.localRotation = Quaternion.Euler(0, 0, 0);
             // Circuit.SetActive(false);
             //  TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();

         }*/
        if (dtHandler.lost)
        {
            Debug.Log("LOST");

           // circuitMangerScript.lostTarget = true;
           // TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
            //gameCamera.transform.position = new Vector3(-12, 95, -11);
            //gameCamera.transform.rotation = Quaternion.Euler(new Vector3(95, 15, 15));
            if (circuitMangerScript.projectNumber == 1)
            {
                Debug.Log("PROJECT1");
                gameCamera.transform.position = new Vector3(-12, 95, -11);
                gameCamera.transform.rotation = Quaternion.Euler(new Vector3(95, 15, 15));

            }
            else if (circuitMangerScript.projectNumber == 2)
            {
                gameCamera.transform.position = new Vector3(-4, 95, -11);
                gameCamera.transform.rotation = Quaternion.Euler(new Vector3(95, 15, 15));
            }
            else if (circuitMangerScript.projectNumber == 3 || circuitMangerScript.projectNumber == 4)
            {
                gameCamera.transform.position = new Vector3(-5, 95, 2);
                gameCamera.transform.rotation = Quaternion.Euler(new Vector3(95, 15, 15));
            }

        }
        /* if (clone)
          {
              centerGameObject(Circuit1, gameCamera);
              centerGameObject(Circuit2, gameCamera);
              centerGameObject(Circuit3, gameCamera);
              centerGameObject(Circuit4, gameCamera);
              clone = false;
              dtHandler.lost = false;

          }*/

    }
    void centerGameObject(GameObject gameOBJToCenter, Camera cameraToCenterOBjectTo, float zOffset = 25f)
    {

        if (KitsHandler.circuitNumber == 1)
        {
            //gameOBJToCenter.transform.position = gameCamera.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, gameCamera.nearClipPlane));
            //gameOBJToCenter.transform.position = cameraToCenterOBjectTo.ViewportToWorldPoint(new Vector3(1f, 1f, cameraToCenterOBjectTo.nearClipPlane + zOffset));
            //gameOBJToCenter.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x-90, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
            //gameOBJToCenter.SetActive(true);

        }
        if (KitsHandler.circuitNumber == 2)
        {
            gameOBJToCenter.transform.position = cameraToCenterOBjectTo.ViewportToWorldPoint(new Vector3(0.5f, 0.7f, cameraToCenterOBjectTo.nearClipPlane + zOffset));
            gameOBJToCenter.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x - 90, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }
        if ((KitsHandler.circuitNumber == 3) || (KitsHandler.circuitNumber == 4))
        {
            gameOBJToCenter.transform.position = cameraToCenterOBjectTo.ViewportToWorldPoint(new Vector3(0.5f, 0.6f, cameraToCenterOBjectTo.nearClipPlane + zOffset));
            gameOBJToCenter.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x - 90, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }



    }


}
