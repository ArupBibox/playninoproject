﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnappingTest : MonoBehaviour
{
    public Camera snapCamera, ARCamera;
    DefaultTrackableEventHandler trackHandler;
    // Start is called before the first frame update
    void Start()
    {
        trackHandler = FindObjectOfType<DefaultTrackableEventHandler>() as DefaultTrackableEventHandler;
        snapCamera.enabled = false;
        ARCamera.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (trackHandler.lost)
        {
            snapCamera.enabled = true;
            ARCamera.enabled = false;
        }
    }
}
