﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SnapCameraController : MonoBehaviour
{


    // public SnapCameraController Inst;

    public Transform Snap_Camera;
    public Camera Ar_Camera;
    public List<Transform> SnapPosition = new List<Transform>();
    //public GameObject circuitComponent;
    // Start is called before the first frame update
   // public GameObject vjfsvj;
    public DefaultTrackableEventHandler defaultTrackableEventHandlerScriptAccess;

    void Awake()
    {

     //   Inst = this;    
    }

    private void Start()
    {
       // defaultTrackableEventHandlerScriptAccess = vjfsvj.GetComponent<DefaultTrackableEventHandler>();
        defaultTrackableEventHandlerScriptAccess = GameObject.FindObjectOfType<DefaultTrackableEventHandler>();
    }
    public void SnapCamera()
    {
        
        //Ar_Camera.transform.position = new Vector3(SnapPosition[index].transform.position.x, SnapPosition[index].transform.position.y, SnapPosition[index].transform.position.z);
       // circuitComponent.transform.position = new Vector3(0, 0, -29.9f);
        
        Ar_Camera.transform.position = new Vector3(-12,95,-11);
         Ar_Camera.transform.rotation = Quaternion.Euler(new Vector3 (95,15,15));
        //Ar_Camera.transform.rotation = Quaternion.Euler(100.86f, 4.1439f, 6.8029f);

        Debug.Log("Snapped");
    }

    public void ARCameraOn(bool val)
    {

    //    Ar_Camera.GetComponent<Camera>().enabled = false;
     //   Snap_Camera.transform.gameObject.SetActive(true);
      //  Ar_Camera.transform.gameObject.SetActive(!val);
    }
    private void Update()
    {
        if (defaultTrackableEventHandlerScriptAccess.lost)
        {
            TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
            SnapCamera();
        }
    }
}
