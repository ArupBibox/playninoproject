﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitsTargetController : MonoBehaviour
{
    public GameObject[] Circuits, canvasComponents;
    public bool removeme1 = true;
    public bool removeme2 = true;
    public bool removeme3 = true;

    public bool isTargetDetected;
    // public Camera[] SnapCameras;
    // public Camera ARCam;
    DefaultTrackableEventHandler TrackHandler;
    public int projectNumber = 0;

    CircuitsUiHandler circuitsUiHandlerAccess;
    UIManager uiManagerScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        TrackHandler = GameObject.FindObjectOfType<DefaultTrackableEventHandler>();
        circuitsUiHandlerAccess = FindObjectOfType<CircuitsUiHandler>();
        uiManagerScriptAccess = FindObjectOfType<UIManager>();

        // SingleScan();
        //InvokeRepeating("SingleScan", 0, .5f);
        canvasComponents[0].SetActive(false);
        canvasComponents[1].SetActive(false);
       // SnapCameras[0].enabled = false;
      //  ARCam.enabled = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isTargetDetected)
        {
            circuitsUiHandlerAccess.ScanCircle_On_Off(true);
            SingleScan();
        }
            
        
    }


    public void CircuitsOn_Off(int val, bool OnSpecific)
    {
        for (int i = 0; i < Circuits.Length; i++)
        {
            //if(!Circuits[i].activeSelf)
                Circuits[i].SetActive(false);
        }

        if (OnSpecific)
        {
            OnSpecificCircuit(val);
        }
            
    }
    void OnSpecificCircuit(int val)
    {
        Circuits[val].SetActive(true);
    }

    void TargetDetected(bool isFreeKit)
    {
        isTargetDetected = true;

        //uiManagerScriptAccess.ScanCircle.SetActive(false);
        if (!isFreeKit)
        {
            circuitsUiHandlerAccess.curProjNum = projectNumber;
            circuitsUiHandlerAccess.ProjectNameOn_Off(true);
        }
        

        circuitsUiHandlerAccess.ScanCircle_On_Off(false);
    }

    void SingleScan()
    {
        string trackedTragetName = DefaultTrackableEventHandler.TrackImageName;

        if(trackedTragetName == "ShipTarget"|| trackedTragetName == "PlaneTarget"|| trackedTragetName == "BrainTarget")
        {
            TargetDetected(true);
            return;
        }
        if (DefaultTrackableEventHandler.TrackImageName == "BATTERIESINSERIES")
        {


            //  ARCam.enabled = false;
            //  SnapCameras[0].enabled = true;
            //  Circuits[0].SetActive(true);
            // Circuits[1].SetActive(false);
            // Circuits[2].SetActive(false);
            // Circuits[3].SetActive(false);
            // Circuits[4].SetActive(false);
            // Circuits[5].SetActive(false);
            // Circuits[6].SetActive(false);
            // Circuits[7].SetActive(false);
            CircuitsOn_Off(0,true);


            canvasComponents[0].SetActive(true);
            canvasComponents[1].SetActive(false);
            projectNumber = 1;
            // if (TrackHandler.lost)
            // {
            // SnapCameras[0].enabled = true;
            // ARCam.enabled = false;
            // }

            // else if (DefaultTrackableEventHandler.IsTracked)
            // {
            //  SnapCameras[0].GetComponent<Camera>().enabled = false;
            //  ARCam.GetComponent<Camera>().enabled = true;
            // }

            TargetDetected(false);
            
        }
        else if (DefaultTrackableEventHandler.TrackImageName == "circuit2") 
        {
            /* Circuits[0].SetActive(false);
             Circuits[1].SetActive(true);
             Circuits[2].SetActive(false);
             Circuits[3].SetActive(false);
             */
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(true);
            projectNumber = 2;
            CircuitsOn_Off(1, true);

            TargetDetected(false);

        }
        else if (DefaultTrackableEventHandler.TrackImageName == "SIMPLE_CIRCUIT")
        {
            /* Circuits[0].SetActive(false);
             Circuits[1].SetActive(false);
             Circuits[2].SetActive(true);
             Circuits[3].SetActive(false);
             */
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 3;
            CircuitsOn_Off(2, true);

            TargetDetected(false);

        }
        else if (DefaultTrackableEventHandler.TrackImageName == "LED_IN_SERIES")
        {
            //Circuits[0].SetActive(false);
            //Circuits[1].SetActive(false);
            //Circuits[2].SetActive(false);
            //Circuits[3].SetActive(true);
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 4;
            CircuitsOn_Off(3, true);

            TargetDetected(false);

        }
        else if (DefaultTrackableEventHandler.TrackImageName == "ledinparallel")
        {
            if (removeme3)
                return;
            removeme3 = true;
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 5;
            CircuitsOn_Off(4, true);
            //  Circuits[4].transform.position = Camera.main.transform.position * 5;

            TargetDetected(false);

        }
        else if (DefaultTrackableEventHandler.TrackImageName == "SWITCHES_IN_PARALLEL")
        {
           // if (removeme2)
            //    return;
           // removeme2 = true;
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 6;
            CircuitsOn_Off(5, true);
            //  Circuits[5].transform.position = Camera.main.transform.position * 5;

            TargetDetected(false);

        }
        else if (DefaultTrackableEventHandler.TrackImageName == "PATH_OF_LEAST_RESISTANCE")
        {
            Debug.Log(removeme1);
           // Debug.Break();
           // if (removeme1)
            //    return;
           // removeme1 = true;

           //Debug.LogError("ffffffttttttttttttttttttttttttttttttttttttttttttttttttt");
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 7;

            CircuitsOn_Off(6, true);
            //  Circuits[6].transform.position = Camera.main.transform.position * 5;

            TargetDetected(false);

        }

        /*else if (trackedTragetName == "Cross_Lap_Joint" || trackedTragetName == "BOX" ||
            trackedTragetName == "Mortise_And_Tenon_Joint" || trackedTragetName == "BUTT_JOINT" ||
            trackedTragetName == "Tongue_and_Groove_Joint")
        {
            Off_On_Circuits(0, false);
        }*/

        else if (DefaultTrackableEventHandler.TrackImageName == "Cross_Lap_Joint")
        {
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 8;
            CircuitsOn_Off(7, true);
            TargetDetected(false);
        }
        else if (DefaultTrackableEventHandler.TrackImageName == "BOX")
        {
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 9;
            CircuitsOn_Off(8, true);
            TargetDetected(false);
        }
        else if (DefaultTrackableEventHandler.TrackImageName == "Mortise_and_Tenon_Joint")
        {
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 10;
            CircuitsOn_Off(9, true);
            TargetDetected(false);
        }
        else if (DefaultTrackableEventHandler.TrackImageName == "BUTT_JOINT")
        {
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 11;
            CircuitsOn_Off(10, true);
            TargetDetected(false);
        }
        else if (DefaultTrackableEventHandler.TrackImageName == "Tongue_and_Groove_Joint")
        {
            canvasComponents[0].SetActive(false);
            canvasComponents[1].SetActive(false);
            projectNumber = 12;
            CircuitsOn_Off(11, true);
            TargetDetected(false);
        }

    }
}
