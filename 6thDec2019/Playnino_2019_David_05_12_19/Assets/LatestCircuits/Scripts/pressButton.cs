﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class pressButton : MonoBehaviour, IPointerDownHandler,
    IPointerUpHandler
{
   public bool pressed = false;
    CircuitController dataHandler;
    public void OnPointerDown(PointerEventData eventData)
    {
        pressed = true;
       // throw new System.NotImplementedException();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
       // throw new System.NotImplementedException();
    }

    // Start is called before the first frame update
    void Start()
    {
        dataHandler = FindObjectOfType<CircuitController>() as CircuitController;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.name == "ButtonSeries")
        {
            if (pressed)
            {
                dataHandler.StartElectronFlow();
            }
            else if (!pressed)
            {
                dataHandler.EndElectronFlow();
            }
        }
        if (this.gameObject.name == "ButtonParallel")
        {
            if (pressed)
            {
                dataHandler.StartElectronFlowParallel();
            }
            else if (!pressed)
            {
                dataHandler.EndElectronFlowParallel();
            }
        }
    }
}
