﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronMovement4 : MonoBehaviour
{
    CircuitController dataHandler;
    public Transform[] target;
   // public List<Transform> WPTransform = new List<Transform>();
   // public List<Transform> target = new List<Transform>();
    public float speed;
    public int current;
    public Transform newSpawnPoint;
    public GameObject circuitTarget;
    bool Spawned = false, Spawned2 = false;
    public GameObject LED1, LED2;
    public GameObject sphere2;
    public Battery2 Battery1;
    public Battery2 Battery2;

    bool isNxtBttnShown;

    [SerializeField]
   // private pressButton buttonHandler;
  //  [SerializeField]
    CircuitController circuitHandler;

    CircuitsUiHandler circuitsUiHandlerAccess;
    // Start is called before the first frame update
    void Start()
    {
        circuitHandler = FindObjectOfType<CircuitController>() as CircuitController;
        circuitsUiHandlerAccess = FindObjectOfType<CircuitsUiHandler>();

        if (this.gameObject.tag == "Electron1")

        //for(int wp = 0; wp < WPTransform.Count; wp++)
        {
            //WPTransform[wp].GetComponent<WP_Prop>().IndexPosition = wp;
           // target[wp]=WPTransform[wp].transform;


            target = new Transform[8];
            target[0] = GameObject.Find("wpp1").transform;
            target[1] = GameObject.Find("wpp2").transform;
            target[2] = GameObject.Find("wpp3").transform;
            target[3] = GameObject.Find("wpp4").transform;
            target[4] = GameObject.Find("wpp5").transform;
            target[5] = GameObject.Find("wpp6").transform;
            target[6] = GameObject.Find("wpp7").transform;
            target[7] = GameObject.Find("wpp8").transform;


          
        }
       else if(this.gameObject.tag == "Electron2")
        {
            target = new Transform[6];
            target[0] = GameObject.Find("wpp3").transform;
            target[1] = GameObject.Find("wpp9").transform;
            target[2] = GameObject.Find("wpp10").transform;
            target[3] = GameObject.Find("wpp11").transform;
            target[4] = GameObject.Find("wpp12").transform;
            target[5] = GameObject.Find("wpp6").transform;

        }
        newSpawnPoint = GameObject.Find("NewSpawnPointParallel").transform;
        circuitTarget = GameObject.Find("ModelsParallelCircuit");

        circuitHandler = FindObjectOfType<CircuitController>() as CircuitController;
        sphere2 = Resources.Load("Electron5") as GameObject;
        GameObject go = GameObject.FindGameObjectWithTag("battery3");
        if (go != null)
        {
            Battery1 = go.GetComponent<Battery2>();
        }
        go = GameObject.FindGameObjectWithTag("battery4");
        if (go != null)
        {
            Battery2 = go.GetComponent<Battery2>();
        }
        //dataHandler=find
    }

    // Update is called once per frame
    void FixedUpdate()
    {
      //  if (DefaultTrackableEventHandler.IsTracked)
      //  {
            MoveThroughPoints();
            if (this.gameObject.tag == "Electron1")
            {
                SpawnNewElectron();
            }
            
            DestroyElectrons();
            if (circuitHandler.endCircuitParallel)
            {
                
                Destroy(gameObject);
            }
           
            HideElectrons();
            SpawnSplitElectron();
            if ((Battery1.batteryVoltage == 0.78f) && (Battery1.batteryVoltage == 0.78f))
            {
                Destroy(gameObject);
            }


       // }

    }
    void MoveThroughPoints()
    {
        // Debug.Log("CURRENT" + target[current].position);
        // Debug.Log("transform.position" + transform.position);
        if (transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.fixedDeltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else
        {
            current = current + 1 % target.Length;
        }

    }
    void SpawnNewElectron()
    {
        if (!Spawned)
        {

            if (gameObject.transform.position.z > newSpawnPoint.transform.position.z)
            {
                GameObject sphereClone = Instantiate(gameObject, target[0].transform.position, Quaternion.identity, circuitTarget.transform) as GameObject;
                Spawned = true;
            }
        }
    }
    void DestroyElectrons()
    {
      
        if (current == target.Length)
        {
            if (Battery1.batteryON)
            {
                Battery1.DrainBattery();
            }
            if (Battery2.batteryON)
            {
                Battery2.DrainBattery();
            }


            Destroy(gameObject);

            if (!isNxtBttnShown)
            {
                circuitsUiHandlerAccess.ShowNextBttn(true);
                isNxtBttnShown = true;
            }

        }
       

    }
    void HideElectrons()
    {
        if (this.gameObject.tag == "Electron1")
        {
            if (current == 4)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
        }
        else if(this.gameObject.tag == "Electron2")
        {
            if (current == 3)
            {
                gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
        }
    }
    void SpawnSplitElectron()
    {
        if (this.gameObject.tag == "Electron1")
        {
            if (!Spawned2)
            {
                if (current == 3)
                {
                   GameObject go = Instantiate(sphere2, target[2].transform.position, Quaternion.identity, circuitTarget.transform);
                    Spawned2 = true;
                }
            }
        }
    }
   
}
