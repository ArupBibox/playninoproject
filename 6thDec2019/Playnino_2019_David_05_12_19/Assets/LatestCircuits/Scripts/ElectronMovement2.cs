﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronMovement2 : MonoBehaviour
{
    CircuitController dataHandler;
    public Transform[] target;
    public float speed;
    public int current;
    public Transform newSpawnPoint;
    public GameObject circuitTarget;
    bool Spawned = false;
    public GameObject LED;

    bool isNxtBttnShown;

    [SerializeField]
   // private pressButton buttonHandler;
  //  [SerializeField]
    CircuitController circuitHandler;

    CircuitsUiHandler circuitsUiHandlerAccess;
    // Start is called before the first frame update
    void Start()
    {
        circuitsUiHandlerAccess = FindObjectOfType<CircuitsUiHandler>();

        target = new Transform[10];
        target[0] = GameObject.Find("SCWP1").transform;
        target[1] = GameObject.Find("SCWP2").transform;
        target[2] = GameObject.Find("SCWP3").transform;
        target[3] = GameObject.Find("SCWP4").transform;
        target[4] = GameObject.Find("SCWP5").transform;
        target[5] = GameObject.Find("SCWP6").transform;
        target[6] = GameObject.Find("SCWP7").transform;
        target[7] = GameObject.Find("SCWP8").transform;
        target[8] = GameObject.Find("SCWP9").transform;
        target[9] = GameObject.Find("SCWP10").transform;
       // target[10] = GameObject.Find("SCWP11").transform;
       // target[11] = GameObject.Find("SCWP12").transform;
        
        newSpawnPoint = GameObject.Find("SCNewSpawnPoint").transform;
        circuitTarget = GameObject.Find("SCModels");

        LED = GameObject.Find("LEDLight");

        circuitHandler = FindObjectOfType<CircuitController>() as CircuitController;
       
        //dataHandler=find
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            MoveThroughPoints();
            SpawnNewElectron();
            DestroyElectrons();
            if (circuitHandler.endCircuitSC)
            {
                LED.GetComponent<Light>().intensity = 0;
                Destroy(gameObject);
            }
           
            HideElectrons();
            
          //  DestroyOnRelease();
        }

    }
    void MoveThroughPoints()
    {
        if (transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.fixedDeltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else
        {
            current = current + 1 % target.Length;
        }

    }
    void SpawnNewElectron()
    {
        if (!Spawned)
        {

            if (gameObject.transform.position.z > newSpawnPoint.transform.position.z)
            {
                GameObject sphereClone = Instantiate(gameObject, target[0].transform.position, Quaternion.identity, circuitTarget.transform) as GameObject;
                Spawned = true;
            }
        }
    }
    void DestroyElectrons()
    {
        if (current == target.Length)
        {

            ////  end animation
            LED.GetComponent<Light>().intensity = 200;
            Destroy(gameObject);
            if (!isNxtBttnShown)
            {
                circuitsUiHandlerAccess.ShowNextBttn(true);
                isNxtBttnShown = true;
            }


        }
       

    }
    void HideElectrons()
    {
        if ((current == 3) || (current == 6))
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        }
    }
   
}
