﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Battery : MonoBehaviour
{
    float offSet = 0;
    public bool batteryON;
    ElectronMovement dataHandler;
    public TMP_Text batteryText, LoadText;
    public float batteryVoltage, LoadVoltage;
    public Battery Battery1;
    public Battery Battery2;
    bool Batterytouched;
    // Start is called before the first frame update
    void Start()
    {
        dataHandler = FindObjectOfType<ElectronMovement>() as ElectronMovement;
        gameObject.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, -0.278f);
        GameObject go = GameObject.FindGameObjectWithTag("battery1");
        if (go != null)
        {
            Battery1 = go.GetComponent<Battery>();
        }
        go = GameObject.FindGameObjectWithTag("battery2");
        if (go != null)
        {
            Battery2 = go.GetComponent<Battery>();
        }
        batteryVoltage = 0;

    }

    // Update is called once per frame
    void Update()
    {
        batteryVoltage = Mathf.Round(batteryVoltage * 100f) / 100f;
        batteryText.text = batteryVoltage.ToString() + "V";
        LoadVoltage = Battery1.batteryVoltage + Battery2.batteryVoltage;
        LoadText.text = LoadVoltage + "V";
    }
    public void DrainBattery()
    {
        if (offSet > -0.278) //battery Material offset reducing condition
        {
            if ((Battery1.batteryON) && (Battery2.batteryON)) //Both batteries are ON
            {
                offSet -= 0.001f;
            }
            else if (((Battery1.batteryON) && (!Battery2.batteryON)) || ((!Battery1.batteryON) && (Battery2.batteryON)))
            //Only one of the batteries are ON
            {
                offSet -= 0.003f;
            }
            gameObject.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, offSet);
        }
        if (batteryVoltage > 0.78) // Set the voltage value
        {
            if ((Battery1.batteryON) && (Battery2.batteryON))
            //Both batteries are ON
            {
                batteryVoltage -= 0.009f;
            }
            else if (((Battery1.batteryON) && (!Battery2.batteryON)) || ((!Battery1.batteryON) && (Battery2.batteryON)))
            //Only one of the batteries are ON
            {
                batteryVoltage -= 0.025f;
            }
        }
    }
    private void OnMouseDown()
    {
        if (!Batterytouched)
        {
            batteryON = true;
            gameObject.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, 0);
            batteryVoltage = 3;
            Batterytouched = true;
        }
    }

}

