﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitController : MonoBehaviour
{
    public GameObject Electron, Electron2, Electron3, Electron4;
   // [SerializeField]
     pressButton buttonHandler;
    // [SerializeField]
    // ElectronMovement electronHandler;
    public bool created = false, createdSC = false, createdLS, createdParallel = false;
    public Transform spawnPoint, circuitTarget;
    public Transform spawnPoint2, circuitTarget2;
    public Transform spawnPoint3, circuitTarget3;
    public Transform spawnPoint4, circuitTarget4;
    public bool startCircuit, endCircuit;
    public bool startCircuitSC, endCircuitSC;
    public bool startCircuitLS, endCircuitLS;
    public bool startCircuitParallel, endCircuitParallel;
    public Battery batteryHandler;
   public Battery2 batteryHandler2;
    




    // Start is called before the first frame update
    void Start()
    {
       // electronHandler = FindObjectOfType<ElectronMovement>() as ElectronMovement;
        Electron  = Resources.Load("Electron") as GameObject;
        Electron2 = Resources.Load("Electron2") as GameObject;
        Electron3 = Resources.Load("Electron3") as GameObject;
        Electron4 = Resources.Load("Electron4") as GameObject;
       // batteryHandler = FindObjectOfType<Battery>() as Battery;
       // batteryHandler2 = FindObjectOfType<Battery2>() as Battery2;
        buttonHandler = FindObjectOfType<pressButton>() as pressButton;
        startCircuit = false;
        endCircuit = false;
        
    }

    // Update is called once per frame
    void Update()
    {

        createElectron1();
        createElectronSC();
        createElectronLS();
        createElectronParallel();
    }
    public void StartElectronFlow()
    {
        startCircuit = true;
        endCircuit = false;
    }
    public void EndElectronFlow()
    {
        if (created)
        {
            startCircuit = false;
            endCircuit = true;
        }
    }
    public void StartElectronFlowSC()
    {
        startCircuitSC = true;
        endCircuitSC = false;
    }
    public void EndElectronFlowSC()
    {
        if (createdSC)
        {
            startCircuitSC = false;
            endCircuitSC = true;
        }
    }
    public void StartElectronFlowLS()
    {
        startCircuitLS = true;
        endCircuitLS = false;
    }
    public void EndElectronFlowLS()
    {
        if (createdLS)
        {
            startCircuitLS = false;
            endCircuitLS = true;
        }
    }
    public void StartElectronFlowParallel()
    {
        startCircuitParallel = true;
        endCircuitParallel = false;
    }
    public void EndElectronFlowParallel()
    {
        if (createdParallel)
        {
            startCircuitParallel = false;
            endCircuitParallel = true;
        }
    }
    void createElectron1()
    {
        if (startCircuit)
        {
            Debug.Log("startCircuit");
            if (!created)
            {
                Debug.Log("!created");
                if ((batteryHandler.Battery1.batteryON) || (batteryHandler.Battery2.batteryON))
                {
                    Debug.Log("batteryHandler.Battery1.batteryON");
                    if ((batteryHandler.Battery1.batteryVoltage > 0.78f) || (batteryHandler.Battery2.batteryVoltage > 0.78f))
                    {
                        GameObject sphereClone = Instantiate(Electron, spawnPoint.transform.position, Quaternion.identity, circuitTarget.transform) as GameObject;
                        Debug.Log("batteryHandler.Battery1.batteryVoltage > 0.78f");
                        created = true;
                    }
                }
            }
        }
        else if (endCircuit)
        {
            // ended in electron script
            created = false;

        }
    }
    void createElectronSC()
    {
        if (startCircuitSC)
        {
            if (!createdSC)
            {
                GameObject sphereClone = Instantiate(Electron2, spawnPoint2.transform.position, Quaternion.identity, circuitTarget2.transform) as GameObject;

                createdSC = true;
            }
        }
        else if (endCircuitSC)
        {
            createdSC = false;
        }
    }
    void createElectronLS()
    {
        if (startCircuitLS)
        {
            if (!createdLS)
            {
                GameObject sphereClone = Instantiate(Electron3, spawnPoint3.transform.position, Quaternion.identity, circuitTarget3.transform) as GameObject;

                createdLS = true;
            }
        }
        else if (endCircuitLS)
        {
            createdLS = false;
        }
    }
    void createElectronParallel()
    {
        if (startCircuitParallel)
        {
            if (!createdParallel)
            {
                if ((batteryHandler2.Battery1.batteryON) || (batteryHandler2.Battery2nd.batteryON))
                {
                    if ((batteryHandler2.Battery1.batteryVoltage > 0.78f) || (batteryHandler2.Battery2nd.batteryVoltage > 0.78f))
                    {
                        GameObject sphereClone = Instantiate(Electron4, spawnPoint4.transform.position, Quaternion.identity, circuitTarget4.transform) as GameObject;

                        createdParallel = true;
                    }
                }
            }
        }
        else if (endCircuitParallel)
        {
            createdParallel = false;
        }
    }



}
