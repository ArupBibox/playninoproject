﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchControl : MonoBehaviour
{

    [SerializeField]
    private CircuitController circuitHandler;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        if (this.gameObject.name == "switchSC")
        {
            circuitHandler.StartElectronFlowSC();
        }
        if (this.gameObject.name == "switchLS")
        {
            circuitHandler.StartElectronFlowLS();
        }
    }
    private void OnMouseUp()
    {
        if (this.gameObject.name == "switchSC")
        {
            circuitHandler.EndElectronFlowSC();
        }
        if (this.gameObject.name == "switchLS")
        {
            circuitHandler.EndElectronFlowLS();
        }
    }
}
