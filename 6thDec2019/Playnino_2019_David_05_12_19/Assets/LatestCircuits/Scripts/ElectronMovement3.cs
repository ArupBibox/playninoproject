﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronMovement3 : MonoBehaviour
{
    CircuitController dataHandler;
    public Transform[] target;
    public float speed;
    public int current;
    public Transform newSpawnPoint;
    public GameObject circuitTarget;
    bool Spawned = false;
    public GameObject LED1, LED2;

    bool isNxtBttnShown;


    CircuitsUiHandler circuitsUiHandlerAccess;

    [SerializeField]
   // private pressButton buttonHandler;
  //  [SerializeField]
    CircuitController circuitHandler;
    

    // Start is called before the first frame update
    void Start()
    {
        circuitHandler = FindObjectOfType<CircuitController>() as CircuitController;
        circuitsUiHandlerAccess = FindObjectOfType<CircuitsUiHandler>();

        target = new Transform[12];
        target[0] = GameObject.Find("LSWP1").transform;
        target[1] = GameObject.Find("LSWP2").transform;
        target[2] = GameObject.Find("LSWP3").transform;
        target[3] = GameObject.Find("LSWP4").transform;
        target[4] = GameObject.Find("LSWP5").transform;
        target[5] = GameObject.Find("LSWP6").transform;
        target[6] = GameObject.Find("LSWP7").transform;
        target[7] = GameObject.Find("LSWP8").transform;
        target[8] = GameObject.Find("LSWP9").transform;
        target[9] = GameObject.Find("LSWP10").transform;
        target[10] = GameObject.Find("LSWP11").transform;
        target[11] = GameObject.Find("LSWP12").transform;
       // target[12] = GameObject.Find("LSWP13").transform;
       // target[13] = GameObject.Find("LSWP14").transform;
       // target[14] = GameObject.Find("LSWP15").transform;
       // target[15] = GameObject.Find("LSWP16").transform;

        newSpawnPoint = GameObject.Find("LSNewSpawnPoint").transform;
        circuitTarget = GameObject.Find("LSModels");

        LED1 = GameObject.Find("LEDLightLS1");
        LED2 = GameObject.Find("LEDLightLS2");

        
       
        //dataHandler=find
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            MoveThroughPoints();
            SpawnNewElectron();
            DestroyElectrons();
            if (circuitHandler.endCircuitLS)
            {
                LED1.GetComponent<Light>().intensity = 0;
                LED2.GetComponent<Light>().intensity = 0;
                Destroy(gameObject);
            }
           
            HideElectrons();
            
          //  DestroyOnRelease();
        }

    }
    void MoveThroughPoints()
    {
        if (transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.fixedDeltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else
        {
            current = current + 1 % target.Length;
        }

    }
    void SpawnNewElectron()
    {
        if (!Spawned)
        {

            if (gameObject.transform.position.z > newSpawnPoint.transform.position.z)
            {
                GameObject sphereClone = Instantiate(gameObject, target[0].transform.position, Quaternion.identity, circuitTarget.transform) as GameObject;
                Spawned = true;
            }
        }
    }
    void DestroyElectrons()
    {
        if (current == target.Length)
        {
            
            LED1.GetComponent<Light>().intensity = 200;
            LED2.GetComponent<Light>().intensity = 200;
            Destroy(gameObject);

            
            if (!isNxtBttnShown)
            {
                circuitsUiHandlerAccess.ShowNextBttn(true);
                isNxtBttnShown = true;
            }
            
           
        }
       

    }
    void HideElectrons()
    {
        if ((current == 3) || (current == 6) || (current == 8))
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        }
    }
   
}
