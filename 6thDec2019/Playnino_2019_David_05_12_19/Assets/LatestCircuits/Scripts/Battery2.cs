﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Battery2 : MonoBehaviour
{
    float offSet = 0;
    public bool batteryON;
    ElectronMovement4 dataHandler;
    public TMP_Text batteryText, LoadText;
    public float batteryVoltage, LoadVoltage;
    public Battery2 Battery1;
    public Battery2 Battery2nd;
    bool Batterytouched;
    // Start is called before the first frame update
    void Start()
    {
        dataHandler = FindObjectOfType<ElectronMovement4>() as ElectronMovement4;
        gameObject.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, -0.278f);
        GameObject go = GameObject.FindGameObjectWithTag("battery3");
        if (go != null)
        {
            Battery1 = go.GetComponent<Battery2>();
        }
        go = GameObject.FindGameObjectWithTag("battery4");
        if (go != null)
        {
            Battery2nd = go.GetComponent<Battery2>();
        }
        batteryVoltage = 0;
        LoadVoltage = 3;
      
    }

    // Update is called once per frame
    void Update()
    {
        batteryVoltage = Mathf.Round(batteryVoltage * 100f) / 100f;
        batteryText.text = batteryVoltage.ToString() + "V";
        LoadVoltage = batteryVoltage;
        if (((Battery1.batteryON) && (!Battery2nd.batteryON)))
        {
            LoadVoltage = Battery1.batteryVoltage;
        }
        if ((Battery1.batteryON) && (Battery2nd.batteryON))
        {
            LoadVoltage = Battery1.batteryVoltage;
        }

            LoadText.text = LoadVoltage+"V";
    }
    public void DrainBattery()
    {
        if (offSet > -0.278) //battery Material offset reducing condition
        {
            if ((Battery1.batteryON) && (Battery2nd.batteryON)) //Both batteries are ON
            {
              offSet -= 0.001f;
            }
            else if (((Battery1.batteryON) && (!Battery2nd.batteryON)) || ((!Battery1.batteryON) && (Battery2nd.batteryON)))
                //Only one of the batteries are ON
            {
                offSet -= 0.003f;
            }
            gameObject.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, offSet);
        }
        if (batteryVoltage > 0.78) // Set the voltage value
        {
            if ((Battery1.batteryON) && (Battery2nd.batteryON))
            //Both batteries are ON
            {
                batteryVoltage -= 0.009f;
               
            }
            else if (((Battery1.batteryON) && (!Battery2nd.batteryON)) || ((!Battery1.batteryON) && (Battery2nd.batteryON)))
            //Only one of the batteries are ON
            {
                batteryVoltage -= 0.025f;
                
            }
        }
    }
    private void OnMouseDown()
    {
        if (!Batterytouched)
        {
            batteryON = true;
            gameObject.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, 0);
            batteryVoltage = 3;
            Batterytouched = true;
        }
    }
    
}
