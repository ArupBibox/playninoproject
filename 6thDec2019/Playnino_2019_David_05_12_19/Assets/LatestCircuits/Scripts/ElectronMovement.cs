﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectronMovement : MonoBehaviour
{
    CircuitController dataHandler;
    public Transform[] target;
    public float speed;
    public int current;
    public Transform newSpawnPoint;
    bool Spawned = false;
    public GameObject circuitTarget;

    bool isNxtBttnShown;

    pressButton buttonHandler;
    [SerializeField]
   // private pressButton buttonHandler;
  //  [SerializeField]
    CircuitController circuitHandler;
    public Battery Battery1;
    public Battery Battery2;

    CircuitsUiHandler circuitsUiHandlerAccess;
    // Start is called before the first frame update
    void Start()
    {
        circuitsUiHandlerAccess = FindObjectOfType<CircuitsUiHandler>();

        target = new Transform[14];
        target[0] = GameObject.Find("wp1").transform;
        target[1] = GameObject.Find("wp2").transform;
        target[2] = GameObject.Find("wp3").transform;
        target[3] = GameObject.Find("wp4").transform;
        target[4] = GameObject.Find("wp5").transform;
        target[5] = GameObject.Find("wp6").transform;
        target[6] = GameObject.Find("wp7").transform;
        target[7] = GameObject.Find("wp8").transform;
        target[8] = GameObject.Find("wp9").transform;
        target[9] = GameObject.Find("wp10").transform;
        target[10] = GameObject.Find("wp11").transform;
        target[11] = GameObject.Find("wp12").transform;
        target[12] = GameObject.Find("wp13").transform;
        target[13] = GameObject.Find("wp14").transform;
        newSpawnPoint = GameObject.Find("NewSpawnPoint").transform;
        circuitTarget = GameObject.Find("Models");
        buttonHandler = FindObjectOfType<pressButton>() as pressButton;
        circuitHandler = FindObjectOfType<CircuitController>() as CircuitController;
        GameObject go = GameObject.FindGameObjectWithTag("battery1");
        if (go != null)
        {
            Battery1 = go.GetComponent<Battery>();
        }
        go = GameObject.FindGameObjectWithTag("battery2");
        if (go != null)
        {
            Battery2 = go.GetComponent<Battery>();
        }
        //dataHandler=find
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            MoveThroughPoints();
            SpawnNewElectron();
            DestroyElectrons();
            if (circuitHandler.endCircuit)
            {
                Destroy(gameObject);
            }
            if ((Battery1.batteryVoltage == 0.78f) && (Battery1.batteryVoltage == 0.78f))
            {
                Destroy(gameObject);
            }
            HideElectrons();
            
          //  DestroyOnRelease();
        }

    }
    void MoveThroughPoints()
    {
        if (transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[current].position, speed * Time.fixedDeltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else
        {
            current = current + 1 % target.Length;
        }

    }
    void SpawnNewElectron()
    {
        if (!Spawned)
        {

            if (gameObject.transform.position.z > newSpawnPoint.transform.position.z)
            {
                GameObject sphereClone = Instantiate(gameObject, target[0].transform.position, Quaternion.identity, circuitTarget.transform) as GameObject;
                Spawned = true;
            }
        }
    }
    void DestroyElectrons()
    {
        if (current == target.Length)
        {
            if (Battery1.batteryON)
            {
                Battery1.DrainBattery(); 
            }
            if (Battery2.batteryON)
            {
                Battery2.DrainBattery();
            }
            if (this.gameObject.name != "Electron") 
            Destroy(gameObject);

            if (!isNxtBttnShown)
            {
                circuitsUiHandlerAccess.ShowNextBttn(true);
                isNxtBttnShown = true;
            }
        }

    }
    void HideElectrons()
    {
        if ((current == 4) || (current == 10))
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        }
    }
   
}
