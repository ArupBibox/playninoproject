﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomBarAnim : MonoBehaviour {
    float buttonNum = 0;
    public Button[] buttons;
    public Sprite[] profbuttons, kitbuttons, homebuttons, btbuttons, infobuttons;
    public Animator[] anim;
    public Text[] buttontexts;
    public static bool btAnimDown = false;

    int exitCounter;
    bool startTimer, msgToasted, profileBttnPressed;
    public float timeLeft;

    //For Toast
    public Image toastImg;
    public Text toastTxt;

    SlidingPanel slidingPanelScriptAccess;


    // Use this for initialization
    void Start () {

        slidingPanelScriptAccess = GameObject.FindObjectOfType<SlidingPanel>();

        timeLeft = 3f;
        exitCounter = 0;
        startTimer = false;
        msgToasted = false;
        profileBttnPressed = false;

        buttons[0].GetComponent<Image>().sprite = profbuttons[0];     //initialise the buttton images
        buttons[1].GetComponent<Image>().sprite = kitbuttons[0];
        buttons[2].GetComponent<Image>().sprite =homebuttons[1];
        buttons[3].GetComponent<Image>().sprite = btbuttons[0];
        buttons[4].GetComponent<Image>().sprite = infobuttons[0];
        buttontexts[0].GetComponent<Text>().enabled = false;          //hide the button names at the beginning
        buttontexts[1].GetComponent<Text>().enabled = false;
        buttontexts[2].GetComponent<Text>().enabled = true;
        buttontexts[3].GetComponent<Text>().enabled = false;
        buttontexts[4].GetComponent<Text>().enabled = false;
        anim[2].Play("HomeAnim");
        buttonNum = 3;
        

    }
	
	// Update is called once per frame
	void Update () {
        //bluetoothPopDown();//to animate bluetooth button when bluetooth panel Pops down

        //Check before exit the app
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(buttonNum == 3 && !profileBttnPressed)
            {
                exitCounter++;
                startTimer = true;
            }
            else
            {
                SlideToHomeScreen();
            }
        }
        if (startTimer)
        { 
            timeLeft -= Time.deltaTime;
            if (timeLeft <= 0)
            {
                startTimer = false;
                exitCounter = 0;
                timeLeft = 3f;
                msgToasted = false;
            }
        }
        if(exitCounter == 1 && !msgToasted)
        {
            showToast("Press again to exit", 1);
            msgToasted = true;
        }
        if(exitCounter == 2)
        {
            startTimer = false;
            exitCounter = 0;
            timeLeft = 3f;
            msgToasted = false;
            Application.Quit();
        }
	}

    void SlideToHomeScreen()
    {
       // slidingPanelScriptAccess.HomePanelClick();
        HomeAnim();
    }

    public void ProfileBttnClicked()
    {
       // buttonNum = 6;
        profileBttnPressed = true;
    }
    public void ProfileBackBttnClicked()
    {
        profileBttnPressed = false;
    }
    
    public void profAnim()     // calls when the profile button is clicked,animates the profile button when clicked
    {
        buttons[0].GetComponent<Image>().sprite = profbuttons[1];
        buttontexts[0].GetComponent<Text>().enabled = true;
        anim[0].Play("profbuttonAnim");
        if (buttonNum == 2)
        {
            anim[1].Play("KitsAnim 0");
            buttons[1].GetComponent<Image>().sprite = kitbuttons[0];
            buttontexts[1].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 3)
        {
            anim[2].Play("HomeAnim 0");
            buttons[2].GetComponent<Image>().sprite = homebuttons[0];
            buttontexts[2].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 4)
        {
            anim[3].Play("bluetoothAnim 0");
            buttons[3].GetComponent<Image>().sprite = btbuttons[0];
            buttontexts[3].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 5)
        {
            anim[4].Play("infoAnim 0");
            buttons[4].GetComponent<Image>().sprite = infobuttons[0];
            buttontexts[4].GetComponent<Text>().enabled = false;
        }
        buttonNum = 1;
        profileBttnPressed = false;

    }
    public void kitsanim() //animates the kits button when clicked
    {
        buttons[1].GetComponent<Image>().sprite = kitbuttons[1];
        buttontexts[1].GetComponent<Text>().enabled = true;
        anim[1].Play("KitsAnim");
        if (buttonNum == 1)
        {
            anim[0].Play("profbuttonAnim 0");
            buttons[0].GetComponent<Image>().sprite = profbuttons[0];
            buttontexts[0].GetComponent<Text>().enabled = false;
        }
         if (buttonNum == 3)
        {
            anim[2].Play("HomeAnim 0");
            buttons[2].GetComponent<Image>().sprite = homebuttons[0];
            buttontexts[2].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 4)
        {
            anim[3].Play("bluetoothAnim 0");
            buttons[3].GetComponent<Image>().sprite = btbuttons[0];
            buttontexts[3].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 5)
        {
            anim[4].Play("infoAnim 0");
            buttons[4].GetComponent<Image>().sprite = infobuttons[0];
            buttontexts[4].GetComponent<Text>().enabled = false;
        }
        buttonNum = 2;
    }
    public void HomeAnim() // animates the home button when clicked 
    {
       
        if (buttonNum != 0) 
        {
            buttons[2].GetComponent<Image>().sprite = homebuttons[1];
            buttontexts[2].GetComponent<Text>().enabled = true;
            anim[2].Play("HomeAnim");
            
        }
        if (buttonNum == 1)  //animates the previous button back to normal based on button number
        {
            anim[0].Play("profbuttonAnim 0");
            buttons[0].GetComponent<Image>().sprite = profbuttons[0];
            buttontexts[0].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 2)
        {
            anim[1].Play("KitsAnim 0");
            buttons[1].GetComponent<Image>().sprite = kitbuttons[0];
            buttontexts[1].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 4)
        {
            anim[3].Play("bluetoothAnim 0");
            buttons[3].GetComponent<Image>().sprite = btbuttons[0];
            buttontexts[3].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 5)
        {
            anim[4].Play("infoAnim 0");
            buttons[4].GetComponent<Image>().sprite = infobuttons[0];
            buttontexts[4].GetComponent<Text>().enabled = false;
        }
        if (buttonNum != 0)
        {
            buttonNum = 3;
            profileBttnPressed = false;
        }
    }
    public void bluetoothAnim() //animates the bletooth button when clicked
    {
        buttons[3].GetComponent<Image>().sprite = btbuttons[1];
        buttontexts[3].GetComponent<Text>().enabled = true;
        anim[3].Play("bluetoothAnim");

        if (buttonNum == 1) // animates the previous button back to normal size based on the button number 
        {
            anim[0].Play("profbuttonAnim 0");
            buttons[0].GetComponent<Image>().sprite = profbuttons[0];
            buttontexts[0].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 2)
        {
            anim[1].Play("KitsAnim 0");
            buttons[1].GetComponent<Image>().sprite = kitbuttons[0];
            buttontexts[1].GetComponent<Text>().enabled = false;
        }

        if (buttonNum == 3)
        {
            anim[2].Play("HomeAnim 0");
            buttons[2].GetComponent<Image>().sprite = homebuttons[0];
            buttontexts[2].GetComponent<Text>().enabled = false; ;
        }
        if (buttonNum == 5)
        {
            anim[4].Play("infoAnim 0");
            buttons[4].GetComponent<Image>().sprite = infobuttons[0];
            buttontexts[4].GetComponent<Text>().enabled = false;
        }
        buttonNum = 4;
    }
    public void infoAnim() //animates the info button when clicked 
    {
        buttons[4].GetComponent<Image>().sprite = infobuttons[1];
        buttontexts[4].GetComponent<Text>().enabled = true;
        anim[4].Play("infoAnim");
        if (buttonNum == 1)
        {
            anim[0].Play("profbuttonAnim 0");
            buttons[0].GetComponent<Image>().sprite = profbuttons[0];
            buttontexts[0].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 2)
        {
            anim[1].Play("KitsAnim 0");
            buttons[1].GetComponent<Image>().sprite = kitbuttons[0];
            buttontexts[1].GetComponent<Text>().enabled = false;
        }

        if (buttonNum == 3)
        {
            anim[2].Play("HomeAnim 0");
            buttons[2].GetComponent<Image>().sprite = homebuttons[0];
            buttontexts[2].GetComponent<Text>().enabled = false;
        }
        if (buttonNum == 4)
        {
            anim[3].Play("bluetoothAnim 0");
            buttons[3].GetComponent<Image>().sprite = btbuttons[0];
            buttontexts[3].GetComponent<Text>().enabled = false;
        }
        buttonNum = 5;
        profileBttnPressed = false;

    }
    void bluetoothPopDown() //to animate bluetooth button when bluetooth panel Pops down

    {
        if (!btAnimDown)
        {
            anim[3].Play("bluetoothAnim 0");
            btAnimDown = true;
            buttons[3].GetComponent<Image>().sprite = btbuttons[0];
            buttontexts[3].GetComponent<Text>().enabled = false;
        }
    }

    #region ToastMessage

    void showToast(string text, int duration)
    {
        StartCoroutine(showToastCOR(text, duration));
    }

    private IEnumerator showToastCOR(string text, int duration)
    {
        Color orginalImgColor = toastImg.color;
        Color orginalTxtColor = toastTxt.color;

        toastImg.enabled = true;

        toastTxt.text = text;
        toastTxt.enabled = true;

        //Fade in
        yield return fadeInAndOut1(toastImg, toastTxt, true, 0.5f);

        //Wait for the duration
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        //Fade out
        yield return fadeInAndOut1(toastImg, toastTxt, false, 0.5f);

        toastImg.enabled = false;
        toastImg.color = orginalImgColor;

        toastTxt.enabled = false;
        toastTxt.color = orginalTxtColor;
    }

    IEnumerator fadeInAndOut1(Image targetImg, Text targetTxt, bool fadeIn, float duration)
    {
        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0f;
            b = 1f;
        }
        else
        {
            a = 1f;
            b = 0f;
        }

        Color currentImgColor = toastImg.color;
        Color currentTxtColor = Color.clear;
        float counter = 0f;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            targetImg.color = new Color(currentImgColor.r, currentImgColor.g, currentImgColor.b, alpha);
            targetTxt.color = new Color(currentTxtColor.r, currentTxtColor.g, currentTxtColor.b, alpha);
            yield return null;
        }
    }

    #endregion


}
