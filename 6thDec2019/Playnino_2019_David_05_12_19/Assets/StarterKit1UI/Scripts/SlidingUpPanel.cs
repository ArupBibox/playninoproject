﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Video;

public class SlidingUpPanel : MonoBehaviour
{
    public RectTransform slidingUpPanel;
    Vector2 centerPosition,minimizedPosition;
    public GameObject swipeUpText,paperCircuitPanel,tinkerKitPanel,watchVideoButton,popup,slider,firstBigButton;//
    public VideoPlayer videoPlayer;
    public Image arrowImage;
    public GameObject[] lockedButtonImage,lockedButtonImagesSecondPanel;
    public Button[] lockedButtons,lockedButtonSecondPanel;
    public Sprite upArrow, downArrow;
    public RawImage videoRenderingImage;
    public float duration;
    int numberOfButtonsUnlockedInFirstPanel,currentVideo,currentPanel, numberOfButtonsUnlockedInSecondPanel,currentVideoSecondPanel, SwipeDirection;
    public VideoClip[] firstPanelVideos,secondPanelVideo;
    StarterKitOneMainScript kitOneScriptHandler;
    Vector2 direction, lastPosition;
    bool fingerDown, isSwiping,up, readyToSwipe;
    Vector2 touchStartPosition;
    public Transform swipeUpLimit;

    public GameObject kitOneMainUIPanel;
    bool isInKit1UIScene;

    SliderSelectOption sliderOptionScript;
    StarterKitOneMainScript starterKitMainScriptHandler;
    ProfileHandler profileHandlerScript;
    // Start is called before the first frame update
    void Start()
    {
        isInKit1UIScene = true;
        sliderOptionScript = FindObjectOfType<SliderSelectOption>() as SliderSelectOption;
        kitOneScriptHandler = FindObjectOfType<StarterKitOneMainScript>() as StarterKitOneMainScript;
        starterKitMainScriptHandler = FindObjectOfType<StarterKitOneMainScript>() as StarterKitOneMainScript;
        profileHandlerScript = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        popup.SetActive(false);
        currentPanel = 0;
        up = false;
        currentVideo = 0;
        currentVideoSecondPanel = 0;
        numberOfButtonsUnlockedInFirstPanel = PlayerPrefs.GetInt("numberOfButtonsUnlockedInFirstPanel");
        numberOfButtonsUnlockedInSecondPanel = PlayerPrefs.GetInt("numberOfButtonsUnlockedInSecondPanel");
        watchVideoButton.SetActive(false);
        centerPosition.Set(0, 0);
        minimizedPosition.Set(0, -1180f);
        SlideDown();
        videoPlayer.clip = firstPanelVideos[0];
        videoPlayer.loopPointReached += EndReached;
        firstBigButton.SetActive(false);

        if (numberOfButtonsUnlockedInFirstPanel >= 4)
            slider.SetActive(true);
        else
            slider.SetActive(false);

        for (int i = 0; i < lockedButtons.Length; i++)//this is for paper circuit panel
        {
            lockedButtonImage[i].SetActive(true);
            lockedButtons[i].interactable = false;
        }
        for (int i = 0; i < lockedButtonSecondPanel.Length; i++)//this is for tinker project panel
        {
            lockedButtonImagesSecondPanel[i].SetActive(true);
            lockedButtonSecondPanel[i].interactable = false;
        }
        tinkerKitPanel.SetActive(false);


        
    }
    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        if (currentPanel == 0)
        {
            if (numberOfButtonsUnlockedInFirstPanel < 4)
            {
                numberOfButtonsUnlockedInFirstPanel++;
                PlayerPrefs.SetInt("numberOfButtonsUnlockedInFirstPanel", numberOfButtonsUnlockedInFirstPanel);
                PlayerPrefs.Save();
                if (numberOfButtonsUnlockedInFirstPanel < 4)
                {
                    videoPlayer.clip = firstPanelVideos[numberOfButtonsUnlockedInFirstPanel];
                    videoPlayer.Play();
                }

            }
        }
        else if(currentPanel==1)
        {
            if (numberOfButtonsUnlockedInSecondPanel < 5)
            {
                numberOfButtonsUnlockedInSecondPanel++;
                PlayerPrefs.SetInt("numberOfButtonsUnlockedInSecondPanel", numberOfButtonsUnlockedInSecondPanel);
                PlayerPrefs.Save();
                if (numberOfButtonsUnlockedInSecondPanel < 5)
                {
                    videoPlayer.clip = secondPanelVideo[numberOfButtonsUnlockedInSecondPanel];
                    videoPlayer.Play();
                }

            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        //testing...
        //testing...
        if(kitOneMainUIPanel.activeSelf)
        {
            isInKit1UIScene = true;
        }
        else
        {
            isInKit1UIScene = false;
        }
        if (Input.GetKeyDown(KeyCode.W))
            SlideUp();
        if (Input.GetKeyDown(KeyCode.S))
            SlideDown();
        if (Input.GetKeyDown(KeyCode.D))
        {
            numberOfButtonsUnlockedInFirstPanel++;
            videoPlayer.clip = firstPanelVideos[numberOfButtonsUnlockedInFirstPanel];
            videoPlayer.Play();
        }

        videoRenderingImage.texture = videoPlayer.texture;
        UnlockButtons();
        if(numberOfButtonsUnlockedInFirstPanel == 4 && kitOneScriptHandler.numberOfButtonsUnlocked==0)
        {
            kitOneScriptHandler.numberOfButtonsUnlocked = 1;

            PlayerPrefs.SetInt("numberOfButtonsUnlockedInMainPanel", kitOneScriptHandler.numberOfButtonsUnlocked);
            PlayerPrefs.Save();
            kitOneScriptHandler.isARLocked = false;
        }
        if(numberOfButtonsUnlockedInSecondPanel==5)
        {
            watchVideoButton.SetActive(true);
        }
        DetectSwipe();
        if (Input.GetKeyDown(KeyCode.Escape) &&isInKit1UIScene)
        {
            if (up)
                SlideDown();
            else
                starterKitMainScriptHandler.BackButtonClick();
        }
    }

    public void SlideUp()
    {
       
            if (kitOneScriptHandler.numberOfButtonsUnlocked >= 4)
            {
                tinkerKitPanel.SetActive(true);
                paperCircuitPanel.SetActive(false);
                currentPanel = 1;
            }
            else
            {
                paperCircuitPanel.SetActive(true);
                tinkerKitPanel.SetActive(false);
                currentPanel = 0;
            }
            if (currentPanel == 0)
                videoPlayer.clip = firstPanelVideos[currentVideo];
            else if (currentPanel == 1)
                videoPlayer.clip = secondPanelVideo[currentVideoSecondPanel];
            videoPlayer.Play();
            slidingUpPanel.DOAnchorPos(centerPosition, duration);
            arrowImage.sprite = downArrow;
            swipeUpText.SetActive(false);
        up = true;


    }
    public void SlideDown()
    {

            videoPlayer.Stop();
            slidingUpPanel.DOAnchorPos(minimizedPosition, duration);
            arrowImage.sprite = upArrow;
            swipeUpText.SetActive(true);
        up = false;


    }
    void UnlockButtons()
    {
        if(currentPanel==0)
        {
            if (numberOfButtonsUnlockedInFirstPanel <= 4)
            {
                for (int i = 0; i < numberOfButtonsUnlockedInFirstPanel; i++)
                {
                    if(lockedButtonImage[i].activeSelf)
                        lockedButtonImage[i].SetActive(false);
                    if(lockedButtons[i].interactable==false)
                        lockedButtons[i].interactable = true;
                }
            }
            if (numberOfButtonsUnlockedInFirstPanel >= 4 && PlayerPrefs.GetInt("introOver")!=1)
            {
                PlayerPrefs.SetInt("introOver", 1);
                PlayerPrefs.Save();
                SlideDown();
                //profileHandlerScript.ChangeCurrentProjectInDB(1);
                slider.SetActive(true);
                firstBigButton.SetActive(true);
                Invoke("GrowBackground", 2f);
            }
        }
        else if(currentPanel==1)
        {
            if (numberOfButtonsUnlockedInSecondPanel <= 5)
            {
                for (int i = 0; i < numberOfButtonsUnlockedInSecondPanel; i++)
                {
                    if (lockedButtonImagesSecondPanel[i].activeSelf)
                        lockedButtonImagesSecondPanel[i].SetActive(false);
                    if (lockedButtonSecondPanel[i].interactable == false)
                        lockedButtonSecondPanel[i].interactable = true;
                }
            }
        }
        
    }
    public void GrowBackground()
    {
        sliderOptionScript.down = true;
        Invoke("ShrinkBackground", 1f);
    }
    public void ShrinkBackground()
    {
        sliderOptionScript.down = false;

    }
    public void OnFirstPanelButonClick(int number)
    {
        currentVideo = number;
        videoPlayer.clip = firstPanelVideos[currentVideo];
        videoPlayer.Play();
    }
    public void OnSecondPanelButtonClick(int number)
    {
        currentVideoSecondPanel = number;
        videoPlayer.clip = secondPanelVideo[currentVideoSecondPanel];
        videoPlayer.Play();
    }
    public void TinkerProjectButtonClick()
    {
        if(kitOneScriptHandler.isTinkerButtonActivated)
        {
            currentPanel = 1;

            currentVideoSecondPanel = 0;
            videoPlayer.clip = secondPanelVideo[0];
            videoPlayer.Play();
            paperCircuitPanel.SetActive(false);
            tinkerKitPanel.SetActive(true);
        }
        else
        {
            //show popup
            popup.SetActive(true);
            Invoke("ClosePopUp", 1.5f);
        }

    }
    void ClosePopUp()
    {
        popup.SetActive(false);

    }
    public void paperKitButtonClick()
    {
        currentPanel = 0;

        currentVideo = 0;
        videoPlayer.clip = firstPanelVideos[0];
        videoPlayer.Play();
        paperCircuitPanel.SetActive(true);
        tinkerKitPanel.SetActive(false);
    }

    void DetectSwipe()
    {
        if (Input.touchCount > 0)
        {
            fingerDown = true;
            if(Input.touches[0].phase==TouchPhase.Began)
            {
                touchStartPosition = Input.touches[0].position;
            }
        }
        else
            fingerDown = false;
        if (Input.touchCount != 0)
        {

            if (Input.GetTouch(0).deltaPosition.sqrMagnitude != 0)
            {
                if (isSwiping == false)
                {
                    isSwiping = true;
                    lastPosition = Input.GetTouch(0).position;
                    return;
                }
                else
                {
                    direction = Input.GetTouch(0).position - lastPosition;

                    if (direction.y > 200)
                    {

                        //Debug.Log("UP!");
                        SwipeDirection = 1;
                        if (touchStartPosition.y < swipeUpLimit.position.y) 
                        {
                            SlideUp();
                        }
                    }
                    else if(direction.y<-200)
                    {
                        //Debug.Log("DOWN!");
                        SwipeDirection = -1;
                        SlideDown();
                    }


                }

            }
            else
            {
                isSwiping = false;
                SwipeDirection = 0;
            }
        }
    }
    public void WatchConstructionVideo()
    {
        Application.OpenURL("https://www.youtube.com");
    }

    public void OpenSlidingUpPanelButton()
    {
        if (up)
            SlideDown();
        else
            SlideUp();
    }
    public void SetReadToSwipe(bool condition)
    {
        readyToSwipe = condition;
    }
    
}
