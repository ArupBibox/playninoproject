﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Vuforia;

public class StarterKitOneMainScript : MonoBehaviour
{
    public GameObject[] lockedButtonImages;
    public Button[] lockedButtons;
    public int numberOfButtonsUnlocked,currentProject;
    public static int currentARSceneSelected;
    public GameObject tinkerButtonLockedImage,mainCanvas;
    public bool isARLocked;
    public Slider openArSlider;
    public SliderSelectOption selectOptionScript;
    PopUpHandler popupHandlerScriptAccess;
   public  bool isTinkerButtonActivated;
    DownloadAndLoad downloadAndLoadScriptAccess;
    public GameObject sceneCamera;
    KitsHandler kitsHandlerScript;
    public GameObject[] bigButtons;
  
    // Start is called before the first frame update
    void Start()
    {
        selectOptionScript = FindObjectOfType<SliderSelectOption>() as SliderSelectOption;
        popupHandlerScriptAccess = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        kitsHandlerScript = FindObjectOfType<KitsHandler>() as KitsHandler;
        isARLocked = true;
        downloadAndLoadScriptAccess = FindObjectOfType<DownloadAndLoad>() as DownloadAndLoad;
        currentProject = PlayerPrefs.GetInt("currentProject");
        currentARSceneSelected = 1;
        mainCanvas.SetActive(true);
        isTinkerButtonActivated = false;
        numberOfButtonsUnlocked = PlayerPrefs.GetInt("numberOfButtonsUnlockedInMainPanel");
        for (int i = 0; i < lockedButtons.Length; i++)
        {
            lockedButtonImages[i].SetActive(true);
            lockedButtons[i].interactable = false;
        }

        if (numberOfButtonsUnlocked > 0)
        {
            isARLocked = false;
            //firstKitButton.SetActive(true);
            currentARSceneSelected = currentProject;
            bigButtons[Mathf.Clamp(currentProject - 1, 1, 4)].SetActive(true);
            for (int i = 0; i < numberOfButtonsUnlocked; i++)
            {
                lockedButtonImages[i].SetActive(false);
                lockedButtons[i].interactable = true;
            }
        }
        
        tinkerButtonLockedImage.SetActive(true);
        sceneCamera.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if(numberOfButtonsUnlocked>=4)
        {
            isTinkerButtonActivated = true;
            tinkerButtonLockedImage.SetActive(false);
        }
        if(isARLocked)
        {
            openArSlider.interactable = false;
            selectOptionScript.enabled = false;
            //sliderLockedImage.SetActive(true);
        }
        else
        {
            selectOptionScript.enabled = true;
            openArSlider.interactable = true;
           // sliderLockedImage.SetActive(false);

        }
        UnlockButtons();
       
    }

    public void KitOneMainPanelButtonClick(int number)
    {
        currentARSceneSelected = number;
    }


    public void BackButtonClick()
    {

        popupHandlerScriptAccess.mainUI.SetActive(true);
        popupHandlerScriptAccess.isArSceneOpened = false;
        downloadAndLoadScriptAccess.UnloadBundle();
        //VuforiaRuntime.Instance.Deinit();
        if(VuforiaRuntime.Instance.InitializationState== VuforiaRuntime.InitState.INITIALIZED)
         {

            VuforiaRuntime.Instance.Deinit();
        }
        SceneManager.UnloadSceneAsync("StarterKit1UI_v1.0.7");

    }
    void UnlockButtons()
    {
        if (numberOfButtonsUnlocked <= 4)
        {
            for (int i = 0; i < numberOfButtonsUnlocked; i++)
            {
                lockedButtonImages[i].SetActive(false);
                lockedButtons[i].interactable = true;
            }
        }
    }


    public void ToArScene()
    {
        Screen.orientation = ScreenOrientation.Landscape;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        
        isARLocked = true;
        mainCanvas.SetActive(false);
       
    }
    public void BackFromArScene()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        
        isARLocked = false;
        mainCanvas.SetActive(true);

        
        selectOptionScript.ResetSlider();
        PlayerPrefs.SetInt("numberOfButtonsUnlockedInMainPanel", numberOfButtonsUnlocked);
        PlayerPrefs.Save();
    }
    public void ButtonUnlockingFunction(int number)
    {
        PlayerPrefs.SetInt("currentProject",Mathf.Clamp(number + 1,0,4));
        PlayerPrefs.Save();
       for(int i=1;i<=4;i++)
        {
            if (i == Mathf.Clamp(number + 1, 0, 4))
                bigButtons[i-1].SetActive(true);
            else
                bigButtons[i-1].SetActive(false);
        }
        currentARSceneSelected = Mathf.Clamp(number + 1, 0, 4);
        if (numberOfButtonsUnlocked <= number)
        {
            if (number + 1 <= 4)
            {
                numberOfButtonsUnlocked = Mathf.Clamp(number + 1, 0, 4);

            }
        }
    }
    //testing....
    public void Back()
    {
        isARLocked = false;
        mainCanvas.SetActive(true);

        numberOfButtonsUnlocked++;
        selectOptionScript.ResetSlider();
        PlayerPrefs.SetInt("numberOfButtonsUnlockedInMainPanel", numberOfButtonsUnlocked);
        PlayerPrefs.Save();
    }

    
}
