﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class RemoveUnwantedComponents : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        VuforiaBehaviour vufroiaBehaviour = gameObject.GetComponent<VuforiaBehaviour>();
        if(vufroiaBehaviour!=null)
        {
            Destroy(vufroiaBehaviour);

        }
        
    }
}
