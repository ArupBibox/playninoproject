﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Android;
using Vuforia;

public class SliderSelectOption : MonoBehaviour,IPointerDownHandler,IPointerUpHandler
{

    public float sliderMaxScale = 2;
    public float growSmoothValue=2;
    public RectTransform backGroundImage;
    public Slider optionSelector;
    float currentValue;
    bool valueSelected;
   public  bool down;
   public bool grow;

    DownloadAndLoad downloadAndLoadScriptAccess;
    StarterKitOneMainScript kitOneScriptHandler;

    // Start is called before the first frame update
    void Start()
    {
        grow = false;
        currentValue = 0;
        down = false;
        valueSelected = false;
        optionSelector.interactable = true;
        kitOneScriptHandler = FindObjectOfType<StarterKitOneMainScript>() as StarterKitOneMainScript;
        downloadAndLoadScriptAccess = FindObjectOfType<DownloadAndLoad>() as DownloadAndLoad;

    }

    // Update is called once per frame
    void Update()
    {
       if(down)
        {
            if (currentValue < sliderMaxScale)
            {
                currentValue += Time.deltaTime * growSmoothValue;

            }
            else
            {
                currentValue = sliderMaxScale;
            }
        }
        else
        {
            if(!valueSelected)
            {
                if (currentValue > 0)
                {
                    currentValue -= Time.deltaTime * growSmoothValue;

                }
                else
                {
                    currentValue = 0;
                }
            }
          
        }

       
        currentValue = Mathf.Clamp(currentValue, 0, sliderMaxScale);
        backGroundImage.localScale = new Vector3(currentValue, 1, 1);
        if(valueSelected)////this is optinal completly based on the need
        {
            optionSelector.interactable = false;
        }
        
    }



    public void OnPointerDown(PointerEventData eventData)
    {
        ///here animate the slider to full image
        ///search for selected option
        ///call the respective function
        Debug.Log("Pointer Down!");
        //grow = !grow;

        down = true;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        ///here animate the slider to back
        ///
        //Debug.Log("Pointer Up!");
        down = false;
        Debug.Log("on pointer up!");
        if (optionSelector.value >= 0.7f)
        {
            optionSelector.value = 1;
            valueSelected = true;
            //AR selected
            OpenARScene();
        }
        else if (optionSelector.value <= -0.7f)
        {
            optionSelector.value = -1;
            valueSelected = true;
            //VR selected
            OpenVR();

        }
        else
            optionSelector.value = 0;


    }


    public void ResetSlider()
    {
        valueSelected = false;
        currentValue = 0;
        //down = false;
        valueSelected = false;
        optionSelector.interactable = true;
        optionSelector.value = 0;
    }

    void OpenARScene()
    {
        Debug.Log("Open Ar scene"+StarterKitOneMainScript.currentARSceneSelected);
        //load ar scene
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
        Invoke("LoadScene", 1);
       
    }


    void LoadScene()
    {
        if (Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            if(VuforiaRuntime.Instance.InitializationState==VuforiaRuntime.InitState.NOT_INITIALIZED)
            {
                VuforiaRuntime.Instance.InitVuforia();
                Debug.Log("init");
            }
            //Invoke("OpenArScene", 1.3f);
            //SceneManager.LoadSceneAsync("ABtesting_NewMarker", LoadSceneMode.Additive);
            //downloadAndLoadScriptAccess.openArSceneRequest = true;
            downloadAndLoadScriptAccess.LoadSceneFromBundle("ABtesting_NewMarker");
            kitOneScriptHandler.ToArScene();

        }
        else
        {
            ResetSlider();
        }
    }
    void OpenVR()
    {
        Application.OpenURL("https://www.youtube.com");
        ResetSlider();
    }

   

   
}
