﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class brainController : MonoBehaviour {

    Animator brain;
    public static bool teared = false;
    public GameObject videoButton;
    public GameObject iamInterested;
   // public GameObject scanCircle;
    public GameObject instructions;
	// Use this for initialization
	void Start () {
        brain = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            //scanCircle.SetActive(false);
            instructions.SetActive(false);
        }
		
	}
    private void OnMouseDown()
    {
        if (!teared)
        {
            brain.SetBool("tear", true);
            teared = true;
            videoButton.SetActive(true);
            iamInterested.SetActive(true);
        }
        else
        {
            brain.SetBool("tear", false);
            teared = false;
        }
    }
}
