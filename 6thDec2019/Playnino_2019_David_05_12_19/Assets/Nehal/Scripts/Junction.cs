﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Junction : MonoBehaviour
{


    public enum JunctionType {L,T,Plus,End,Led,Switch,T2,L2,flowControl,switchExit,L3,ledExit,firstL};
    public Transform movePosition;
    public JunctionType junctionType;
    public GameObject debugCube;
    int plusCount,tCount,t2Count;
    public GameObject electron;
    public CircuitManager circuitManager;
    public ButtonClick button1,button2;
    public bool firstOne;
    public static bool once,once2;

    // Start is called before the first frame update
    void Start()
    {
        once = true;
        circuitManager = FindObjectOfType<CircuitManager>() as CircuitManager;
        tCount = 0;
        plusCount = 0;
        debugCube = transform.GetChild(1).gameObject;
        debugCube.SetActive(false);

        debugCube.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

       
           


    }
    private void OnTriggerEnter(Collider other)//
    {
        switch (junctionType)
        {
            case JunctionType.L:
                {
                   //rotate the electron 90 degree
                    Transform temp = other.transform.GetComponent<Transform>();
                    bool check = other.transform.GetComponent<MoveForward>().passIt;
                    if (!check)
                    {
                        temp.transform.localPosition = transform.localPosition;
                        temp.Rotate(transform.forward, 45.0f, Space.Self);
                    }
                    break;
                }
            case JunctionType.L2:
                {
                    //this is for the last L joint
                    Transform temp = other.transform.GetComponent<Transform>();
                    // temp.transform.localPosition = movePosition.transform.localPosition;
                    temp.transform.localPosition = transform.localPosition;
                    temp.Rotate(transform.forward, 45f, Space.Self);
                    other.transform.GetComponent<MoveForward>().passIt=true;
                    break;
                }
            case JunctionType.End:
                {
                    //this is the end
                    GameObject temp = other.gameObject;
                    bool isLed= other.transform.GetComponent<MoveForward>().isPassedThroughLed;
                    if(other.transform.tag== "electron")
                    {
                        if ((isLed && button1.button1Pressed) || (isLed && button2.button1Pressed) || (circuitManager.projectNumber == 4 && !button1.button1Pressed))
                        {
                            circuitManager.glowLed = true;
                            circuitManager.flag2 = true;

                        }
                    }
                    if(other.transform.tag=="b1")
                    {
                        Destroy(temp);

                    }
                    other.transform.GetComponent<MoveForward>().isPassedThroughLed = false;
                    other.transform.GetComponent<MoveForward>().passIt = false;
                   
                    break;
                }
            case JunctionType.Plus:
                {
                    //junction  plus. splits electrons into 3
                    plusCount++;
                    if (plusCount > 2)
                        plusCount = 0;
                    Transform temp = other.transform.GetComponent<Transform>();
                    if (plusCount==0)
                    {
                        //temp.transform.position = movePosition.transform.position;
                        temp.transform.localPosition = transform.localPosition;
                        temp.Rotate(transform.forward, 45f, Space.Self);
                    }
                    if(plusCount==1)
                    {

                        //temp.transform.position = movePosition.transform.position;
                        temp.transform.localPosition = transform.localPosition;
                        temp.Rotate(transform.forward, -45f, Space.Self);
                    }
                    if(plusCount==2)
                    {
                       // Debug.Log("2");
                    }
                   



                    break;
                }
            case JunctionType.T:
                {
                    //junction T. Spliting electrons into 2
                    Transform temp = other.transform.GetComponent<Transform>();
                    if (tCount == 0)
                    {
                        temp.transform.localPosition = transform.localPosition;
                        temp.Rotate(transform.forward, -45f, Space.Self);
                        //temp.transform.position = movePosition.transform.position;
                    }
                    if (tCount == 1)
                    {
                        //temp.Rotate(transform.forward, 45f, Space.Self);
                        //temp.transform.position = movePosition.transform.position;

                    }
                   
                    tCount++;
                    if (tCount > 1)
                        tCount = 0;
                    if (other.tag == "electron")
                    {
                        if ((circuitManager.projectNumber == 4 && once2) || (circuitManager.projectNumber == 3 && once2))
                        {
                            circuitManager.move4 = true;
                            once2 = false;
                        }
                    }
                    
                    break;
                }
            case JunctionType.T2:
                {
                    //junction T. Spliting electrons into 2 [type 2]
                    Transform temp = other.transform.GetComponent<Transform>();
                    if (t2Count == 0)
                    {
                        temp.transform.localPosition = transform.localPosition;
                        temp.Rotate(transform.forward, 45f, Space.Self);
                        //temp.transform.position = movePosition.transform.position;
                    }
                    if (t2Count == 1)
                    {
                        //Debug.Log("nothing");
                    }
                    t2Count++;
                    if (t2Count > 1)
                        t2Count = 0;
                    if(other.tag == "electron")
                    {
                        if(circuitManager.projectNumber == 4 && once)
                        {
                            circuitManager.move3=true;
                            once = false;
                        }
                        if(circuitManager.projectNumber==3 && once)
                        {
                            circuitManager.move3 = true;
                            once = false;
                        }
                    }
                    break;
                }
            case JunctionType.Led:
                {
                    //this is for led
                    other.transform.GetComponent<MoveForward>().isPassedThroughLed = true;
                    break;
                }
            case JunctionType.Switch:
                {
                    //for switch
                    /* if (button1.button1Pressed)
                         circuitManager.move = true;
                     else
                         circuitManager.move = false;*/

                    //added new
                    if (other.transform.tag == "b1")
                    {
                        circuitManager.move = false;

                    }
                    else if(other.transform.tag=="b2")
                    {
                        circuitManager.move2 = false;

                    } 
                    else if(other.transform.tag=="b3")
                    {
                        circuitManager.move3 = false;
                    }
                    else if(other.transform.tag=="b4")
                    {
                        circuitManager.move4 = false;
                    }
                    

                    if(other.transform.tag != "b4" && other.transform.tag != "b3")
                    {
                        if (button1 != null)
                        {
                            if (button1.button1Pressed)
                            {
                                //other.transform.tag = "electron";
                                circuitManager.move = true;
                            }
                        }
                        if (button2 != null)
                        {
                            if (button2.button1Pressed)
                            {
                                //other.transform.tag = "electron";
                                circuitManager.move2 = true;
                            }
                        }
                    }
                   
                     
                    


                    break;
                }
            case JunctionType.switchExit:
                {
                    //this is placed after  a switch
                    other.transform.tag = "electron";
                    break;
                }
            case JunctionType.L3:
                {
                    //rotate the electron -90 degree
                    Transform temp = other.transform.GetComponent<Transform>();
                    temp.transform.localPosition = transform.localPosition;
                    temp.Rotate(transform.forward, -45.0f, Space.Self);
            
                    break;
                }
            case JunctionType.ledExit:
                {
                    //this is placed after  a led
                    if (other.transform.tag=="b2")
                    {
                        Destroy(other.gameObject);
                    }
                    if(other.transform.tag== "electron" && button1.button1Pressed)
                    {
                        Destroy(other.gameObject);

                    }
                    break;
                }
            case JunctionType.firstL:
                {

                    //rotate the electron 90 degree
                    Transform temp = other.transform.GetComponent<Transform>();
                    if (other.transform.tag == "electron" && !button1.button1Pressed)
                    {
                        break;
                    }
                    if ( other.transform.tag!="b2")
                    {
                        temp.transform.localPosition = transform.localPosition;
                        temp.Rotate(transform.forward, 45.0f, Space.Self);
                    }
                    
                    break;
                }


        }

    }

    //this is for some bugs 
    private void OnTriggerExit(Collider other)
    {
        switch(junctionType)
        {
            case JunctionType.switchExit:
                {
                    if (other.transform.tag == "b1" || other.transform.tag == "b2" || other.transform.tag == "b3")
                    {
                        Destroy(other.gameObject);
                    }
                    break;
                }
        }
        
    }
    
}
