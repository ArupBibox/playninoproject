﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMobileUserNotification : MonoBehaviour
{
    public Vector3 accInputVector;
    public float minimumValue=-0.5f;
    public GameObject message;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        accInputVector = Input.acceleration;
        Debug.Log(Input.acceleration);
        if(Input.acceleration.z<minimumValue)
        {
            message.SetActive(true);
        }
        else if(Input.acceleration.z > minimumValue)
        {
            message.SetActive(false);
        }
    }
}
