﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{
   public float moveSpeed;
    public bool isPassedThroughLed;
    public bool move, passIt;
    CircuitManager circuitManagerScript;
    public Transform TargetImage;
    // Start is called before the first frame update
    void Start()
    {
        circuitManagerScript = FindObjectOfType<CircuitManager>() as CircuitManager;
        moveSpeed = 60f;
        passIt = false;
        isPassedThroughLed = false;
        TargetImage = GameObject.FindGameObjectWithTag("ImageTarget").GetComponent<Transform>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //move = circuitManagerScript.move;

        //added new
        //based on gameobject tag it will take move variable from circuit Manager
        if (transform.tag == "b1")
            move = circuitManagerScript.move;
        else if (transform.tag == "b2")
            move = circuitManagerScript.move2;
        else if (transform.tag == "b3")
            move = circuitManagerScript.move3;
        else if (transform.tag == "b4")
            move = circuitManagerScript.move4;

        if (move)
        {
            transform.Translate(transform.forward * moveSpeed * Time.deltaTime, Space.Self);//function for moving forward
        }
    }
}
