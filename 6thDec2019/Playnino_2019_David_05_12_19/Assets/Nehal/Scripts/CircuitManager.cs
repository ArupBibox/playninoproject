﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;


public class CircuitManager : MonoBehaviour
{
    public GameObject led1, led2, led3, led4, led5, led6, led7, led8;//list of al leds in all projects
    public bool glowLed;
    public ButtonClick button1;

    public GameObject prevButton, nextButton;
    public GameObject Que1, Que2, Que3, Que4, AskPanel, lastMessage;

    public bool acivateScancircle;
    public GameObject scanningCircle;

    public int projectNumber;
    public InstantiateElectron startingPoint;
    public GameObject[] circuits;
    public GameObject[] lights;
    public GameObject[] projectHeading;

    public bool move,once, answered;

    public bool move2,move3,move4;//added new
    public GameObject kit1Canvas;
    PopUpHandler popupScript;
    KitsHandler kitsHandlerScriptAccess;
    DefaultTrackableEventHandler dataHandler;
    FillBlueLine fillBlueImageScript;

    public bool lostTarget;

    public bool flag,flag2;

    // Start is called before the first frame update
    void Start()
    {
        popupScript = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        kitsHandlerScriptAccess = FindObjectOfType<KitsHandler>() as KitsHandler;
        dataHandler = GameObject.FindObjectOfType<DefaultTrackableEventHandler>();
        lastMessage.SetActive(false);
        flag = true;
        move = true;
        move2 = true;
        move3 = false;
        move4 = false;
        acivateScancircle = true;
        //this is for checking the final question is answered or not 
        if (PlayerPrefs.GetInt("answered")==0)
        {
            answered = false;

        }
        else
        {
            answered = true;
        }
        ResetLed();
        once = true;
        prevButton.SetActive(false);
        nextButton.SetActive(false);
        lostTarget = false;
        projectNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);

        SetProject(projectNumber);
        if (!kit1Canvas.activeSelf)
            kit1Canvas.SetActive(true);
        fillBlueImageScript = FindObjectOfType<FillBlueLine>() as FillBlueLine;

        dataHandler = GameObject.FindObjectOfType<DefaultTrackableEventHandler>();

        
    }

    // Update is called once per frame
    void Update()
    {
        //moving the blueLine to project 1 in first time

        if (DefaultTrackableEventHandler.IsTracked)
        {
            int t = PlayerPrefs.GetInt("currentUnlockedPoint");
            if (t < 2)
            {
                PlayerPrefs.SetInt("currentUnlockedPoint", 2);
                PlayerPrefs.Save();
            }
        }
        //activating previous button
        if (projectNumber!=1 && !prevButton.activeSelf)
        {
            prevButton.SetActive(true);
        }
        else if(projectNumber==1 && prevButton.activeSelf)
        {
            prevButton.SetActive(false);
        }
        //on and off led 
        if (glowLed &&flag2)
        {
            SetLed();
            if(flag)
            {
                ShowAndNextButtons();
                flag = false;
                flag2 = false;
            }
        }
        else if(!glowLed &&flag2)
        {
            ResetLed();
            flag = true;
            flag2 = false;
        }
       /*if(!DefaultTrackableEventHandler.IsTracked && once)
        {
           // KillAllParticles();
            move = true;
            move2 = true;//added new
            move3 = false;
            //ResetLed();
            //SetActiveCircuit();
            once = false;
            glowLed = false;
        }
        if(DefaultTrackableEventHandler.IsTracked)
        {
            once = true;
        }*/

        //this is only for debugging
        if(Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            Debug.LogError("!");
        }
       
        //this is for automatic flow of electrons in project 4
        if (projectNumber == 4)
        {
            if (!button1.button1Pressed && DefaultTrackableEventHandler.IsTracked)
            {
                startingPoint.StartFlow = true;
            }
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            BackButtonClick();
        }

    }

    //this is for activating and settingup of current project
    public void SetProject(int num)
    {

        KillAllParticles();
        projectNumber = num;
        move = true;
        move2 = true;//added new
        move3 = false;
        move4 = false;

        ResetLed();
        ResetCounter();
        SetActiveCircuit();
        SetProjectHeading();

    }
    //enable current circuit gameobject
    void SetActiveCircuit()
    {
        for(int i=0;i<4;i++)
        {
            if (i != projectNumber-1)
                circuits[i].SetActive(false);
            else
                circuits[i].SetActive(true);
        }
        DestroyAllElectrons();
    }
    #region LedAndElectronFunctions
    public void ResetLed()
    {
        led1.GetComponent<MeshRenderer>().material.color = Color.white;
        led1.transform.GetChild(0).gameObject.SetActive(false);

        led2.GetComponent<MeshRenderer>().material.color = Color.white;
        led2.transform.GetChild(0).gameObject.SetActive(false);

        led3.GetComponent<MeshRenderer>().material.color = Color.white;
        led3.transform.GetChild(0).gameObject.SetActive(false);

        led4.GetComponent<MeshRenderer>().material.color = Color.white;
        led4.transform.GetChild(0).gameObject.SetActive(false);

        led5.GetComponent<MeshRenderer>().material.color = Color.white;
        led5.transform.GetChild(0).gameObject.SetActive(false);

        led7.GetComponent<MeshRenderer>().material.color = Color.white;
        led7.transform.GetChild(0).gameObject.SetActive(false);

        led8.GetComponent<MeshRenderer>().material.color = Color.white;
        led8.transform.GetChild(0).gameObject.SetActive(false);

        led6.GetComponent<MeshRenderer>().material.color = Color.white;
        led6.transform.GetChild(0).gameObject.SetActive(false);

    }
    public void SetLed()
    {
        led1.GetComponent<MeshRenderer>().material.color = Color.red;
        led1.transform.GetChild(0).gameObject.SetActive(true);

        led2.GetComponent<MeshRenderer>().material.color = Color.red;
        led2.transform.GetChild(0).gameObject.SetActive(true);

        led3.GetComponent<MeshRenderer>().material.color = Color.red;
        led3.transform.GetChild(0).gameObject.SetActive(true);

        led4.GetComponent<MeshRenderer>().material.color = Color.red;
        led4.transform.GetChild(0).gameObject.SetActive(true);

        led5.GetComponent<MeshRenderer>().material.color = Color.red;
        led5.transform.GetChild(0).gameObject.SetActive(true);

        led7.GetComponent<MeshRenderer>().material.color = Color.red;
        led7.transform.GetChild(0).gameObject.SetActive(true);

        led8.GetComponent<MeshRenderer>().material.color = Color.red;
        led8.transform.GetChild(0).gameObject.SetActive(true);

        if (!button1.button1Pressed)
        {
            led6.GetComponent<MeshRenderer>().material.color = Color.red;
            led6.transform.GetChild(0).gameObject.SetActive(true);

        }




    }
   

    public void DestroyAllElectrons()
    {
        GameObject[] temp = GameObject.FindGameObjectsWithTag("electron");
        foreach (var item in temp)
        {
            Destroy(item);
        }
        GameObject[] temp2 = GameObject.FindGameObjectsWithTag("b3");
        foreach (var item in temp)
        {
            Destroy(item);
        }
    }



    void KillAllParticles()
    {
        GameObject[] temp = GameObject.FindGameObjectsWithTag("electron");
        foreach (var item in temp)
        {
            Destroy(item);
        }
        temp = GameObject.FindGameObjectsWithTag("b1");
        foreach (var item in temp)
        {
            Destroy(item);
        }
        temp = GameObject.FindGameObjectsWithTag("b2");
        foreach (var item in temp)
        {
            Destroy(item);
        }
        temp = GameObject.FindGameObjectsWithTag("b3");
        foreach (var item in temp)
        {
            Destroy(item);
        }
        temp = GameObject.FindGameObjectsWithTag("b4");
        foreach (var item in temp)
        {
            Destroy(item);
        }
    }
    #endregion

    //for showing project heading
    public void SetProjectHeading()
    {
        for (int i = 1; i <= projectHeading.Length; i++)
        {
            if (i == projectNumber)
            {
                projectHeading[i - 1].SetActive(true);
                Debug.Log("current project number " + i);
            }
            else
            {
                projectHeading[i - 1].SetActive(false);
            }
        }
    }
    //closing question popup
    public void CloseQuestion()
    {
        if (projectNumber==1)
        {
            Que1.SetActive(false);
        }
        if (projectNumber==2)
        {
            Que2.SetActive(false);
        }
        if (projectNumber==3)
        {
            Que3.SetActive(false);
        }
        if (projectNumber==4)
        {
            Que4.SetActive(false);
        }
        acivateScancircle = true;
        TrackerManager.Instance.GetTracker<ObjectTracker>().Start();


    }
    //this is for close button
    public void BackButtonClick()
    {
        popupScript.isArSceneOpened = false;
        kitsHandlerScriptAccess.isSceneLoaded = false;
        kitsHandlerScriptAccess.loadArScene = false;
        kitsHandlerScriptAccess.sceneAlreadyLoaded = false;
        kit1Canvas.SetActive(false);
        popupScript.isSceneBack = true;
        fillBlueImageScript.grow = true;

        

        SceneManager.UnloadSceneAsync("StarterKit1_v1");
    }
    //reload function
    public void Reload()
    {
        Debug.Log("reload!");
        ResetLed();
        Junction.once = true;
        TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
        if (!answered)
        {
            nextButton.SetActive(false);
        }
        nextButton.SetActive(false);

        Que1.SetActive(false);
        Que2.SetActive(false);
        Que3.SetActive(false);
        Que4.SetActive(false);
        AskPanel.SetActive(false);
        Invoke("StartScanningAgain", 1.0f);
        dataHandler.reload = true;

        lastMessage.SetActive(false);
        flag = true;
        move = true;
        move2 = true;
        move3 = false;
        move4 = false;
        lostTarget = false;
        acivateScancircle = true;

    }
    //next project button
    public void NextButtonClick()
    {
        
        scanningCircle.SetActive(false);//added
        acivateScancircle = false;
        if (answered && projectNumber == 4)
        {
            lastMessage.SetActive(true);
        }
        else
        {
            if (projectNumber == 1)
            {
                Que1.SetActive(true);
            }
            else if (projectNumber == 2)
            {
                Que2.SetActive(true);
            }
            else if (projectNumber == 3)
            {
                Que3.SetActive(true);
            }
            else if (projectNumber == 4)
            {
                Que4.SetActive(true);
            }
        }
        
        
        
    }
    //previous project button
    public void PrevButtonClick()
    {

        if (projectNumber==2)
        {
            SetProject(1);
        }
        else if(projectNumber==3)
        {
            SetProject(2);
        }
        else if(projectNumber==4)
        {
            SetProject(3);
        }
        Reload();
    }
    ///private functions goes here
    void StartScanningAgain()
    {
        Debug.Log("start scanning");
        glowLed = false;
        flag2 = true;
        //ResetLed();
        //projectNumber = KitsHandler.circuitNumber;
        
        dataHandler.reload = false;
        SetProjectHeading();
        //acivateScancircle = true;
        TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
        if (projectNumber <= 4)
        {
            SetProject(projectNumber);
        }
        else if (projectNumber > 4)
        {
            SetProject(4);
        }
    }

    //function that enable next button
    void ShowAndNextButtons()
    {
        if(projectNumber==1)
        {
            nextButton.SetActive(true);
        }
        else
        {
            nextButton.SetActive(true);
        }
    }

    public void LevelMapQuesClose()
    {
        acivateScancircle = true;
    }

    void ResetCounter()
    {
        GameObject[] temp = GameObject.FindGameObjectsWithTag("sPoint");
        foreach (var item in temp)
        {
            item.transform.GetComponent<InstantiateElectron>().tempCount = 0;
        }
    }
}
