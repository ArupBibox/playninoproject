﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstantiateElectron : MonoBehaviour
{
    // Start is called before the first frame update
    float time;
    public GameObject electron;
    public float timeDelay;
    public Transform startingPoint;
    public bool StartFlow;
    public Transform ImageTarget;
    CircuitManager circuitMangerScript;
    
    public bool s2,s3,s4;
    public bool move;

    public int count;
    public int tempCount;

    void Start()
    {
        //StartFlow = false;
        StartFlow = true;//added new
        

        circuitMangerScript = FindObjectOfType<CircuitManager>() as CircuitManager;
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        {


        }
        //instantiate electrons on differnt position based on the bool variable 
        if(s4 && tempCount<count)
        {
            if (circuitMangerScript.move4)
            {
                time += Time.deltaTime;
                if (time > timeDelay)
                {
                    time = 0;

                    //Instantiate(electron, startingPoint.position,startingPoint.rotation);
                    Instantiate(electron, startingPoint.position, startingPoint.localRotation, ImageTarget);
                    tempCount++;
                }
            }
        }
        else if (s3 && tempCount < count)
        {
            if (circuitMangerScript.move3)
            {
                time += Time.deltaTime;
                if (time > timeDelay)
                {
                    time = 0;

                    //Instantiate(electron, startingPoint.position,startingPoint.rotation);
                    Instantiate(electron, startingPoint.position, startingPoint.localRotation, ImageTarget);//
                    tempCount++;
                }
            }
        }
        else if (!s2 && tempCount < count)
        {
            if (StartFlow && circuitMangerScript.move && DefaultTrackableEventHandler.IsTracked)
            {
                time += Time.deltaTime;
                if (time > timeDelay)
                {
                    time = 0;

                    //Instantiate(electron, startingPoint.position,startingPoint.rotation);
                    Instantiate(electron, startingPoint.position, startingPoint.localRotation, ImageTarget);//
                    tempCount++;
                }
            }
        }
        else if(s2 && tempCount < count)
        {

            if (StartFlow && circuitMangerScript.move2 && DefaultTrackableEventHandler.IsTracked)
            {
                time += Time.deltaTime;
                if (time > timeDelay)
                {
                    time = 0;

                    //Instantiate(electron, startingPoint.position,startingPoint.rotation);
                    Instantiate(electron, startingPoint.position, startingPoint.localRotation, ImageTarget);//
                    tempCount++;
                }
            }
        }
        
      
    }
    void CheckButtonPress()
    {

    }
    public void OnOff()
    {
      
        StartFlow = !StartFlow;
        
    }
    
}
