﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SlideScoreAnim : MonoBehaviour
{
    public Vector2 touchStart, touchEnd;
    public string inputDir = "";
    public bool Anim;
    public Animator SlideAnimationController;
    public bool flag,isSlidinScoreOpen;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("DisableWaringPanel", 2f);

        timer = 0;
        flag = true;
        isSlidinScoreOpen = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (flag && DefaultTrackableEventHandler.IsTracked)
        {
            SlideAnimationController.Play("progressBarAnim");
            flag = false;
            isSlidinScoreOpen = false;
        }
        if(isSlidinScoreOpen && !flag)
        {
            timer += Time.deltaTime;
            if(timer>3.0f)
            {
                timer = 0;
                isSlidinScoreOpen = false;
                SlideAnimationController.Play("progressBarAnim");
            }
        }
        if(!flag)
        {
            GetSlideInput();
        }
    }

    public void GetSlideInput()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touchStart = Input.touches[0].position;
                Anim = true;
            }

            else if ((Input.touches[0].phase == TouchPhase.Moved) && (Input.touches[0].phase != TouchPhase.Stationary))
            {
                touchEnd = Input.touches[0].position;

                Vector2 touchDirection = touchEnd - touchStart;

                if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                {
                    if (touchEnd.x > touchStart.x)
                    {
                        if ((Mathf.Abs(touchDirection.x) > 5))
                        {
                            if ((touchStart.x < 40) && (touchEnd.x < 71) && (touchEnd.x > touchStart.x))
                            {
                                inputDir = "Right";
                                if (Anim)
                                {
                                    SlideAnimationController.Play("progressBarAnim 0");
                                    isSlidinScoreOpen = true;
                                    // progressBar.GetComponent<RectTransform>().anchoredPosition = Vector2.MoveTowards(progressBar.GetComponent<RectTransform>().anchoredPosition, touchEnd, 1f * Time.deltaTime);
                                    Anim = false;
                                }
                            }
                        }


                    }

                    else if (touchEnd.x < touchStart.x)
                    {
                        inputDir = "Left";

                    }
                }
                else
                {
                    if (touchEnd.y > touchStart.y)
                    {
                        inputDir = "Up";
                    }
                    else if (touchEnd.y < touchStart.y)
                    {
                        inputDir = "Down";
                    }
                }
            }
        }
    }
}
