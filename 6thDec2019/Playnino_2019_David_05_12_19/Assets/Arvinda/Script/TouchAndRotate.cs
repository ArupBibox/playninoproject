﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchAndRotate : MonoBehaviour
{
    public float rotationSpeed;
    public GameObject LampTransform;
   

    private void OnMouseDrag()
    {

        float Xaxies = Input.GetAxis("Mouse X") * rotationSpeed;
        float Yaxies = Input.GetAxis("Mouse Y") * rotationSpeed;

        LampTransform.transform.RotateAround(Vector3.down, Xaxies);
        LampTransform.transform.RotateAround(Vector3.right, Yaxies);
        
        
    }
    
}
