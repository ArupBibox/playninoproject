﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;



public class AnimationController : MonoBehaviour
{
    Ray mRay;
    RaycastHit mHit;
    GameObject Target;
    public bool called;
    Animation currentAnim;

    CircuitsUiHandler circuitsUiHandlerScript;

    // Start is called before the first frame update
    void Start()
    {
        circuitsUiHandlerScript = FindObjectOfType<CircuitsUiHandler>();

        called = false;
    }

    
    // Update is called once per frame
    void Update()
    {
        string trackedTragetName = DefaultTrackableEventHandler.TrackImageName;

        if (trackedTragetName == "Cross_Lap_Joint" || trackedTragetName == "BOX"|| trackedTragetName == "Mortise_and_Tenon_Joint"||
            trackedTragetName == "BUTT_JOINT" || trackedTragetName == "Tongue_and_Groove_Joint" )
        {
          
            if (Input.GetMouseButtonDown(0))
            {
                mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(mRay, out mHit))
                {
                    Target = mHit.collider.gameObject;
                    currentAnim = Target.GetComponent<Animation>();
                    currentAnim.Play();
                    called = true;
                }
                
            }
            
            if (called)
            {
                if (!currentAnim.isPlaying)
                {
                    circuitsUiHandlerScript.ShowNextBttn(true);
                    called = false;
                }
            }

        }
        
    }


}
