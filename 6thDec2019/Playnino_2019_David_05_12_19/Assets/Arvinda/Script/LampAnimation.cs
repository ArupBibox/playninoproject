﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampAnimation : MonoBehaviour
{
    public Animator lampAnim;
    //Canvas lampCanvas;
    public List<GameObject> pcbBoard,lampStand,frontBase,backBase,
        midWing,ledFixture,topWingPlank,backWire;
    public List<GameObject> Model;
    public GameObject[] buttons;
    public string[] triggerName;
    public string[] reverseTriggerName;
    private int clickCount = 0;
    bool front, back;
    private int forwardCount, backwardCount;
    int controller;

    // Start is called before the first frame update
    void Start()
    {
        

    }
    
    public void Next()
    {
        print("OnClick" + clickCount);
       
        if (forwardCount < triggerName.Length)
        {
            lampAnim.SetTrigger(triggerName[forwardCount]);
            forwardCount++;
            if (forwardCount == triggerName.Length)
            {
                print("max");
                buttons[0].SetActive(false);
                buttons[1].SetActive(true);
            }
            backwardCount++;
        }
        else
        {
            print("OnclickMax" + forwardCount);
            forwardCount = 0;
            print("OnclickMax" + forwardCount);
        }
        if (forwardCount == 0)
        {
            for (int i = 0; i < pcbBoard.Count; i++)
            {
                pcbBoard[i].SetActive(true);
            }
        }
        else if (forwardCount == 1)
        {
            for (int i = 0; i < lampStand.Count; i++)
            {
                lampStand[i].SetActive(true);
            }
        }
        else if (forwardCount == 2)
        {
            for (int i = 0; i < frontBase.Count; i++)
            {
                frontBase[i].SetActive(true);
            }
        }
        else if (forwardCount == 3)
        {
            for (int i = 0; i < backBase.Count; i++)
            {
                backBase[i].SetActive(true);
            }
        }
        else if (forwardCount == 4)
        {
            for (int i = 0; i < midWing.Count; i++)
            {
                midWing[i].SetActive(true);
            }
        }
        else if (forwardCount == 5)
        {
            for (int i = 0; i < ledFixture.Count; i++)
            {
                ledFixture[i].SetActive(true);
            }
        }
        else if (forwardCount == 6)
        {
            for (int i = 0; i < topWingPlank.Count; i++)
            {
                topWingPlank[i].SetActive(true);
            }
            for (int i = 0; i < backWire.Count; i++)
            {
                backWire[i].SetActive(true);
            }
        }

    }
    public void Previous()
    {
        if(forwardCount==7)
        {
            backwardCount = 6;
        }
        if (forwardCount <= reverseTriggerName.Length && backwardCount > 0)
        {
             print(backwardCount);
            backwardCount--;
           // buttons[0].SetActive(true);
            forwardCount--;
            lampAnim.SetTrigger(reverseTriggerName[backwardCount]);
            if (backwardCount == 1)
            {
                buttons[0].SetActive(true);
                buttons[1].SetActive(false);
            }

        }
        else
        {
            backwardCount = 0;
        }

    }
    
    void DisAbleObj()
    {
        for (int i = 0; i < Model.Count; i++)
        {
            Model[i].SetActive(false);
        }
        buttons[0].SetActive(true);
    }
    
    private void Update()
    {
        
        if (DefaultTrackableEventHandler.TrackImageName == "Table_Lamp")
        {
//            lampCanvas.enabled = true;
            if (Input.GetMouseButtonDown(0))
            {
                lampAnim.enabled = true;
                if(controller == 0)
                {
                    Invoke("DisAbleObj", 3f);
                }
                
                if (controller > 0)
                {
                    CancelInvoke("DisAbleObj");
                }
                controller++;

            }
        }
        else
        {
//            lampCanvas.enabled = false;
        }
                
        
    }

}
