﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sliderControl : MonoBehaviour {
    public float Score = 0;
    public Slider slider;
    public Text ScoreText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        slider.value = Score;
        ScoreText.text =Score.ToString();
    }
  
}
