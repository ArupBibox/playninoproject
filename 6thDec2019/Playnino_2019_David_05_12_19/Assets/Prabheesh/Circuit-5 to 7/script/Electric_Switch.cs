﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Electric_Switch : MonoBehaviour
{

   
    public WayPoints[] WayPoints_ins;
   [SerializeField]
    private Custom_CircuitManager Custom_CircuitManager_ref;
    [SerializeField]
    private LightHandler[] ConnectedLights;
    [HideInInspector]
    public int fingerid = 999;
    [SerializeField]
    private TouchHandler InputtoucHandler_ref;
    [HideInInspector]
    public List<MoveController> AllParticlesforthisSwitch;

    public List<LightHandler> TurnOffLightsHandlers;

    public bool isLed = false;

    public enum LeastResistance
    {
        off,
        on

    }
    public LeastResistance MyType;



    public bool ison = true;


    public void cleanList()
    {
        AllParticlesforthisSwitch = AllParticlesforthisSwitch.Where(x => x != null).ToList();
    }
    public void turnOFFParticle()
    {
        for (int i = 0; i < AllParticlesforthisSwitch.Count; i++)
        {
            if (AllParticlesforthisSwitch[i] != null)
            {
                //            Debug.LogError(i + "all particles");
                AllParticlesforthisSwitch[i].gameObject.SetActive(false);
                AllParticlesforthisSwitch[i].isonscreen = false;
            }
        }
    }


    public MoveController getFreeParticle()
    {
        for(int i = 0; i < AllParticlesforthisSwitch.Count; i++)
        {
            if (!AllParticlesforthisSwitch[i].gameObject.activeSelf)
            {
                MoveController mc = AllParticlesforthisSwitch[i];

                mc.current = 0;
                return mc;
            }
        }
        return null;
    }

    public void forceOffLights()
    {
        for(int i = 0; i < TurnOffLightsHandlers.Count; i++)
        {
            TurnOffLightsHandlers[i].setCompleteOff();
        }
    }
    void OnMouseDown()
    {
        //Debug.LogError("----");
        if (InputtoucHandler_ref != null)
        {
          //  Debug.LogError("----");
            return;
        }
       
        if (MyType == LeastResistance.on)
        {
          //  Debug.LogError("on here");
            //ison = true;
            Custom_CircuitManager_ref.disableParticles();
            turnCompleteOffLights();
            Custom_CircuitManager_ref.flipAllSwitch();
            forceOffLights();

            ison = true;
         }
        else
        {
            Debug.LogError("working");
            setSwitch(true);
           // Custom_CircuitManager_ref.startFlow();
        }

    }
    public void touchUp()
    {
        ison = false;
        Custom_CircuitManager_ref.istarted = false;
        //  Custom_CircuitManager_ref.turnOffAllParticles();
        turnOFFParticle();
        turnOffLights();
    }




    public void OnMouseUp()
    {
        if (InputtoucHandler_ref != null)
        {
            return;
        }

        if (MyType == LeastResistance.on)
        {
           
            Custom_CircuitManager_ref.backflipAllSwitch();
           

        }


        Custom_CircuitManager_ref.disableParticles();
        Custom_CircuitManager_ref.istarted = false;
        turnOFFParticle  ();
        turnOffLights();
        Custom_CircuitManager_ref.isleastref = false;
        Custom_CircuitManager_ref.istarted = false;
        ison = false;

    }


    public void turnOnLights()
    {
        for (int i = 0; i < ConnectedLights.Length; i++)
        {
           // ConnectedLights[i].gameObject.SetActive(false);
            ConnectedLights[i].setLightIntensity(1);
        }
    }
    public void turnOffLights()
    {
        for (int i = 0; i < ConnectedLights.Length; i++)
        {
            ConnectedLights[i].setCompleteOff();
        }
    }
    public void turnCompleteOffLights()
    {
        Debug.LogError(ConnectedLights.Length+"---------------0000" + gameObject.name);
        for (int i = 0; i < ConnectedLights.Length; i++)
        {
            ConnectedLights[i].setCompleteOff();
        }
    }



    public void setSwitch(bool val)
    {


        ison = val;

        return;





    }
    public bool getSwitch()
    {
        return ison;
    }
    public int getWayPointLeastTyped()
    {
        if (MyType == LeastResistance.off)
        {
            return 0;
        }
        else
        {

            return WayPoints_ins.Length;
        }
    }
    public int getWayPointLen()
    {
        if (!ison)
        {
            return 0;
        }
        else
            return WayPoints_ins.Length;
    }
    public WayPoints getWayPooint(int i)
    {
        return WayPoints_ins[i];
    }

   

}

