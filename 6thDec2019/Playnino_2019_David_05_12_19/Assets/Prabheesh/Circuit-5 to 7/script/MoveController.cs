﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveController : MonoBehaviour
{

    [SerializeField]
    private Transform[] WayPoint;
    public int current = 0;
    public bool isonscreen = false;
    public Rigidbody Rigidbody_ref;
    public float speed = 50.0f;
    public void setWayPoint(Transform[] waypoint_ref)
    {
        if (WayPoint.Length > 0)
        {
            Array.Clear(WayPoint, 0, WayPoint.Length);
        }
        //loop to popout everything
       // Debug.LogError(WayPoint.Length);
        WayPoint = waypoint_ref;
        transform.position = waypoint_ref[0].transform.position;
        isonscreen = true;
        // InvokeRepeating("MoveThroughPoints", 1, .01f);
    }

    public void setIsonScreen(bool val)
    {
        isonscreen = val;
    }

    void MoveThroughPoints()
    {
        if (Rigidbody_ref == null)
            Rigidbody_ref = GetComponent<Rigidbody>();
        //        Debug.LogError(current + "-----" + WayPoint.Length);
        if (current >= WayPoint.Length)
        {
            CancelInvoke("MoveThroughPoints");
            isonscreen = false;
            gameObject.SetActive(false);
            DestroyImmediate(gameObject);
            return;
        }
        if (transform.position != WayPoint[current].position)
        {
             Vector3 pos = Vector3.MoveTowards(transform.position, WayPoint[current].position, 15 * Time.fixedDeltaTime);
             Rigidbody_ref.MovePosition(pos);

           // float step = speed * Time.deltaTime; // calculate distance to move
          //  transform.position = Vector3.MoveTowards(transform.position, WayPoint[current].position, step);



        }
        else
        {
            current = current + 1 % WayPoint.Length;
        }

    }

    public bool getStat()
    {
        return isonscreen;
    }

    public void getStat(bool val)
    {
        isonscreen = val;
    }


    public void UpdateMe()
    {
        if (isonscreen)
        {

            MoveThroughPoints();
        }

    }
}
