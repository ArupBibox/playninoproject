﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchHandler : MonoBehaviour
{
    [HideInInspector]
    public int fingerid1 = 0;
    [HideInInspector]
    private int fingerid2 = 0;
    string str = "";
    public Custom_CircuitManager Custom_CircuitManager_ref;
    public List<Electric_Switch> Electric_Switches_raycasted;

   
    void shootRay(int finger_id,int index_ref)
    {

        var tapCount = Input.touchCount;
        Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(index_ref).position);
        RaycastHit raycastHit;
        if (Physics.Raycast(raycast, out raycastHit))
        {



            Electric_Switch es = raycastHit.collider.GetComponent<Electric_Switch>();
            if (es != null)
            {
                if (es.ison)
                    return;
                es.ison = true;
                if (!Custom_CircuitManager_ref.istarted)
                {
                    es.setSwitch(true);
                    es.ison = true;
                    es.fingerid = finger_id;
                    // Custom_CircuitManager.
                }
                else
                {
                    Custom_CircuitManager_ref.updateAllPrefabs(es);
                    es.ison = true;
                    es.fingerid = finger_id;
                }

                // Electric_Switches_raycasted.Add(es);
              //  Debug.LogError("collider found-------" + es.gameObject.name);
            }


        }

    }

    private void Update()
    {
        if (Custom_CircuitManager_ref == null)
        {

            return;
        }
        //  Debug.LogError("mainbool  >>>>" + mainboolref.id);
        // Debug.LogError("2:->" + mainboolref2.id);

        for (int i = 0; i < Input.touchCount; ++i)
        {
            switch (Input.GetTouch(i).phase)
            {
                // Record initial touch position.
                case TouchPhase.Began:

                    shootRay(Input.GetTouch(i).fingerId,i);
                    break;

                // Determine direction by comparing the current touch position with the initial one.
                case TouchPhase.Moved:

                    break;

                // Report that a direction has been chosen when the finger is lifted.
                case TouchPhase.Ended:
                    //Debug.LogError(Electric_Switches_raycasted.Count + "touch up");

                    {


                        for (int j = 0; j < Custom_CircuitManager_ref.AllElectric_Switches.Length; j++)
                        {
                            if (Custom_CircuitManager_ref.AllElectric_Switches[j].fingerid == Input.GetTouch(i).fingerId)
                            {
                               // Debug.LogError("touch up" + Custom_CircuitManager_ref.AllElectric_Switches[j].fingerid);
                                Custom_CircuitManager_ref.AllElectric_Switches[j].ison = false;
                                Custom_CircuitManager_ref.AllElectric_Switches[j].touchUp();
                                Custom_CircuitManager_ref.AllElectric_Switches[j].fingerid = 999;
                                Custom_CircuitManager_ref.AllElectric_Switches[j].ison = false;
                                Custom_CircuitManager_ref.AllElectric_Switches[j].turnOFFParticle();

                                //Custom_CircuitManager_ref.AllElectric_Switches.Remove(Electric_Switches_raycasted[i]);
                            }
                        }
                    }

                    break;
            }
        }
    }


}
