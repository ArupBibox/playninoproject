﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightHandler : MonoBehaviour
{
    private float intensityvalue = 0;
    [SerializeField]
    private Light LightComponent;

    [SerializeField]
    private Electric_Switch[] ConnectedSwitches;
    private CircuitsUiHandler circuitsUiHandlerAccess;

    bool isnextbuttonshown = false;
    private void Start()
    {
        circuitsUiHandlerAccess = FindObjectOfType<CircuitsUiHandler>();
    }


    private void OnTriggerEnter(Collider other)
    {

        // other.GetComponent<OnTrigger>().LightHandler_ref = this;
        other.GetComponent<OnTrigger>().LedList.Add(this);
    }
    public void updateLight(float val)
    {
        intensityvalue += val;
        if (intensityvalue < 0)
            intensityvalue = 0;
        else
        {
            LightComponent.intensity = intensityvalue;
        }
    }
    public void setCompleteOff()
    {
       // Debug.LogError("-----"+LightComponent.gameObject.name);
        LightComponent.intensity = 0;
    }
    public void setOffLights()
    {
        if (LightComponent.intensity - 1000 < 0)
            LightComponent.intensity = 0;
        else
            LightComponent.intensity += -1000;
    }

    public void setLightIntensity(float val)
    {
       
        int tempval = 0;
        for (int i = 0; i < ConnectedSwitches.Length; i++)
        {
            if (ConnectedSwitches[i].ison)
            {
                tempval += 1;
            }
           else if (ConnectedSwitches[i].isLed == true)
            {
                tempval += 1;
            }
        }
        // Debug.LogError(gameObject.name+"val" + val);
        if (tempval == 1)
        {
            LightComponent.intensity = 1000;
            if(!isnextbuttonshown)
            {
                circuitsUiHandlerAccess.ShowNextBttn(true);
                isnextbuttonshown = true;
            }
            
        }
        else if (tempval == 2)
        {
            LightComponent.intensity = 2000;
            if (!isnextbuttonshown)
            {
                circuitsUiHandlerAccess.ShowNextBttn(true);
                isnextbuttonshown = true;
            }
        }
           




      //  Debug.LogError(tempval+"gggg" + gameObject.name);




    }



}
