﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsCollider : MonoBehaviour
{
    [SerializeField]
    private Electric_Switch[] ConnectedSwitches;

    public bool isswitch1on = false;
    public bool isswitch2on = false;
    private Renderer MyMeshRenderer;

    void Start()
    {

      //  MyMeshRenderer = GetComponent<Renderer>();
        // Debug.LogError("888888888888888" + MyMeshRenderer);

    }
    private void OnTriggerEnter(Collider other)
    {

        return;

        if (MyMeshRenderer != null)
            MyMeshRenderer.enabled = false;
        else
        {
            MyMeshRenderer = GetComponent<Renderer>();
            MyMeshRenderer.enabled = false;
        }
    }
    void OnTriggerExit(Collider other)
    {
        return;
        MyMeshRenderer.enabled = true;
    }

}
