﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllCamera :MonoBehaviour
{
    public Camera LED_IN_PARALLEL_Camera;
    public Camera SWITCHES_IN_PARALLEL_Camera;
    public Camera PATH_OF_LEAST_RESISTANCE_Camera;

    public static Camera LED_IN_PARALLEL;
    public static Camera SWITCHES_IN_PARALLEL;
    public static Camera PATH_OF_LEAST_RESISTANCE;
    private void Start()
    {
        LED_IN_PARALLEL = LED_IN_PARALLEL_Camera;
        SWITCHES_IN_PARALLEL = SWITCHES_IN_PARALLEL_Camera;
        PATH_OF_LEAST_RESISTANCE = PATH_OF_LEAST_RESISTANCE_Camera;

    }

}
