﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircuitEndHandler : MonoBehaviour
{
    [SerializeField]
    private LightHandler[] AllLights;
    [SerializeField]
    private Electric_Switch[] AllElectricSwitches;
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.LogError("name" + gameObject.name);
       
        for (int i = 0; i < AllElectricSwitches.Length; i++)
        {
          //  Debug.LogError("coming here" + AllElectricSwitches.Length);
            AllElectricSwitches[i].turnOnLights();
        }


    }
    
}
