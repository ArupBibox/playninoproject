﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTrigger : MonoBehaviour
{
    private Renderer MyMeshRenderer;
    public Electric_Switch Electric_Switch_ref;
    public LightHandler LightHandler_ref;
    public List<LightHandler> LedList;
    void Start()
    {

        MyMeshRenderer = GetComponent<Renderer>();
       // Debug.LogError("888888888888888" + MyMeshRenderer);

    }
    private void OnTriggerEnter(Collider other)
    {




        if(MyMeshRenderer!=null)
        MyMeshRenderer.enabled = false;
        else
        {
            MyMeshRenderer = GetComponent<Renderer>();
            MyMeshRenderer.enabled = false;
        }
    }
    void OnTriggerExit(Collider other)
    {
        MyMeshRenderer.enabled = true;
    }
}
