﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Custom_CircuitManager : MonoBehaviour
{
    [HideInInspector]
    public int index = 99;
    [SerializeField]
    private TouchHandler TouchHandler_Ins;
    [SerializeField]
    public Electric_Switch[] AllElectric_Switches;
   // public Transform TR;
    [SerializeField]
    public GameObject ParticlePrefab;
    [SerializeField]
    private Transform ParticleParent;
    [HideInInspector]
    public List<WayPoints> AllPrefabs;


    private List<MoveController> AllParticles;
    [HideInInspector]
    public List<Electric_Switch> ElectricSwitch_AS_AllPrefabs;
    [HideInInspector]
    public bool isleastref = false;


    public bool istarted = false;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void disableParticles()
    {
        for (int i = 0; i < ElectricSwitch_AS_AllPrefabs.Count; i++)
        {
            if(ElectricSwitch_AS_AllPrefabs[i]!=null)
            ElectricSwitch_AS_AllPrefabs[i].turnOFFParticle();
        }

    }

    public void flipAllSwitch()
    {

        for (int i = 0; i < AllElectric_Switches.Length; i++)
        {
            if (AllElectric_Switches[i].MyType == Electric_Switch.LeastResistance.on)
                AllElectric_Switches[i].ison = true;
            else
                AllElectric_Switches[i].ison = false;
        }

        ElectricSwitch_AS_AllPrefabs.Clear();
        istarted = false;
        startFlow();


    }

    public void backflipAllSwitch()
    {

        for (int i = 0; i < AllElectric_Switches.Length; i++)
        {
            if (AllElectric_Switches[i].MyType == Electric_Switch.LeastResistance.on)
                AllElectric_Switches[i].ison = false;
            else
                AllElectric_Switches[i].ison = true;
        }

        ElectricSwitch_AS_AllPrefabs.Clear();
        startFlow();


    }
    public void flipAllSwitch(bool val)
    {

        for (int i = 0; i < AllElectric_Switches.Length; i++)
        {
            if (AllElectric_Switches[i].MyType == Electric_Switch.LeastResistance.off)
                AllElectric_Switches[i].ison = val;
            else
                AllElectric_Switches[i].ison = val;
        }

        ElectricSwitch_AS_AllPrefabs.Clear();
        startFlow();


    }
    public void turnOffAllParticlesleasTref()
    {
        for (int i = 0; i < AllParticles.Count; i++)
        {
            if (AllParticles[i]!=null)
            AllParticles[i].gameObject.SetActive(false);
        }

        CancelInvoke("createParticles");
        leastResistance();
    }
    public void turnOffAllParticles()
    {
        for (int i = 0; i < AllParticles.Count; i++)
        {
            if(AllParticles[i]!=null)
            AllParticles[i].gameObject.SetActive(false);
        }
        AllParticles.Clear();
        AllPrefabs.Clear();
        istarted = false;
        //  CancelInvoke();
        CancelInvoke("createParticles");
    }
    public void leastResistance()
    {
        isleastref = true;
        AllPrefabs.Clear();
        for (int i = 0; i < AllElectric_Switches.Length; i++)
        {
            for (int j = 0; j < AllElectric_Switches[i].getWayPointLeastTyped(); j++)
            {
                if (AllElectric_Switches[i].getWayPointLeastTyped() > 0)
                {
                    AllPrefabs.Add(AllElectric_Switches[i].getWayPooint(j));

                }
            }
        }
     //   Debug.LogError("called from least prefer" + istarted);
        InvokeRepeating("createParticles", .5f, .2f);
        istarted = true;
    }

    public void updateAllPrefabs()
    {
        for (int j = 0; j < AllElectric_Switches[0].getWayPointLen(); j++)
        {
            if (AllElectric_Switches[j].getWayPointLen() > 0)
                AllPrefabs.Add(AllElectric_Switches[0].getWayPooint(j));
        }
    }

    public void updateAllPrefabs(Electric_Switch electric_Switch_param)
    {
        for (int j = 0; j < electric_Switch_param.getWayPointLen(); j++)
        {
            if (AllElectric_Switches[j].getWayPointLen() > 0)
                AllPrefabs.Add(electric_Switch_param.getWayPooint(j));
        }
    }
    public void startFlow()
    {

        if (istarted == true)
            return;
        CancelInvoke();
       // Debug.LogError("all electric switch" + AllElectric_Switches.Length);
        for (int i = 0; i < AllElectric_Switches.Length; i++)
        {
            for (int j = 0; j < AllElectric_Switches[i].getWayPointLen(); j++)
            {
               // Debug.LogError("all electric switch" + AllElectric_Switches[i].getWayPointLen());
                if (AllElectric_Switches[i].getWayPointLen() > 0)
                    AllPrefabs.Add(AllElectric_Switches[i].getWayPooint(j));
                ElectricSwitch_AS_AllPrefabs.Add(AllElectric_Switches[i]);
            }
        }


        InvokeRepeating("createParticles", 1f, .2f);
        // istarted = true;
    }


    public void updateAllPrefabsList()
    {

    }
    public void createParticles()
    {

        GameObject go;
        MoveController mc;
        List<MoveController> templist = new List<MoveController>();
        for (int i = 0; i < AllElectric_Switches.Length; i++)
        {
            if (AllElectric_Switches[i].ison)
            {
                for (int j = 0; j < AllElectric_Switches[i].WayPoints_ins.Length; j++)
                {
                    mc = getParticleFromPool(AllElectric_Switches[i]);
                    mc.transform.SetParent(ParticleParent);
                    mc.setWayPoint(AllElectric_Switches[i].WayPoints_ins[j].WayPointsTransform);
                    AllElectric_Switches[i].AllParticlesforthisSwitch.Add(mc);
                   // templist.Add(mc);
                }
            }


        }

     //   for (int i = 0; i < templist.Count; i++)
          //  templist[i].setIsonScreen(true);

    }



    public MoveController getParticleFromPool(Electric_Switch es)
    {

        {

            GameObject go = Instantiate(ParticlePrefab);
            MoveController mc = go.GetComponent<MoveController>();
            return mc;

        }

    }
    public void createParticles1()
    {

        GameObject go;
        MoveController mc;

        for (int i = 0; i < AllPrefabs.Count; i++)
        {

            go = Instantiate(ParticlePrefab);

            mc = go.GetComponent<MoveController>();
            mc.setWayPoint(AllPrefabs[i].WayPointsTransform);
            AllParticles.Add(mc);
            ElectricSwitch_AS_AllPrefabs[i].AllParticlesforthisSwitch.Add(mc);
        }

    }


    private void FixedUpdate()
    {

       


        if (DefaultTrackableEventHandler.IsTracked)
        {



            if (istarted == false)
            {
                for (int i = 0; i < AllElectric_Switches.Length; i++)
                {
                    if (AllElectric_Switches[i].ison == true)
                    {

                        startFlow();
                        istarted = true;
                        break;
                    }
                }
                // 
                //startFlow();
            }


        }

        for (int i = 0; i < ElectricSwitch_AS_AllPrefabs.Count; i++)
        {
            for (int j = 0; j < ElectricSwitch_AS_AllPrefabs[i].AllParticlesforthisSwitch.Count; j++)
            {
                if (ElectricSwitch_AS_AllPrefabs[i].AllParticlesforthisSwitch[j] != null)
                    ElectricSwitch_AS_AllPrefabs[i].AllParticlesforthisSwitch[j].UpdateMe();
            }
            ElectricSwitch_AS_AllPrefabs[i].cleanList();
        }


      


    }




}

