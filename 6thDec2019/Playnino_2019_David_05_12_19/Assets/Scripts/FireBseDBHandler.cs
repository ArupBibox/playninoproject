﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class FireBseDBHandler : MonoBehaviour
{
    MobileNumberLoginFbase mblLoginScript;
    
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

    public int totalUserCount;


    // Start is called before the first frame update
    void Start()
    {
        mblLoginScript = GameObject.FindObjectOfType<MobileNumberLoginFbase>();

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });


    }

    // Initialize the Firebase database:
    protected virtual void InitializeFirebase()
    {
        FirebaseApp app = FirebaseApp.DefaultInstance;
        // NOTE: You'll need to replace this url with your Firebase App's database
        // path in order for the database connection to work correctly in editor.
        app.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");
        if (app.Options.DatabaseUrl != null)
        {
            app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);

            //To Start Total User Count
            FirebaseDatabase.DefaultInstance.GetReference("Users").ValueChanged += DetectTotalCount;
        }
        //DemoTry();
    }

    void DetectTotalCount(object sender, ValueChangedEventArgs argsCount)
    {
        totalUserCount = Convert.ToInt32(argsCount.Snapshot.ChildrenCount.ToString());
    }

    public void AddUserToDB()
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

       // mDatabaseRef.Child("Users").Child("0").Child

        mDatabaseRef.Child("Users").Child(totalUserCount.ToString()).Child("phoneNumber").SetValueAsync(mblLoginScript.userPhoneNumber.text);
        mDatabaseRef.Child("Users").Child(totalUserCount.ToString()).Child("userName").SetValueAsync(mblLoginScript.userName.text);

        //Scene Change
        SceneManager.LoadScene("MainScene");

    }


}
