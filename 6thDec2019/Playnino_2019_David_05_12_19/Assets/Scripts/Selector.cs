﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour
{
    public Vector2 touchStart;
    public Vector2 touchEnd;
    public string inputDir = "";
    int ModelNo = 1;
    public GameObject[] planeModels;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ModelNo < 1)
        {
            ModelNo = 1;
        }
        if (ModelNo > 3)
        {
            ModelNo = 3;
        }
        getInput();
        if (DefaultTrackableEventHandler.IsTracked)
        {
            if (ModelNo == 1)
            {
                planeModels[0].SetActive(true);
                planeModels[1].SetActive(false);
                planeModels[2].SetActive(false);
            }
            if (ModelNo == 2)
            {
                planeModels[0].SetActive(false);
                planeModels[1].SetActive(true);
                planeModels[2].SetActive(false);
            }

            else if (ModelNo == 3)
            {
                planeModels[0].SetActive(false);
                planeModels[1].SetActive(false);
                planeModels[2].SetActive(true);
            }

        }
    }
    public void getInput()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touchStart = Input.touches[0].position;
            }

            else if (Input.touches[0].phase == TouchPhase.Ended)
            {
                touchEnd = Input.touches[0].position;

                Vector2 touchDirection = touchEnd - touchStart;

                if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                {
                    if (touchEnd.x > touchStart.x)
                    {
                        inputDir = "Right";
                        if (pilotController.canSwipe)
                        {
                            ModelNo--;
                        }
                       
                    }
                    else if (touchEnd.x < touchStart.x)
                    {
                        inputDir = "Left";
                        if (pilotController.canSwipe)
                        {
                            ModelNo++;
                        }

                    }
                }
                else
                {
                    if (touchEnd.y > touchStart.y)
                    {
                        inputDir = "Up";
                        

                    }
                    else if (touchEnd.y < touchStart.y)
                    {
                        inputDir = "Down";
                    }
                }
            }
        }
    }
}
