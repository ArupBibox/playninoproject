﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arPanelUI : MonoBehaviour
{
    public Animator phoneAnim, textAnim, tickAnim, contactAnim, popupdownanim;
    public UIManager uiHandler;

    
    bool check;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (check)
        {
            if (!phoneAnim.GetCurrentAnimatorStateInfo(0).IsName("phoneIconAnim"))
            {
                tickAnim.Play("tickPopUp");

                check = false;
            }
        }
        //if(!phoneAnim.GetCurrentAnimatorStateInfo(0).IsName("phoneIconAnim"))
    }
    public void OnPhoneClick()
    {
        phoneAnim.Play("phoneIconAnim");
      
        textAnim.Play("textAnim");
        check = true;
        if (!phoneAnim.GetCurrentAnimatorStateInfo(0).IsName("textAnim"))
        {
            contactAnim.Play("fadeIn");
        }
    }
    public void disablePanel()
    {
        popupdownanim.Play("popUpPanel 0");
        uiHandler.backCount = 0;
        

        
        
    }
    


}
