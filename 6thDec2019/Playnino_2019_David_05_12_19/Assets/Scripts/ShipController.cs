﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipController : MonoBehaviour
{
    public GameObject  /*scanCircle,*/  waterSplash;
    public Animator  shipAnimator;
    public float time = 0;
    public  bool startTime;
    public static bool shipMove;
    public float shipSpeed = 2f;
    public float countTime = 0;
    public Vector2 touchStart;
    public Vector2 touchEnd;
    public string inputDir = "";
    public GameObject upArrow;
    public AudioSource startSound, MoveSound;
    public float turnTime = 0;
    float rotationResetSpeed = 0.1f;
    public Quaternion originalRotationValue; // declare this as a Quaternion

    public Vector3 orgVal;
    public bool reset = false;
    public GameObject watchVideo;

    CircuitsUiHandler circuitsUiHandlerAccess;

    // Start is called before the first frame update
    void Start()
    {
        circuitsUiHandlerAccess = FindObjectOfType<CircuitsUiHandler>();
       // tryAgain.SetActive(false);
      
        //scanCircle.SetActive(true);
        shipMove = false;
        waterSplash.SetActive(false);
        startSound.enabled = false;
        MoveSound.enabled = false;
        originalRotationValue = transform.rotation;

        orgVal = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z);
    }

    void SetScanCircle(bool val)
    {
        if (val == false) {
        }

        //scanCircle.SetActive(val);
    }

    // Update is called once per frame
    void Update()
    {
        if (reset)
        {
            initialise();
          //  reset = false;
        }

        getInput();

   
        if (startTime)
        {
            
           // videoButton.SetActive(true);
            //tryAgain.SetActive(true);
            MoveSound.volume -= 0.001f;
            //if (MoveSound.volume < 0.09f)
            //{
              //  MoveSound.volume = 0.09f;
            //}

            time++;
            
            
           
            
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (this.gameObject.tag == "ship")
            {
                upArrow.SetActive(false);
              //  rightArrow.SetActive(false);
               // LeftArrow.SetActive(false);
                startSound.enabled = false;
                MoveSound.enabled = true;
                startTime = true;






                if (this.gameObject.tag == "ship")
                {
                    shipMove = true;
                    shipAnimator.applyRootMotion = true;
                }



            }
        }
        if (DefaultTrackableEventHandler.IsTracked)
        {
            if (DefaultTrackableEventHandler.TrackImageName == "ShipTarget")
            {
               
             //   if (!reset)
             //   {

                    SetScanCircle(false);
                //  scanCircle.SetActive(false);
                //   }
              //  if (reset)
              //  {
                //    SetScanCircle(true);
               // }
                if (!shipMove)
                {
                    startSound.enabled = true;
                    // startSound.Play();

                }
            }
           
        }
        else if (!DefaultTrackableEventHandler.IsTracked)
        {
            //startSound.Pause();
        }
        if (shipMove)
        {
            if (this.gameObject.tag == "ship")
            {
                gameObject.transform.Translate(new Vector3(0, 0, shipSpeed * Time.deltaTime));
                waterSplash.SetActive(true);
                turnTime += Time.deltaTime;
                SetScanCircle(false);


            }
        }
        if (time > 500)
        {
           // MoveSound.enabled = false;
        }
        if((turnTime>6) && (turnTime < 9))
        {
            gameObject.transform.Rotate(0, -0.1f, 0);
            shipSpeed = 10;
            watchVideo.SetActive(true);
        }
        else
        {
            shipSpeed = 15;
        }

        
        
       
      

    }
    void initialise()
    {
     
        // tryAgain.SetActive(false);

        //scanCircle.SetActive(true);
        shipMove = false;
        waterSplash.SetActive(false);
        startSound.enabled = false;
        MoveSound.enabled = false;
        countTime = 0;
        time = 0;
        turnTime = 0;
        startTime = false;
        upArrow.SetActive(true);
      //  rightArrow.SetActive(true);
      // LeftArrow.SetActive(true);
        shipMove = false;
        startSound.volume = 1;
        MoveSound.volume = 1;
        gameObject.transform.localPosition = new Vector3(0.07021274f, 0.023977f, -0.03638f);
        //transform.localRotation = Quaternion.Slerp(transform.rotation, originalRotationValue, Time.time * rotationResetSpeed);

        transform.localRotation = Quaternion.Euler(orgVal);
        watchVideo.SetActive(false);


    }
   
       
   
   
    public void getInput()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touchStart = Input.touches[0].position;
            }

            else if (Input.touches[0].phase == TouchPhase.Ended)
            {
                touchEnd = Input.touches[0].position;

                Vector2 touchDirection = touchEnd - touchStart;

                if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                {
                    if (touchEnd.x > touchStart.x)
                    {
                        inputDir = "Right";
                    }
                    else if (touchEnd.x < touchStart.x)
                    {
                        inputDir = "Left";
                    }
                }
                else
                {
                    if (touchEnd.y > touchStart.y)
                    {
                        if (DefaultTrackableEventHandler.IsTracked)
                        {
                            if (DefaultTrackableEventHandler.TrackImageName == "ShipTarget")
                            {
                                inputDir = "Up";
                                if (upArrow.activeSelf)
                                {
                                    upArrow.SetActive(false);
                                }
                              //  rightArrow.SetActive(false);
                              //  LeftArrow.SetActive(false);
                                startSound.enabled = false;
                                MoveSound.enabled = true;
                                // MoveSound.Play();
                                startTime = true;
                              






                                if (this.gameObject.tag == "ship")
                                {
                                    if (!shipMove)
                                    {
                                        shipMove = true;

                                        shipAnimator.applyRootMotion = true;
                                    }
                                }
                            }

                           
                        }
                        else if (!DefaultTrackableEventHandler.IsTracked)
                        {
                          //  MoveSound.Pause();
                        }

                    }
                    else if (touchEnd.y < touchStart.y)
                    {
                        inputDir = "Down";
                    }
                }
            }
        }
    }

}
