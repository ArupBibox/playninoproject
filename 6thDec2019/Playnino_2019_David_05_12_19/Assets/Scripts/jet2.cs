﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jet2 : MonoBehaviour
   
{
    public Animator jet2Animator;
    public Vector2 touchStart;
    public Vector2 touchEnd;
    public string inputDir = "";
    public float jetSpeed = 50f;
    public float countTime = 0;
    bool count;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (DefaultTrackableEventHandler.IsTracked)
        {
            jet2Animator.Play("Take 001");
            
        }
        if (count)
        {
            countTime++;
        }
        getInput();
        if (countTime > 180)
        {
            jetMove();
        }
    }
    public void getInput()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touchStart = Input.touches[0].position;
            }

            else if (Input.touches[0].phase == TouchPhase.Ended)
            {
                touchEnd = Input.touches[0].position;

                Vector2 touchDirection = touchEnd - touchStart;

                if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                {
                    if (touchEnd.x > touchStart.x)
                    {
                        inputDir = "Right";
                    }
                    else if (touchEnd.x < touchStart.x)
                    {
                        inputDir = "Left";
                    }
                }
                else
                {
                    if (touchEnd.y > touchStart.y)
                    {
                        if (pilotController.canSwipe)
                        {
                            inputDir = "Up";
                            jet2Animator.Play("jet2Anim");
                            count = true;
                        }
                       
                    }
                    else if (touchEnd.y < touchStart.y)
                    {
                        inputDir = "Down";
                    }
                }
            }
        }
    }
    public void jetMove()
    {
        jet2Animator.applyRootMotion = true;
        gameObject.transform.Translate(new Vector3(-jetSpeed * Time.deltaTime, 0, 0));
    }
}
