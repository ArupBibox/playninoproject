﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstClick : MonoBehaviour
{
    public int isClick;
    public int ClickValue;
    public GameObject[] TemporaryObjects;
    public GameObject[] panels;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Awake()
    {
        isClick = 0;
        


    }

    // Update is called once per frame
    void Update()
    {
        ClickValue = PlayerPrefs.GetInt("click", isClick);
        if (ClickValue > 0)
        {
            TemporaryObjects[0].SetActive(false);
            TemporaryObjects[1].SetActive(false);
        }
       
    }
    public void setClick()
    {
        isClick = 1;
        PlayerPrefs.SetInt("click", isClick);
    }
    public void firstScreen()
    {
        panels[0].SetActive(true);
        panels[1].SetActive(false);
    }
    public  void secondScreen()
    {
        //panels[0].SetActive(false);
        //panels[1].SetActive(true);
        SceneManager.LoadScene(1);
    }
    

}

