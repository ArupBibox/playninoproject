﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using DG.Tweening;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase;
//using Firebase.Database;
using UnityEngine.SceneManagement;
using System;
using TMPro;
using System.Text.RegularExpressions;

public class MobileNumberLoginFbase : MonoBehaviour
{

    //public InputField userName, userPhoneNumber, emailId, otpCode;
    public TMP_InputField userName, userPhoneNumber, userEmailId, otpCode , loginUserPhoneNumber;
    public TMP_Dropdown countryCodeDD1, countryCodeDD2;
    public string fullPhoneNumber;
    public TMP_Text editablePhoneNumber;
    public Button signUpBttn, loginBttn, verifyOtpBttn;

    bool isEmailValid;

    //int initNinots = 0;

    
    //For Intro Clip
    //public GameObject introClip;
    public Animator anim;
    public SpriteRenderer sprite;
    bool screenSlided, isNinoAnimFinished, playIntroAnim;
    //public GameObject logoPanel;

    //For DoTween
    public RectTransform signUpPanel, loginPanel, otpPanel, introClipPanel;
    Vector2 centerAnchorPos, rightAnchorPos, leftAnchorPos, topAnchorPos, bottomAnchorPos;
    float duration;

    //For Toast Msg
    //public Text toastTxt;

    //For FireBase DataBase
    /*public string userLoggedInName;
    public string userLoggedInId;
    public string userEmailID;
    public string userLoggedInPhoneNumber;*/
    public int totalUserCount;

    bool isUserAdded;


    //For FireBaseLogin
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;
    PhoneAuthProvider provider;
    uint phoneAuthTimeoutMs;
    string phoneId;
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

    //For Loggin Check
    bool userAlreadySignedIn;

    //Golabal Variables
    /* public static string loggedInUserNumber;
     public static string loggedInUserName;*/

    //For Loading Anim
    public GameObject LoadingPanel, loadingImg;
    public float loadingSpeed;
    bool loadAnim;

    //For exit app
    public int currentScrn,previousScrn, exitCounter;
    bool startTimer, msgToasted, inOtpScreen;
    float timeLeft;
    

    //For Logging In
    bool isLoggingIn;

    //For Enter Button Click in keyboard
    TouchScreenKeyboard keyboard;
    bool isInputClicked, isKeyBoardClosed;

    //Script Access
    ToastMessage toastAccess;
    InternetCheck internetScriptAccess;
    ZohoCRM zohoCrmScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetString("UserSignedIn") == "true")
        {
            //logoPanel.SetActive(true);
            //playIntroAnim = false;
            StartCoroutine(LoadYourAsyncScene());
            //SceneManager.LoadScene("MainScene");
            //userAlreadySignedIn = true;
        }
        else
        {
            //logoPanel.SetActive(false);

            userAlreadySignedIn = false;
            //playIntroAnim = true;

            //Play intro Animation
           // anim.Play("New Animation");

            duration = 0.5f;
            centerAnchorPos.Set(0f, 0f);
            leftAnchorPos.Set(-800, 0f);
            rightAnchorPos.Set(800, 0f);
            topAnchorPos.Set(0, 1920);
            bottomAnchorPos.Set(0, -1920);

            ScreenOrder();

            //Script access assign
            toastAccess = FindObjectOfType<ToastMessage>();
            internetScriptAccess = FindObjectOfType<InternetCheck>() as InternetCheck;
            zohoCrmScriptAccess = FindObjectOfType<ZohoCRM>() as ZohoCRM;

            LoadingPanel.SetActive(false);
            loadingSpeed = 35f;
            LoginSceneLoad();
            //currentScrn = 1;
            msgToasted = false;
            timeLeft = 3f;
            isLoggingIn = false;

            isUserAdded = false;

        }

            

    }

    // Update is called once per frame
    void Update()
    {
        if (playIntroAnim)
        {
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("New Animation") && !screenSlided && !isNinoAnimFinished)
            {
                //StartCoroutine(DisableIntroPanel());

                isNinoAnimFinished = true;

                if (isNinoAnimFinished && !userAlreadySignedIn)
                {
                    SlideTop();
                    playIntroAnim = false;
                }
                /*else if(isNinoAnimFinished && userAlreadySignedIn)
                {
                    StartCoroutine(LoadYourAsyncScene());
                }*/

            }
        }
        


        //fullPhoneNumber = countryCode.options[countryCode.value].text + userPhoneNumber.text;
        if(currentScrn == 1)
        {
            //fullPhoneNumber = "+91" + userPhoneNumber.text;
            fullPhoneNumber = countryCodeDD1.options[countryCodeDD1.value].text + userPhoneNumber.text;
        }
        else if(currentScrn == 3 && previousScrn == 1)
        {
            fullPhoneNumber = countryCodeDD1.options[countryCodeDD1.value].text + userPhoneNumber.text;
        }
        else if(currentScrn == 2)
        {
            //fullPhoneNumber = "+91" + loginUserPhoneNumber.text;
            fullPhoneNumber = countryCodeDD2.options[countryCodeDD2.value].text + loginUserPhoneNumber.text;
        }
        else if(currentScrn == 3 && previousScrn == 2)
        {
            fullPhoneNumber = countryCodeDD2.options[countryCodeDD2.value].text + loginUserPhoneNumber.text;
        }
        

        if (loadAnim)
        {
            loadingImg.transform.Rotate(new Vector3(0, 0, -20) * Time.deltaTime * loadingSpeed);
        }

        //To hide the blinking cursor from text field
        if(otpCode.text.Length == 6 && !isKeyBoardClosed)
        {
            otpCode.shouldHideMobileInput = false;
            CloseKeyBoard();
        }
        else
        {
            otpCode.shouldHideMobileInput = true;
        }
        
        if(!isUserAdded && zohoCrmScriptAccess.isLeadCreated )
        {
            AddUserToDB();
            isUserAdded = true;
        }


        //For Enter Button Click
        if (isInputClicked)
        {
            if (keyboard != null && keyboard.status == TouchScreenKeyboard.Status.Done)
            {
                if (currentScrn == 1)
                {
                    SignUpBttnClick();
                }
                else if (currentScrn == 2)
                {
                    LoginBttnClick();
                }
                else if (currentScrn == 3)
                {
                    VerifyOtpBttnClick();
                }

                keyboard = null;
                isInputClicked = false;
                isKeyBoardClosed = true;
            }
            /*else if (keyboard == null)
            {
                isInputClicked = false;
            }
            else if (keyboard.status == TouchScreenKeyboard.Status.Canceled || keyboard.status == TouchScreenKeyboard.Status.LostFocus)
            {
                keyboard = null;
                isInputClicked = false;
            }*/
            
        }
            
        

        //For exit app
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (currentScrn == 1)
            {
                exitCounter++;
                startTimer = true;
            }
            else if(currentScrn == 2)
            {
                GoToSignUpScreen();
            }
            else if(currentScrn != 0 && currentScrn == 3)
            {
                OtpScreenBackBttnClick();
            }
        }
        if (startTimer)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft <= 0)
            {
                startTimer = false;
                exitCounter = 0;
                timeLeft = 3f;
                msgToasted = false;
            }
        }
        if (exitCounter == 1 && !msgToasted)
        {
            toastAccess.showToast("Press again to exit", 1);
            msgToasted = true;
        }
        if (exitCounter == 2)
        {
            startTimer = false;
            exitCounter = 0;
            timeLeft = 3f;
            msgToasted = false;
            Application.Quit();
            Debug.Log("app closed");
        }
    }

    IEnumerator LoadYourAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainScene");
        //asyncLoad.allowSceneActivation = false;

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    void LoginSceneLoad()
    {
        phoneAuthTimeoutMs = 6000;

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                //   app = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.

                InitFireBase();

            }
            else
            {
                Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));

                Debug.Log("dependency not available" + dependencyStatus);
                // Firebase Unity SDK is not safe to use here.
            }
        });

        //DatabaseRef userRef = FirebaseDatabase.getInstance.getRef("users");
        
    }

    void ScreenOrder()
    {
        introClipPanel.DOAnchorPos(centerAnchorPos, duration);
        signUpPanel.DOAnchorPos(bottomAnchorPos, duration);
        loginPanel.DOAnchorPos(rightAnchorPos, duration);
        otpPanel.DOAnchorPos(rightAnchorPos, duration);
        screenSlided = false;

        Invoke("SlideTop", 2f);
    }
    void SlideTop()
    {
        introClipPanel.DOAnchorPos(topAnchorPos, duration + 1f);
        signUpPanel.DOAnchorPos(centerAnchorPos, duration + 1f);
        loginPanel.DOAnchorPos(rightAnchorPos, duration);
        otpPanel.DOAnchorPos(rightAnchorPos, duration);

        screenSlided = true;
        currentScrn = 1;
    }

    void InitFireBase()
    {

        //For FireBase Auth
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        //For FireBase DB & Crashlytics
        FirebaseApp app = FirebaseApp.DefaultInstance;
        // NOTE: You'll need to replace this url with your Firebase App's database
        // path in order for the database connection to work correctly in editor.
        app.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");
        if (app.Options.DatabaseUrl != null)
        {
            app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);

            //To Start Total User Count
            FirebaseDatabase.DefaultInstance.GetReference("Users").ValueChanged += DetectTotalCount;
        }


    }
    public void OnSelectInputField( GameObject gObject)
    {
            isInputClicked = false;
        
            if (currentScrn == 1)
            {
                if (gObject.name == "TextMeshPro - NameInputField")
                {
                    keyboard = TouchScreenKeyboard.Open(userName.text, TouchScreenKeyboardType.NamePhonePad);
                    isInputClicked = true;
                    isKeyBoardClosed = false;
                }
                else if (gObject.name == "TextMeshPro - NumberInputField")
                {
                    keyboard = TouchScreenKeyboard.Open(userPhoneNumber.text, TouchScreenKeyboardType.NumberPad);
                    isInputClicked = true;
                isKeyBoardClosed = false;
            }
                else if (gObject.name == "TextMeshPro - EmailInputField")
                {
                    keyboard = TouchScreenKeyboard.Open(userEmailId.text, TouchScreenKeyboardType.EmailAddress);
                    isInputClicked = true;
                isKeyBoardClosed = false;
            }
            }
            else if (currentScrn == 2)
            {
                keyboard = TouchScreenKeyboard.Open(loginUserPhoneNumber.text, TouchScreenKeyboardType.NumberPad);
                isInputClicked = true;
            isKeyBoardClosed = false;
        }
            else if (currentScrn == 3)
            {
                keyboard = TouchScreenKeyboard.Open(otpCode.text, TouchScreenKeyboardType.NumberPad);
                isInputClicked = true;
            isKeyBoardClosed = false;
        }
    }
    public void OnEditInputField()
    {
        Debug.Log("onEditCalled");
        if (!isInputClicked && keyboard != null)
        {
            isInputClicked = true;
        }
    }

    void CloseKeyBoard()
    {
        keyboard.active = false;
        keyboard = null;
        isInputClicked = false;
        isKeyBoardClosed = true;
    }

    public bool ValidateEmail(string address)
    {
        //var atCharacter:String[];
        //var dotCharacter:String[];

        string[] atCharacter;
        string[] dotCharacter;
        // this splits the input address by the @ symbol. If it finds the @ symbol, it throws it away,
        // and puts what comes before the @ symbol into atCharacter[0], and what comes after the @ symbol
        // into atCharacter[1]
        atCharacter = address.Split("@"[0]);

        //now we check that the returned array is exactly 2 members long, that means there was only 1 @ symbol
        if (atCharacter.Length == 2)
        {
            //here we split the second member returned above by the dot character, and shove the returned info
            // into the dotCharacter array.
            dotCharacter = atCharacter[1].Split("."[0]);
            // now we check the length of the dotCharacter array. If it is greater than or equal to 2, we know
            // we have at least one dot, maybe more than one.
            if (dotCharacter.Length >= 2)
            {
                //Character before @ check
                if (atCharacter[0].Length >= 1)
                {
                    //Character before dot check
                    if(dotCharacter[0].Length >= 1)
                    {
                        // this last check makes sure that there is actual data after the last dot.
                        if (dotCharacter[dotCharacter.Length - 1].Length <= 1)
                        {
                            // fail
                            Debug.Log("Not Valid char after dot");
                            isEmailValid = false;
                            return false;
                        }
                        else
                        {
                            // if we get to here, you have a valid email address format
                            Debug.Log("valid");
                            isEmailValid = true;
                            return true;
                        }
                    }
                    else
                    {
                        Debug.Log("No char before dot");
                        isEmailValid = false;
                        return false;
                    }
                    
                }
                else
                {
                    Debug.Log("No char before @");
                    isEmailValid = false;
                    return false;
                }
                
            }
            else
            {
                // fail
                Debug.Log("No dot");
                isEmailValid = false;
                return false;
            }
        }
        else
        {
            // fail
            Debug.Log("no @");
            isEmailValid = false;
            return false;
        }

    }


    #region ScreenSlideBttnActions

    public void GoToLoginScreen()
    {
        previousScrn = currentScrn;
        signUpPanel.DOAnchorPos(leftAnchorPos, duration);
        otpPanel.DOAnchorPos(rightAnchorPos, duration);
        loginPanel.DOAnchorPos(centerAnchorPos, duration);
        currentScrn = 2;
    }

    public void GoToSignUpScreen()
    {
        previousScrn = currentScrn;
        loginPanel.DOAnchorPos(rightAnchorPos, duration);
        otpPanel.DOAnchorPos(rightAnchorPos, duration);
        signUpPanel.DOAnchorPos(centerAnchorPos, duration);
        currentScrn = 1;
    }
    
    public void OtpScreenBackBttnClick()
    {
        if(previousScrn == 1)
        {
            GoToSignUpScreen();
        }
        else if(previousScrn == 2)
        {
            GoToLoginScreen();
        }
        /*otpPanel.DOAnchorPos(rightAnchorPos, duration);
        loginPanel.DOAnchorPos(centerAnchorPos, duration);*/
        //currentScrn = 2;
    }

    void OtpVerifyScreen()
    {
        previousScrn = currentScrn;
        editablePhoneNumber.text = fullPhoneNumber;

        signUpPanel.DOAnchorPos(leftAnchorPos, duration);
        loginPanel.DOAnchorPos(leftAnchorPos, duration);
        otpPanel.DOAnchorPos(centerAnchorPos, duration);
        currentScrn = 3;
    }

    #endregion

    #region LoginBttnActions

    public void SignUpBttnClick()
    {
        signUpBttn.interactable = false;
        PlayLoadingAnimation();
        ValidateEmail(userEmailId.text);

        if (!internetScriptAccess.internetAvailableChecked || !internetScriptAccess.internetAvailable)
        {
            internetScriptAccess.CheckInternetConnection();
        }
        

        if (userName.text.Length != 0 && userPhoneNumber.text.Length != 0 && userPhoneNumber.text.Length >= 3 && userEmailId.text.Length != 0 && isEmailValid && internetScriptAccess.internetAvailable)
        {
            //PlayLoadingAnimation();

            UserExistsCheck();
        }
        else
        {
            if(userName.text.Length == 0)
            {
                StopLoadingAnimation();
                toastAccess.showToast("Enter Your Name", 2);
            }
            else if(userPhoneNumber.text.Length == 0)
            {
                StopLoadingAnimation();
                toastAccess.showToast("Enter Your Mobile Number", 2);
            }
            else if(userPhoneNumber.text.Length <= 3 && userPhoneNumber.text.Length != 0)
            {
                StopLoadingAnimation();
                toastAccess.showToast("Check Your Mobile Number", 2);
            }
            else if(userEmailId.text.Length == 0)
            {
                StopLoadingAnimation();
                toastAccess.showToast("Enter Your Email Address", 2);
            }
            else if (!isEmailValid)
            {
                StopLoadingAnimation();
                toastAccess.showToast("Invalid Email Address", 2);
            }
            else if (!internetScriptAccess.internetAvailable)
            {
                StopLoadingAnimation();
                toastAccess.showToast("No Internet", 2);
            }

            signUpBttn.interactable = true;
        }
    }

    public void LoginBttnClick()
    {
        loginBttn.interactable = false;
        PlayLoadingAnimation();

        if (!internetScriptAccess.internetAvailableChecked || !internetScriptAccess.internetAvailable)
        {
            internetScriptAccess.CheckInternetConnection();
        }

        if (loginUserPhoneNumber.text.Length != 0 && internetScriptAccess.internetAvailable)
        {
            UserExistsCheck();
        }
        else
        {
            StopLoadingAnimation();
            if(loginUserPhoneNumber.text.Length == 0)
            {
                toastAccess.showToast("Enter Your Mobile Number", 2);
            }
            else if (!internetScriptAccess.internetAvailable)
            {
                toastAccess.showToast("No Internet", 2);
            }
            

            loginBttn.interactable = true;
            
        }
    }
    public void EditableNumBttnClick()
    {
        if(previousScrn == 1)
        {
            GoToSignUpScreen();
        }
        else if(previousScrn == 2)
        {
            GoToLoginScreen();
        }
    }
    public void VerifyOtpBttnClick()
    {
        PlayLoadingAnimation();
        verifyOtpBttn.interactable = false;

        if (!internetScriptAccess.internetAvailableChecked || !internetScriptAccess.internetAvailable)
        {
            internetScriptAccess.CheckInternetConnection();
        }

        if (otpCode.text.Length == 6 && internetScriptAccess.internetAvailable)
        {
            Credential credential = provider.GetCredential(PlayerPrefs.GetString("VerificationId"), otpCode.text);

            OnVerifyCode(credential);
        }
        else
        {
            StopLoadingAnimation();
            if (otpCode.text.Length != 6)
            {
                toastAccess.showToast("Invalid OTP", 2);
            }
            else if(!internetScriptAccess.internetAvailable)
            {
                toastAccess.showToast("No Internet", 2);
            }
            verifyOtpBttn.interactable = true;
        }

    }
    public void SignOutBttnClick()
    {
        auth.SignOut();
    }

    void SendOtp()
    {
        provider = PhoneAuthProvider.GetInstance(auth);

        provider.VerifyPhoneNumber(fullPhoneNumber, phoneAuthTimeoutMs, null,
          verificationCompleted: (credential) =>
          {
              // Auto-sms-retrieval or instant validation has succeeded (Android only).
              // There is no need to input the verification code.
              // `credential` can be used instead of calling GetCredential().

              toastAccess.showToast("OTP Auto Verifed", 1);
              PlayLoadingAnimation();

              //StopLoadingAnimation();
              //OtpVerifyScreen();
              OnVerifyCode(credential);
          },
          verificationFailed: (error) =>
          {
                  // The verification code was not sent.
                  // `error` contains a human readable explanation of the problem.
                  Debug.Log(error);
              StopLoadingAnimation();
              toastAccess.showToast("OTP Not Sent", 1);
              internetScriptAccess.CheckInternetConnection();
              if (!internetScriptAccess.internetAvailable)
              {
                  toastAccess.showToast("No Internet", 2);
              }
              else
              {
                  toastAccess.showToast("Try Again",1);
              }
          },
          codeSent: (id, token) =>
          {
                  // Verification code was successfully sent via SMS.
                  // `id` contains the verification id that will need to passed in with
                  // the code from the user when calling GetCredential().
                  // `token` can be used if the user requests the code be sent again, to
                  // tie the two requests together.


                  //phoneId = id;

                  PlayerPrefs.SetString("VerificationId", id);
              PlayerPrefs.Save();

              StopLoadingAnimation();
                  //showToast("OTP Sent", 1);
                  toastAccess.showToast("OTP Sent", 1);

              OtpVerifyScreen();
          },
          codeAutoRetrievalTimeOut: (id) =>
          {
                  // Called when the auto-sms-retrieval has timed out, based on the given
                  // timeout parameter.
                  // `id` contains the verification id of the request that timed out.
                  toastAccess.showToast("SMS auto retrival timed out", 1);
          }
          );
    }
    #endregion

    void OnVerifyCode(Credential credential)
    {
        auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
            if (task.IsFaulted)
            {
                Debug.Log("SignInWithCredentialAsync encountered an error: " + task.Exception);

                StopLoadingAnimation();
                
                toastAccess.showToast("Verification Failed", 1);
                verifyOtpBttn.interactable = true;

                return;
            }
            else
            {
                /* PlayerPrefs.SetInt(Utils.LOGGED, Utils.PH);
             PlayerPrefs.Save();*/

                FirebaseUser newUser = task.Result;
                Debug.Log("User signed in successfully");
                // This should display the phone number.
                Debug.Log("Phone number: " + newUser.PhoneNumber);
                // The phone number providerID is 'phone'.
                Debug.Log("Phone provider ID: " + newUser.ProviderId);


                //To delete the saved verification Id
                PlayerPrefs.SetString("VerificationId", null);

                if (!isLoggingIn)
                {
                    //To Save User Already Signed in with verification
                    PlayerPrefs.SetString("UserSignedIn", "true");
                    //To save logged in user mobile number
                    PlayerPrefs.SetString("UserLoggedInNumber", fullPhoneNumber);
                    //To save logged in user name
                    PlayerPrefs.SetString("UserLoggedInName", userName.text);
                    //To save logged in user EmailId
                    PlayerPrefs.SetString("UserLoggedInEmailId", userEmailId.text);
                    //To save kit active status
                    PlayerPrefs.SetString("UserKit1ActiveStatus", "false");
                    //To Save all data to disk
                    PlayerPrefs.Save();
                }
                else if (isLoggingIn)
                {
                    //To Save User Already Signed in with verification
                    PlayerPrefs.SetString("UserSignedIn", "true");
                    //To save logged in user mobile number
                    PlayerPrefs.SetString("UserLoggedInNumber", fullPhoneNumber);
                    //To check user entering as logging In
                    PlayerPrefs.SetString("LoggingIn", "true");
                    //To Save all data to disk
                    PlayerPrefs.Save();

                    toastAccess.showToast("logging in", 1);
                }
                

                if(previousScrn == 1)
                {
                    //Create Lead in Zoho CRM
                    zohoCrmScriptAccess.CreateLeadInCRM();

                    //AddUserToDB();

                    
                }
                else if (previousScrn == 2)
                {
                    toastAccess.showToast("sign in successfull", 1);

                    //App version Set
                    PlayerPrefs.SetString("SavedAppVersion", Application.version);
                    PlayerPrefs.Save();

                    //Scene Change
                    SceneManager.LoadScene("MainScene");
                }
                
                //UserExistsCheck();
            }

            
        });
    }




    #region FireBaseDB_Handling

    void DetectTotalCount(object sender, ValueChangedEventArgs argsCount)
    {
        totalUserCount = Convert.ToInt32(argsCount.Snapshot.ChildrenCount.ToString());
    }

    void UserExistsCheck()
    {
        DatabaseReference userRef = FirebaseDatabase.DefaultInstance.GetReference("Users");

        userRef.OrderByChild("phoneNumber").EqualTo(fullPhoneNumber).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Handle the error...
                //showToast("Sign in failed", 1);
                toastAccess.showToast("Try again", 1);
                StopLoadingAnimation();
                signUpBttn.interactable = true;
                loginBttn.interactable = true;
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                // Do something with snapshot...

                if (snapshot.Exists)
                {
                    //showToast("Sign in successfull", 1);
                    //toastAccess.showToast("sign in successfull", 1);

                    Debug.Log("userExist");

                    if(currentScrn == 1)
                    {
                        StopLoadingAnimation();
                        countryCodeDD2.value = countryCodeDD1.value;
                        loginUserPhoneNumber.text = userPhoneNumber.text;
                        GoToLoginScreen();
                        toastAccess.showToast("User already registered", 2);
                        isLoggingIn = false;
                        signUpBttn.interactable = true;
                    }
                    else if(currentScrn == 2)
                    {
                        SendOtp();

                        isLoggingIn = true;
                        loginBttn.interactable = true;
                    }

                    //Scene Change
                    //SceneManager.LoadScene("MainScene");
                }
                else
                {
                    Debug.Log("userNotExist");
                    if (currentScrn == 2)
                    {
                        StopLoadingAnimation();
                        countryCodeDD1.value = countryCodeDD2.value;
                        userPhoneNumber.text = loginUserPhoneNumber.text;
                        GoToSignUpScreen();
                        toastAccess.showToast("User not registered", 2);
                        isLoggingIn = false;
                        loginBttn.interactable = true;
                    }
                    else if(currentScrn == 1)
                    {
                        SendOtp();
                        isLoggingIn = false;
                        signUpBttn.interactable = true;
                    }

                    //AddUserToDB();

                    //showToast("Adding user to FBase", 1);
                }
            }
        });
    }

    public void AddUserToDB()
    {
        //To get the last count at the last frame
        int currentUserCount = totalUserCount;

        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        // mDatabaseRef.Child("Users").Child("0").Child

        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("phoneNumber").SetValueAsync(fullPhoneNumber);
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("userName").SetValueAsync(userName.text);
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("emailID").SetValueAsync(userEmailId.text);
        //mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("score").SetValueAsync(initNinots);
        //mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("location").SetValueAsync("0");

        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("TotalScore").SetValueAsync(0);
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("Level").SetValueAsync(0);

        //To store created lead record id in DB
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("CRM").Child("leadRecordId").SetValueAsync(zohoCrmScriptAccess.userLeadRecordID);

        /*mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("AR").Child("ScannedAR").Child("ship").SetValueAsync(0);
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("AR").Child("ScannedAR").Child("plane").SetValueAsync(0);
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("AR").Child("ScannedAR").Child("brain").SetValueAsync(0);

        //To set kit active status
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("KitActivation").Child("Starterkit1").Child("activeStatus").SetValueAsync("false");

        //To store created lead record id in DB
        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("CRM").Child("leadRecordId").SetValueAsync(zohoCrmScriptAccess.userLeadRecordID);

        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("Level").SetValueAsync(0);

        mDatabaseRef.Child("Users").Child(currentUserCount.ToString()).Child("KitActivation").Child("Starterkit1").Child("currentProject").SetValueAsync(0);
        */

        StopLoadingAnimation();
        //showToast("sign in successfull", 1);
        toastAccess.showToast("sign in successfull", 1);

        isUserAdded = true;

        //App version Set
        PlayerPrefs.SetString("SavedAppVersion", Application.version);
        PlayerPrefs.Save();

        //Scene Change
        SceneManager.LoadScene("MainScene");

        
        

    }

    /*public void FirebaeOneTimeRetriveData()
    {

        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Users");
        reference.OrderByChild("phoneNumber").EqualTo(testString).GetValueAsync().ContinueWith(task =>
        //FirebaseDatabase.DefaultInstance.GetReference("Users").OrderByChild("phoneNumber").EqualTo(testString).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                

                foreach(DataSnapshot myChild in snapshot.Children)
                {
                    string parentKey = myChild.Key;
                    //String value = myChild.Value.ToString();
                    testTxt.text = reference.Child(parentKey).Child("userName").GetValueAsync().Result.Value.ToString();


                    parentString = parentKey;
                }
                testTxt1.text = reference.Child(parentString).Child("userName").GetValueAsync().Result.ToString();
            }
        });
    }*/

        #endregion

        #region LoadingAnimation

        public void PlayLoadingAnimation()
    {
        loadAnim = true;
        LoadingPanel.SetActive(true);
    }
    public void StopLoadingAnimation()
    {
        loadAnim = false;
        LoadingPanel.SetActive(false);
    }

    #endregion

    #region ToastMessage

   /* void showToast(string text,int duration)
    {
        StartCoroutine(showToastCOR(text, duration));
    }

    private IEnumerator showToastCOR(string text,int duration)
    {
        Color orginalColor = toastTxt.color;

        toastTxt.text = text;
        toastTxt.enabled = true;

        //Fade in
        yield return fadeInAndOut(toastTxt, true, 0.5f);

        //Wait for the duration
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        //Fade out
        yield return fadeInAndOut(toastTxt, false, 0.5f);

        toastTxt.enabled = false;
        toastTxt.color = orginalColor;
    }

    IEnumerator fadeInAndOut(Text targetText, bool fadeIn, float duration)
    {
        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0f;
            b = 1f;
        }
        else
        {
            a = 1f;
            b = 0f;
        }

        Color currentColor = toastTxt.color;//Color.clear;
        float counter = 0f;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            targetText.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
            yield return null;
        }
    }*/
    #endregion
}
