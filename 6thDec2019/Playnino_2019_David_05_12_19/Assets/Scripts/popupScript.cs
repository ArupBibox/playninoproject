﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class popupScript : MonoBehaviour
{
    public GameObject firstPopup, SecondPopUp, closeButton;
    public Animator AnimatepopUp;
    // Start is called before the first frame update
    void Start()
    {
        firstPopup.SetActive(true);
        SecondPopUp.SetActive(false);
        closeButton.SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ClosePopUp()
    {
       
        AnimatepopUp.Play("popupPanel 0");
        firstPopup.SetActive(true);
        SecondPopUp.SetActive(false);
        closeButton.SetActive(true);
    }
    public void NoLeaflet()
    {
        firstPopup.SetActive(false);
        SecondPopUp.SetActive(true);
    }
    public void animate()
    {
        AnimatepopUp.Play("popupPanel");
    }
    public void ARMode()
    {
        SceneManager.LoadScene(2);
        Debug.Log("AR");
    }
}
