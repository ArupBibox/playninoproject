﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pilotController : MonoBehaviour
{
    public Animator pilotAnim;
    public Vector2 touchStart;
    public Vector2 touchEnd;
    public string inputDir = "";
    public float pilotSpeed = 2f;
    bool move;
    public int climbTime = 0;
    public GameObject[] pilots;
    public static bool canSwipe;
    bool climbing;
    public GameObject jet3;
    // Start is called before the first frame update
    void Start()
    {
        pilotAnim.Play("Idle");
        pilots[1].SetActive(false);
        jet3.GetComponent<jet3>().enabled = false;
        jet3.GetComponent<changeMaterial>().enabled = false;
        
      
    }

    // Update is called once per frame
    void Update()
    {
       // if (Input.GetKey(KeyCode.D)){
         //   move = true;
        //}
        pilotMove();
        if (climbing)
        {

            climbTime++;
        }
        if (climbTime > 150)
        {
            jet3.GetComponent<jet3>().enabled = true;
            jet3.GetComponent<changeMaterial>().enabled = true;
            
           
            

            pilots[1].SetActive(true);
            pilots[0].SetActive(false);
            canSwipe=true;
         
            Debug.Log("sit");
        }
    }
    public void pilotMove()
    {
        getInput();
        if (move)
        {
            gameObject.transform.Translate(0,0,pilotSpeed * Time.deltaTime);
            pilotAnim.Play("Walking");
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ClimbCollider")
        {
            pilotAnim.Play("Climbing");
            move = false;
            climbing = true;
            
            
        }
       
    }

    public void getInput()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touchStart = Input.touches[0].position;
            }

            else if (Input.touches[0].phase == TouchPhase.Ended)
            {
                touchEnd = Input.touches[0].position;

                Vector2 touchDirection = touchEnd - touchStart;

                if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                {
                    if (touchEnd.x > touchStart.x)
                    {
                        inputDir = "Right";
                        move = true;
                    }
                    else if (touchEnd.x < touchStart.x)
                    {
                        inputDir = "Left";
                    }
                }
                else
                {
                    if (touchEnd.y > touchStart.y)
                    {
                        inputDir = "Up";
                       

                    }
                    else if (touchEnd.y < touchStart.y)
                    {
                        inputDir = "Down";
                    }
                }
            }
        }
    }

}
