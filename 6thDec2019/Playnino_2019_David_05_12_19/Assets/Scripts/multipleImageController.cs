﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class multipleImageController : MonoBehaviour
{
    public string TrackedImageName;
    public bool PlaneShowCased = false, shipShowCased = false;
    public GameObject shipSceneComponents, JetSceneComponents, BrainSceneComponents, yatch;
    public UIManager UIHandler;
    public BrainManager brainHandler;
    bool brainAnimPlay = false;
    bool reScan = false;
    float timer = 0;
    bool loadingIsActive;
    PopUpHandler popupHandlerScript;
    public GameObject ShowCaseModels, ShowCaseModelsShip;
    public Camera ArCam;
    public GameObject showCasePanel, showCasePanelShip;
    public GameObject descript1, descript2;
    UIManager UIManagerAccess;
    changeMaterial materialAccess;
    ShipController shipScriptAccess;
    public Renderer jetBody;
    public Material[] materials;
    public GameObject Scantext;
    float counter = 0;
    public AudioSource jetDescriptSound, jetDescriptSound2, yatchSound;
    bool soundPlayed = false;
    bool yatchSoundPlayed = false;
    public GameObject normalCam, arCam;
    public GameObject prevButton, NextButton;
    public Vector3 shipComponenPosition;
    public GameObject[] platforms;
    public Button[] muteButton;
    public Sprite[] muteButtonImages;
    public GameObject tiltMessage;
    bool jetafterShowcase = false;
    bool shipAfterShowCase = false;
    bool stopPanelFunctionsJet = false;
    bool stopPanelFunctionsShip = false;
    public float infoButtonCounter = 0;
    //public GameObject 

    public bool reset;

    CircuitsUiHandler circuitsUiHandlerScriptAccess;
    CircuitsTargetController circuitsTargetControllerScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        UIManagerAccess = FindObjectOfType<UIManager>() as UIManager;
        materialAccess = FindObjectOfType<changeMaterial>() as changeMaterial;
        shipScriptAccess = FindObjectOfType<ShipController>() as ShipController;
        popupHandlerScript = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        circuitsUiHandlerScriptAccess = FindObjectOfType<CircuitsUiHandler>();
        circuitsTargetControllerScriptAccess = FindObjectOfType<CircuitsTargetController>();

        if (popupHandlerScript != null)
            if (popupHandlerScript.loadingPanel != null)
            {
                popupHandlerScript.loadingPanel.SetActive(false);
            }


        DefaultTrackableEventHandler.TrackImageName = null;

        descript1.SetActive(true);
        descript2.SetActive(false);
        jetDescriptSound.enabled = true;
        jetDescriptSound2.enabled = false;
        NextButton.SetActive(true);
        prevButton.SetActive(false);
        JetSceneComponents.SetActive(false);
        shipSceneComponents.transform.localPosition = new Vector3(shipComponenPosition.x + 3000, shipComponenPosition.y + 3000, shipComponenPosition.z + 3000);
        Debug.Log("start");

        //loadingIsActive = true;





    }

    // Update is called once per frame
    void Update()
    {
        if (reset)
        {
            counter = 0;
            UIManagerAccess.infoButton.SetActive(false);
            TrackedImageName = null;
            // infoButtonCounter += Time.deltaTime;

            reset = false;

        }
        if (DefaultTrackableEventHandler.IsTracked)
        {
            if (Scantext.activeSelf)
            {
                Scantext.SetActive(false);
            }
            /*if (DontHavLeaflet.activeSelf)
            {
                DontHavLeaflet.SetActive(false);
            }*/
            if (TrackedImageName == "PlaneTarget")
            {

                if (!PlaneShowCased)
                {
                    jetInfoPanel();



                }
                else if (PlaneShowCased)
                {

                    infoButtonCounter++;
                    if (infoButtonCounter > 2)
                    {
                        UIManagerAccess.infoButton.SetActive(true);
                        infoButtonCounter = 0;
                    }
                    if (!jetafterShowcase)
                    {
                        JetSceneComponents.SetActive(true);
                        ShowCaseModels.SetActive(false);
                        showCasePanel.SetActive(false);
                        jetafterShowcase = true;
                    }



                }
            }
            if (TrackedImageName == "ShipTarget")
            {


                if (!shipShowCased)
                {
                    TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
                    ShipInfoPanel();



                }
                else if (shipShowCased)
                {
                    //hipSceneComponents.SetActive(true);
                    infoButtonCounter++;
                    if (infoButtonCounter > 1)
                    {
                        UIManagerAccess.infoButton.SetActive(true);
                        infoButtonCounter = 0;
                    }
                    if (!shipAfterShowCase)
                    {
                        ShowCaseModelsShip.SetActive(false);
                        showCasePanelShip.SetActive(false);
                        Debug.Log("showCased");
                        shipAfterShowCase = true;
                        //UIManagerAccess.ScanCircle.SetActive(false);
                    }



                }
            }
        }
        /*else if (!DefaultTrackableEventHandler.IsTracked)
        {
            counter += Time.deltaTime;
            if (ArCam.clearFlags == CameraClearFlags.SolidColor)
            {
                if (counter < 2)
                {
                    Scantext.SetActive(true);
                    DontHavLeaflet.SetActive(false);
                }
            }
            if (ArCam.clearFlags == CameraClearFlags.Skybox)
            {
                UIManagerAccess.ScanCircle.SetActive(false);
                Scantext.SetActive(false);
                DontHavLeaflet.SetActive(false);
            }

            if (counter > 2)
            {
                if (ArCam.clearFlags == CameraClearFlags.SolidColor)
                {
                    if (!UIManagerAccess.popped)
                    {
                        DontHavLeaflet.SetActive(true);
                    }
                    Scantext.SetActive(false);
                }

                if (ArCam.clearFlags == CameraClearFlags.Skybox)
                {
                    UIManagerAccess.ScanCircle.SetActive(false);
                    Scantext.SetActive(false);
                    DontHavLeaflet.SetActive(false);
                }
                counter = 2;
            }

        }*/
        /*if (loadingIsActive)
        {
            popupHandlerScript.loadingPanel.SetActive(false);
            loadingIsActive = false;
        }*/

        if (reScan)
        {
            timer += Time.deltaTime;
            if (timer > 0.5f)
            {
                TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
                reScan = false;
                timer = 0;
            }
        }
        if (UIHandler.backCount > 0)
        {
            BrainSceneComponents.SetActive(false);
            TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
            //shipSceneComponents.SetActive(false);
            JetSceneComponents.SetActive(false);
        }
        if (brainAnimPlay)
        {
            // brainHandler.brain.Play("brainClose");
            // BrainManager.opened = false;
            // brainHandler.partsDisplay.SetActive(false);
            // brainAnimPlay = false;
        }
        TrackedImageName = DefaultTrackableEventHandler.TrackImageName;
        if (TrackedImageName == "BrainTarget")
        {
            if (UIManagerAccess.infoButton.activeSelf)
            {
                UIManagerAccess.infoButton.SetActive(false);
            }
            if (tiltMessage.activeSelf)
            {
                tiltMessage.SetActive(false);
            }
            if (JetSceneComponents.activeSelf)
            {
                JetSceneComponents.SetActive(false);
            }
            if (yatch.activeSelf)
            {
                yatch.SetActive(false);
            }
            if ((!reset) && (UIHandler.backCount == 0))
            {
                BrainSceneComponents.SetActive(true);
                Scantext.SetActive(false);
                //DontHavLeaflet.SetActive(false);
                // brainAnimPlay = true;
            }
            else
            {

                BrainSceneComponents.SetActive(false);
                TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
                reScan = true;




            }
            // shipSceneComponents.SetActive(false);
            // TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
            // reScan = true;

        }
        if (TrackedImageName == "PlaneTarget")    //for jet
        {
            if (BrainSceneComponents.activeSelf)
            {
                BrainSceneComponents.SetActive(false);
            }
            if (yatch.activeSelf)
            {
                yatch.SetActive(false);
            }
            //shipSceneComponents.SetActive(false);
            // TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
            if ((!reset) && (UIHandler.backCount == 0))
            {
                if (PlaneShowCased)
                {
                    JetSceneComponents.SetActive(true);
                    //UIManagerAccess.ScanCircle.SetActive(false);
                }
            }
            else
            {
                JetSceneComponents.SetActive(false);
                TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
                reScan = true;

            }
        }
        if (TrackedImageName == "ShipTarget")  //for ship
        {
            if (tiltMessage.activeSelf)
            {
                tiltMessage.SetActive(false);
            }
            if (BrainSceneComponents.activeSelf)
            {
                BrainSceneComponents.SetActive(false);
            }
            if (JetSceneComponents.activeSelf)
            {
                JetSceneComponents.SetActive(false);
            }
            if (!yatch.activeSelf)
            {
                yatch.SetActive(true);
            }

            if ((!reset) && (UIHandler.backCount == 0))
            {
                if (shipShowCased)
                {
                    shipSceneComponents.SetActive(true);
                }
            }
            else
            {
                // shipSceneComponents.SetActive(false);
                TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
                reScan = true;
            }

        }

    }
    public void LetsFly()
    {
        arCam.SetActive(true);
        normalCam.SetActive(false);
        PlaneShowCased = true;
        UIManagerAccess.playAgain();
        UIManagerAccess.reloadButton.SetActive(true);
        //UIManagerAccess.ScanCircle.SetActive(true);
        // UIManagerAccess.infoButton.SetActive(true);
        
        soundPlayed = false;
        platforms[0].GetComponent<rotate>().enabled = true;

        circuitsTargetControllerScriptAccess.isTargetDetected = false;
        circuitsUiHandlerScriptAccess.ScanCircle_On_Off(true);
    }
    public void LetsSail()
    {
        arCam.SetActive(true);
        normalCam.SetActive(false);
        shipShowCased = true;
        UIManagerAccess.playAgain();
        UIManagerAccess.reloadButton.SetActive(true);
        //  UIManagerAccess.infoButton.SetActive(true);
        ShowCaseModelsShip.SetActive(false);
        showCasePanelShip.SetActive(false);
        shipSceneComponents.transform.localPosition = new Vector3(shipComponenPosition.x, shipComponenPosition.y, shipComponenPosition.z);
        platforms[1].GetComponent<rotate>().enabled = true;
        //shipSceneComponents.transform.SetParent(ShipImageTarget);
        //rackerManager.Instance.GetTracker<ObjectTracker>().Start();

        //circuitsUiHandlerScriptAccess.ReloadBttnClick();
        /*TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
        DefaultTrackableEventHandler.TrackImageName = null;*/

        
        circuitsTargetControllerScriptAccess.isTargetDetected = false;
        circuitsUiHandlerScriptAccess.ScanCircle_On_Off(true);
    }
    public void jetInfoPanel()
    {
        if (!stopPanelFunctionsJet)
        {
            jetafterShowcase = false;
            PlaneShowCased = false;
            ShowCaseModels.SetActive(true);
            JetSceneComponents.SetActive(false);
            ArCam.clearFlags = CameraClearFlags.Skybox;
            // TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
            showCasePanel.SetActive(true);
            UIManagerAccess.reloadButton.SetActive(false);
            UIManagerAccess.infoButton.SetActive(false);
            circuitsUiHandlerScriptAccess.ScanCircle_On_Off(false);
            //UIManagerAccess.ScanCircle.SetActive(false);
            Scantext.SetActive(false);
            //DontHavLeaflet.SetActive(false);
            arCam.SetActive(false);
            normalCam.SetActive(true);
            stopPanelFunctionsJet = true;
        }
        if (!soundPlayed)
        {
            if (jetDescriptSound.enabled)
            {
                jetDescriptSound.Play();


            }
            if (jetDescriptSound2.enabled)
            {
                jetDescriptSound2.Play();
            }
            soundPlayed = true;
        }

    }
    public void ShipInfoPanel()
    {
        if (!stopPanelFunctionsShip)
        {
            shipAfterShowCase = false;
            shipShowCased = false;
            ShowCaseModelsShip.SetActive(true);
            TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
            ArCam.clearFlags = CameraClearFlags.Skybox;
            showCasePanelShip.SetActive(true);
            UIManagerAccess.reloadButton.SetActive(false);
            UIManagerAccess.infoButton.SetActive(false);
            circuitsUiHandlerScriptAccess.ScanCircle_On_Off(false);
            //UIManagerAccess.ScanCircle.SetActive(false);
           
            Scantext.SetActive(false);
            //DontHavLeaflet.SetActive(false);
            shipScriptAccess.startSound.enabled = false;
            shipScriptAccess.MoveSound.enabled = false;
            arCam.SetActive(false);
            normalCam.SetActive(true);
            stopPanelFunctionsShip = true;
        }
        if (!yatchSoundPlayed)
        {
            yatchSound.Play();
            yatchSoundPlayed = false;
        }


    }


    public void NextPlane()
    {
        jetBody.material = materials[1];
        materialAccess.materialNo = 1;
        descript1.SetActive(false);
        descript2.SetActive(true);
        jetDescriptSound.enabled = false;
        jetDescriptSound2.enabled = true;
        jetDescriptSound2.Play();
        NextButton.SetActive(false);
        prevButton.SetActive(true);
    }
    public void PrevPlane()
    {
        jetBody.material = materials[0];
        materialAccess.materialNo = 2;
        descript1.SetActive(true);
        descript2.SetActive(false);
        jetDescriptSound.enabled = true;
        jetDescriptSound2.enabled = false;
        jetDescriptSound.Play();
        NextButton.SetActive(true);
        prevButton.SetActive(false);
    }
    public void MuteButton()
    {

        if (jetDescriptSound.volume == 1)
        {
            jetDescriptSound.volume = 0;
        }
        else if (jetDescriptSound.volume == 0)
        {
            jetDescriptSound.volume = 1;
        }


        if (jetDescriptSound2.volume == 1)
        {
            jetDescriptSound2.volume = 0;
            muteButton[0].image.sprite = muteButtonImages[1];
        }
        else if (jetDescriptSound2.volume == 0)
        {
            jetDescriptSound2.volume = 1;
            muteButton[0].image.sprite = muteButtonImages[0];
        }

    }
    public void InfoButtonFunction()
    {
        if (TrackedImageName == "PlaneTarget")
        {
            stopPanelFunctionsJet = false;
            jetInfoPanel();
        }
        else if (TrackedImageName == "ShipTarget")
        {
            stopPanelFunctionsShip = false;
            ShipInfoPanel();
        }
    }
    public void MuteShipAudio()
    {
        if (yatchSound.volume == 0)
        {
            yatchSound.volume = 1;
            muteButton[1].image.sprite = muteButtonImages[0];
        }
        else if (yatchSound.volume == 1)
        {
            yatchSound.volume = 0;
            muteButton[1].image.sprite = muteButtonImages[1];
        }
    }

}
