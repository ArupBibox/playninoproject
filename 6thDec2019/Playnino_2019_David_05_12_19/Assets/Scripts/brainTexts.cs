﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class brainTexts : MonoBehaviour
{
    public Transform target;
    public GameObject brainTxtObj;



    public float x, y, z;
    // Start is called before the first frame update
    void Start()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
        //brainTxtObj.transform.localEulerAngles = new Vector3(x, y, z);
        transform.rotation = Quaternion.LookRotation(transform.position - target.position);
    }
}
