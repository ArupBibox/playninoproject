﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainManager : MonoBehaviour
{
   // public GameObject videoButton;
    public GameObject iamInterested;
    //public GameObject scanCircle;
   public  Animator brain;
   public static bool opened = false;
    public GameObject partsDisplay;
    public static int TouchCount = 0;
    public bool reset = false;
    public UIManager uiHandler;
    public GameObject watchVideo;
    public GameObject[] touchRipples;
    bool partsShown = false;
    //public PartsDisplay parts;
    // Start is called before the first frame update
    void Start()
    {
        brain = GetComponent<Animator>();
        partsDisplay.SetActive(false);
        this.GetComponent<BoxCollider>().enabled = true;
        opened = false;
        TouchCount = 0;
        watchVideo.SetActive(false);
        if (!partsShown)
        {
            touchRipples[0].SetActive(true);
            touchRipples[1].SetActive(false);
            touchRipples[2].SetActive(false);
            touchRipples[3].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (reset)
        {
            brain.Play("brainClose");
            opened = false;
            brain.SetBool("close", true);

            reset = false;
        }
        
        /*if (uiHandler.replay)
        {
            brain.SetBool("close", true);
            
           
            // initialise();
            // reset = false;
            //  gameObject.SetActive(false);
            // scanCircle.SetActive(true);
            //  iamInterested.SetActive(false);

        }*/

        // if (!uiHandler.replay)
       // {
         //   gameObject.SetActive(true);
       // }
        if (DefaultTrackableEventHandler.IsTracked && DefaultTrackableEventHandler.TrackImageName == "BrainTarget")
        {
            //if (!uiHandler.replay)
            if (!reset)
            {
                //scanCircle.SetActive(false);
            }
           // instructions.SetActive(false);
        }
        if (opened)
        {
            this.GetComponent<BoxCollider>().enabled = false;
            if (!partsShown)
            {
                touchRipples[0].SetActive(false);
                if(!PartsDisplay.CenterTouched)
                touchRipples[1].SetActive(true);
                if (!PartsDisplay.RightTouched)
                    touchRipples[3].SetActive(true);
                if (!PartsDisplay.leftTouched)
                    touchRipples[2].SetActive(true);
            }

        }
        if ((PartsDisplay.leftTouched) && (PartsDisplay.RightTouched) && (PartsDisplay.CenterTouched))
        {
            this.GetComponent<BoxCollider>().enabled = true;
            partsShown = true;
        }
        else  if ((!PartsDisplay.leftTouched) || (!PartsDisplay.RightTouched) || (!PartsDisplay.CenterTouched))
        {
            this.GetComponent<BoxCollider>().enabled = false;
        }

        if (!opened)
        {
            TouchCount = 0;
            this.GetComponent<BoxCollider>().enabled = true;

          
        }
    }
    private void OnMouseDown()
    {
        if (!opened)
        {
            brain.Play("brainOpen");
            opened = true;
            partsDisplay.SetActive(true);
            watchVideo.SetActive(true);
           // iamInterested.SetActive(true);
            //videoButton.SetActive(true);
        }
        else if (opened)
        {
            brain.Play("brainClose");
            opened = false;
            partsDisplay.SetActive(false);
        }
    }
    void initialise()
    {
        partsDisplay.SetActive(false);
        this.GetComponent<BoxCollider>().enabled = true;
        opened = false;
        TouchCount = 0;
        iamInterested.SetActive(false);
        //scanCircle.SetActive(true);
    }
}
