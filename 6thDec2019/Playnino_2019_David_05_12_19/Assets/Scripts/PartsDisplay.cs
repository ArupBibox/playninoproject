﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartsDisplay : MonoBehaviour
{
    public GameObject leftText, rightText, stemText;
    bool Displayed = false;
    public static bool leftTouched, RightTouched, CenterTouched;
    public AudioSource leftSound, RightSound, BrainStem;
    BrainManager brainAccess;
    // Start is called before the first frame update
    void Start()
    {
        //text.SetActive(false);
        leftText.SetActive(false);
        rightText.SetActive(false);
        stemText.SetActive(false);
        Displayed = false;
        leftTouched = false;
        RightTouched = false;
        CenterTouched = false;
        brainAccess = FindObjectOfType<BrainManager>() as BrainManager;

    }

    // Update is called once per frame
    void Update()
    {
        if (!BrainManager.opened)
        {
            this.GetComponent<MeshCollider>().enabled = false;
           // text.SetActive(false);
            leftText.SetActive(false);
            rightText.SetActive(false);
            stemText.SetActive(false);
            Displayed = false;
            leftTouched = false;
            RightTouched = false;
            CenterTouched = false;
        }
        else if (BrainManager.opened)
        {
            if (!Displayed)
            {
                this.GetComponent<MeshCollider>().enabled = true;
            }
        }
    }
    private void OnMouseDown()
    {
        if (BrainManager.opened)
        {
           // text.SetActive(true);
            if (this.gameObject.name == "Brain_Part_04")
            {
                RightTouched = true;
                RightSound.Play();
                leftSound.Stop();
                BrainStem.Stop();
                rightText.SetActive(true);
                leftText.SetActive(false);
                stemText.SetActive(false);
                brainAccess.touchRipples[3].SetActive(false);
            }
            else if ((this.gameObject.name == "Brain_Part_05"))
            {
                CenterTouched = true;
                BrainStem.Play();
                RightSound.Pause();
                leftSound.Pause();
                stemText.SetActive(true);
                leftText.SetActive(false);
                rightText.SetActive(false);
                brainAccess.touchRipples[1].SetActive(false);
            }
            else if ((this.gameObject.name == "Brain_Part_06"))
            {
                leftTouched = true;
                leftSound.Play();
                RightSound.Stop();
                BrainStem.Stop();
                leftText.SetActive(true);
                rightText.SetActive(false);
                stemText.SetActive(false);
                brainAccess.touchRipples[2].SetActive(false);
            }
            // BrainManager.TouchCount++;
            // this.GetComponent<MeshCollider>().enabled = false;
            Displayed = true;

        }
        
    }
}
