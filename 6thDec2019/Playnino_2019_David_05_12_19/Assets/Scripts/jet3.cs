﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jet3 : MonoBehaviour
{
    public Vector2 touchStart;
    public Vector2 touchEnd;
    public string inputDir = "";
    public Animator jet3Controller;
    public float jetSpeed, TurnSpeed = 15f;
    public float Timer = 0;
    bool count;
    public AudioSource StartSound, Flysound, landSound;
    public GameObject upArrow;
    bool StopEngineSound;
    public static bool isJetMove;
    // public GameObject videoButton;
    // public GameObject iamInterested;
    //public GameObject scanCircle;
    bool jetTurnLeft;
    UIManager UIHandler;
    public bool reset = false;
    public float newTimer;
    public Quaternion originalRotationValue, missile1Rotation, missile2Rotation, explosion1Rotation, explosion2Rotation; // declare this as a Quaternion
    float rotationResetSpeed = 0.1f;
    public float delayCount = 0;
    bool StartDelay = false;
    public bool soundFadeIn = false;
    public GameObject exhaust, smoke;
    public float minimumTiltValue = -0.5f;
    public GameObject watchVideo;
    public GameObject message;
    bool displayMessage = false;
    bool messgaeShown = false;
    public bool TurnRight = false, turnBack = false;
    public GameObject LandTarget, lookTarget;
    public bool MovetowardsRunway = false;
    public bool jetLand = false;
    bool afterStopFunctions = false;
    private ParticleSystem particleAccess, particleAccess2;
    float partclespeed = 32;
    public GameObject[] missiles;
    bool activateMissile = false;
    public float missileSpeed = 15;
    public float missileRotateSpeed = 15;
    public GameObject[] missileSmokes;
    // public GameObject[] towers;
    public GameObject[] missileTargets;
    public GameObject[] explosions;
    multipleImageController dataHandler;
    Vector3 missile1Position, missile2Position;
    public AudioSource missileSound, ExplosionSound;
    bool missileTimeActivate = false;
    public float missileTime = 0;
    public bool explode = false;
    public GameObject fireButton;
    Vector3 explosion1Position, explosion2Position;








    // Start is called before the first frame update
    void Start()
    {
        StartSound.enabled = true;
        Flysound.enabled = false;
        StopEngineSound = false;
        originalRotationValue = transform.rotation;
        missile1Rotation = missiles[0].transform.localRotation;
        missile2Rotation = missiles[1].transform.localRotation;// save the initial rotation
       // StartSound.volume = 0;
        exhaust.SetActive(true);
        UIHandler = FindObjectOfType<UIManager>() as UIManager;
        particleAccess = exhaust.GetComponent<ParticleSystem>();
        missileSmokes[0].SetActive(false);
        missileSmokes[1].SetActive(false);
        dataHandler = FindObjectOfType<multipleImageController>() as multipleImageController;
        explosions[0].SetActive(false);
        explosions[1].SetActive(false);
        missile1Position = new Vector3(missiles[0].transform.localPosition.x, missiles[0].transform.localPosition.y, missiles[0].transform.localPosition.z);
        missile2Position = new Vector3(missiles[1].transform.localPosition.x, missiles[1].transform.localPosition.y, missiles[1].transform.localPosition.z);
        fireButton.SetActive(false);
        explosion1Position = new Vector3(explosions[0].transform.localPosition.x, explosions[0].transform.localPosition.y, explosions[0].transform.localPosition.z);
        explosion2Position = new Vector3(explosions[1].transform.localPosition.x, explosions[1].transform.localPosition.y, explosions[1].transform.localPosition.z);
        explosion1Rotation = explosions[0].transform.localRotation;
        explosion2Rotation = explosions[1].transform.localRotation;
        // message.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (DefaultTrackableEventHandler.TrackImageName == "PlaneTarget")
        {
            if (displayMessage)
            {
                Debug.Log("tilt");
                if (Input.acceleration.z < (minimumTiltValue))
                {
                    message.SetActive(true);

                }
                else if (Input.acceleration.z > minimumTiltValue)
                {
                    message.SetActive(false);
                    displayMessage = false;
                    messgaeShown = true;
                }
            }
        }

       /* if (UIHandler.replay)
        {
            reset = true;

        }*/
        if (reset)
        {
            initialise();
            reset = false;
        }

        if (Input.GetKey(KeyCode.A))
        {
            // jet3Controller.applyRootMotion = false;
            //  jet3Controller.Play("jet3Anim");
            // count = true;
            // Flysound.enabled = true;
            // StartSound.enabled = false;
            //StopEngineSound = true;
            //  Handheld.Vibrate();
            // upArrow.SetActive(false);
            //rightArrow.SetActive(false);
            // leftArrow.SetActive(false);
            //isJetMove = true;
            displayMessage = true;
            if (!jetLand)
            {
                var particleVar = particleAccess.main;
                particleVar.startSpeed = 32;
                StartDelay = true;
            }
            displayMessage = true;
        }

            if (Input.GetKey(KeyCode.M))
            {
                if (!activateMissile)
                {
                    activateMissile = true;
                }
            }

        
        if (StartDelay)
        {
            delayCount += Time.deltaTime;

        }
        if (delayCount > 0)
        {
            fireButton.SetActive(true);
            jet3Controller.applyRootMotion = false;
            jet3Controller.Play("jet3Anim");
            count = true;
            Flysound.enabled = true;
            StartSound.enabled = false;
            StopEngineSound = true;
            //  Handheld.Vibrate();
            upArrow.SetActive(false);
            // rightArrow.SetActive(false);
            // leftArrow.SetActive(false);
            isJetMove = true;
            delayCount = 0;
            StartDelay = false;


        }
        if ((Timer > 0) && (Timer < 1))
        {
            Handheld.Vibrate();
        }

        getInput();

        if (count)
        {
            Timer += Time.deltaTime;
        }
        if (Timer >= 1.58f)
        {
            if (!jetLand)
            {
                jetMove();
            }
            //videoButton.SetActive(true);

        }
        if ((Timer > 3) && (Timer < 4))            //Rotate the jet after SomeTime
        {
            gameObject.transform.Rotate(0, -0.4f, 0.7f);
            jetTurnLeft = true;
            watchVideo.SetActive(true);
        }
        if ((Timer > 12) && (Timer < 13))
        {
            gameObject.transform.Rotate(0, 0.4f, -0.7f);
            TurnRight = true;
            jetTurnLeft = false;
        }
        if ((Timer > 15) && (Timer < 19))
        {
            gameObject.transform.Rotate(0, 1.5f, 0.01f);
            landSound.enabled = true;
            turnBack = true;
            jetTurnLeft = false;
            TurnRight = false;

        }
        if (Timer > 17)
        {
            gameObject.transform.LookAt(LandTarget.transform);
        }
        if ((Timer > 20) && (Timer < 24))
        {
            gameObject.transform.Rotate(0, 0, -0.01f);


        }
        if ((Timer > 25) && (Timer < 26))
        {
            gameObject.transform.Rotate(0, 0.4f, 0);


        }
        if ((Timer > 26) && (Timer < 27))
        {
            gameObject.transform.Rotate(0, -0.4f, 0);
            MovetowardsRunway = true;
            //activateMissile = true;
            turnBack = false;
            jetTurnLeft = false;
            TurnRight = false;

        }

        if (Timer > 10)
        {
            if (!MovetowardsRunway)
                Flysound.enabled = false;
        }







        if (DefaultTrackableEventHandler.IsTracked)
        {

            if (DefaultTrackableEventHandler.TrackImageName == "PlaneTarget")
            {
              //  if (scanCircle.activeSelf)
              //  {
                  //  scanCircle.SetActive(false);
              //  }
                //instructions.SetActive(false);
                if (!StopEngineSound)
                {
                    if (delayCount > 0)
                    {
                       // soundFadeIn = true;
                        StartSound.enabled = true;
                      //  exhaust.SetActive(true);


                    }
                    //  StartSound.Play();
                    //StartSound.volume -= 0.006f;
                }
            }
        }
        else if (!DefaultTrackableEventHandler.IsTracked)
        {
            // StartSound.Pause();

        }
        if (StopEngineSound)
        {
            Flysound.volume -= 0.1f;
            if (Flysound.volume < 0.09f)
            {
                Flysound.volume = 0.09f;
            }
        }
       // if (soundFadeIn)
       // {

          //  StartSound.volume += 0.003f;

       // }
        if (jetLand)
        {

            //gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, lookTarget.transform.position, 50 * Time.deltaTime);
            gameObject.transform.LookAt(lookTarget.transform);
            gameObject.transform.Translate(new Vector3(0, 0, jetSpeed * Time.deltaTime));


        }
        if (afterStopFunctions)
        {
            var particleVar = particleAccess.main;
            particleVar.startSpeed = 0;
            smoke.SetActive(false);
            landSound.volume -= 0.03f;

            // main2.startSpeed = partclespeed;
        }


        //  newTimer += Time.deltaTime;
        if (activateMissile)
        {


            missileSound.enabled = true;
            missiles[0].transform.parent = dataHandler.JetSceneComponents.transform;
            missiles[1].transform.parent = dataHandler.JetSceneComponents.transform;
            // missiles[0].transform.position = Vector3.MoveTowards(missiles[0].transform.position, missileTargets[1].transform.position, missileSpeed * Time.deltaTime);
            // missiles[1].transform.position = Vector3.MoveTowards(missiles[1].transform.position, missileTargets[0].transform.position, missileSpeed * Time.deltaTime);
            missiles[0].transform.Translate(new Vector3(0, missileSpeed * Time.deltaTime, 0));
            missiles[1].transform.Translate(new Vector3(0, missileSpeed * Time.deltaTime, 0));
            missiles[0].transform.Rotate(new Vector3(0, missileRotateSpeed * Time.deltaTime, 0));
            missiles[1].transform.Rotate(new Vector3(0, missileRotateSpeed * Time.deltaTime, 0));
            missileSmokes[0].SetActive(false);
            missileSmokes[1].SetActive(false);
            missileTimeActivate = true;

        }
        if (missileTimeActivate)
        {
            missileTime += Time.deltaTime;
            if (missileTime > 1.5f)
            {
                activateMissile = false;
                explode = true;
                explosions[0].transform.parent = dataHandler.JetSceneComponents.transform;
                explosions[1].transform.parent = dataHandler.JetSceneComponents.transform;

                //missileTime is reset in MissileExplodeScript
            }
        }


        //  newTimer += Time.deltaTime;

    }
    void initialise()
    {

        // materialhandler.jetBody.material =materialhandler. materials[1];
        Debug.Log("initialise");
        StartSound.volume = 1;
        Flysound.volume = 1;
        upArrow.SetActive(true);
        // rightArrow.SetActive(true);
        // leftArrow.SetActive(true);
        // iamInterested.SetActive(false);
        StopEngineSound = false;
        count = false;
        isJetMove = false;
        jetTurnLeft = false;

        StartSound.enabled = true;
        Flysound.enabled = false;
        StopEngineSound = false;
        Timer = 0;
        newTimer = 0;
        gameObject.transform.localPosition = new Vector3(0.0002882099f, 0.0840053f, 0.06099997f);
        transform.localRotation = Quaternion.Slerp(transform.rotation, originalRotationValue, Time.time * rotationResetSpeed);
        //StartSound.volume = 0;
        delayCount = 0;
        StartDelay = false;
        soundFadeIn = false;
       // exhaust.SetActive(false);
        watchVideo.SetActive(false);
        message.SetActive(false);
        jetLand = false;
        jet3Controller.applyRootMotion = true;
        jetSpeed = 80;
        afterStopFunctions = false;
        landSound.enabled = false;
        landSound.volume = 1;
        smoke.SetActive(true);
        activateMissile = false;
        // towers[0].SetActive(true);
        // towers[1].SetActive(true);
        missileSmokes[0].SetActive(false);
        missileSmokes[1].SetActive(false);
        missiles[0].SetActive(true);
        missiles[1].SetActive(true);
        explosions[0].SetActive(false);
        explosions[1].SetActive(false);
        missileSound.enabled = false;
        ExplosionSound.enabled = false;
        missiles[0].transform.parent = gameObject.transform;
        missiles[1].transform.parent = gameObject.transform;
        explosions[0].transform.parent = missiles[0].transform;
        explosions[1].transform.parent = missiles[1].transform;
        explosions[0].transform.localPosition = explosion1Position;
        explosions[1].transform.localPosition = explosion2Position;
        explosions[0].transform.localRotation = Quaternion.Slerp(transform.rotation, explosion1Rotation, Time.time * rotationResetSpeed);
        explosions[1].transform.localRotation = Quaternion.Slerp(transform.rotation, explosion2Rotation, Time.time * rotationResetSpeed);
        missiles[0].transform.localPosition = missile1Position;
        missiles[1].transform.localPosition = missile2Position;
        missiles[0].transform.localRotation = Quaternion.Slerp(transform.rotation, missile1Rotation, Time.time * rotationResetSpeed);
        missiles[1].transform.localRotation = Quaternion.Slerp(transform.rotation, missile2Rotation, Time.time * rotationResetSpeed);
        missileTimeActivate = false;
        missileTime = 0;
        explode = false;
        fireButton.SetActive(false);



    }

    public void getInput()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touchStart = Input.touches[0].position;
            }

            else if (Input.touches[0].phase == TouchPhase.Ended)
            {
                touchEnd = Input.touches[0].position;

                Vector2 touchDirection = touchEnd - touchStart;

                if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                {
                    if (touchEnd.x > touchStart.x)
                    {
                        inputDir = "Right";


                    }
                    else if (touchEnd.x < touchStart.x)
                    {
                        inputDir = "Left";

                    }
                }
                else
                {
                    if (touchEnd.y > touchStart.y)
                    {
                        if (DefaultTrackableEventHandler.IsTracked)
                        {
                            if (DefaultTrackableEventHandler.TrackImageName == "PlaneTarget")
                            {
                                inputDir = "Up";

                                /*  jet3Controller.applyRootMotion = false;
                                  jet3Controller.Play("jet3Anim");
                                  count = true;
                                  Flysound.enabled = true;
                                  // Flysound.Play();
                                  StartSound.enabled = false;
                                  StopEngineSound = true;
                                  //  Handheld.Vibrate();
                                  upArrow.SetActive(false);
                                  rightArrow.SetActive(false);
                                  leftArrow.SetActive(false);
                                  isJetMove = true;*/
                                if (!jetLand)
                                {
                                    StartDelay = true;
                                    var particleVar = particleAccess.main;
                                    particleVar.startSpeed = 32;
                                }
                                if (!messgaeShown)
                                    displayMessage = true;
                            }
                        }
                        else if (!DefaultTrackableEventHandler.IsTracked)
                        {
                            // Flysound.Pause();
                        }


                    }
                    else if (touchEnd.y < touchStart.y)
                    {
                        inputDir = "Down";
                    }
                }
            }
        }
    }
    public void jetMove()
    {
        jet3Controller.applyRootMotion = true;


        if ((!jetTurnLeft) && (!TurnRight) && (!turnBack) && (!MovetowardsRunway))
        {

            gameObject.transform.Translate(new Vector3(0, 0, jetSpeed * Time.deltaTime));
            Debug.Log("moving");
        }
        else if (jetTurnLeft)
        {
            jetSpeed = 80f;
            gameObject.transform.Translate(new Vector3(TurnSpeed * Time.deltaTime, 0, jetSpeed * Time.deltaTime));
            Debug.Log("moving");
        }
        else if (TurnRight)
        {
            jetSpeed = 80f;
            gameObject.transform.Translate(new Vector3(0, 0, jetSpeed * Time.deltaTime));
            Debug.Log("moving");


        }
        else if (turnBack)
        {
            gameObject.transform.Translate(new Vector3(0, 0, jetSpeed * Time.deltaTime));
            // gameObject.transform.LookAt(LandTarget.transform);

            Debug.Log("turned back");
        }
        else if (MovetowardsRunway)
        {

            {
                jetSpeed = 80f;
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, LandTarget.transform.position, jetSpeed * Time.deltaTime);


                // gameObject.transform.LookAt(lookTarget.transform);
            }
            // gameObject.transform.LookAt(LandTarget.transform);
            // Debug.Log("landing");
        }




    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "LandCollide")
        {
            if (Timer > 10)
            {
                Debug.Log("landed");
                gameObject.transform.LookAt(lookTarget.transform);
                MovetowardsRunway = false;
                Flysound.volume += 0.003f;
                // gameObject.transform.Translate(new Vector3(0, 0, jetSpeed * Time.deltaTime));
                jetLand = true;


            }
        }
        else if (collision.gameObject.name == "stopCollide")
        {
            if (Timer > 10)
            {
                jetSpeed = 0;
                afterStopFunctions = true;



            }
        }
    }
    public void FireButtonFunction()
    {
        if (!activateMissile)
        {
            activateMissile = true;
        }
    }


}


