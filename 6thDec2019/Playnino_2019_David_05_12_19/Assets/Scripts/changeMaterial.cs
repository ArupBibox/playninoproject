﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeMaterial : MonoBehaviour
{
    public Renderer jetBody;
    public Material[] materials;
    public Vector2 touchStart;
    public Vector2 touchEnd;
    public string inputDir = "";
    public int materialNo = 2;
   // public GameObject rightArrow, leftArrow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        getInput();
        if (materialNo > 3)
        {
            materialNo = 3;
        }
        if (materialNo < 1)
        {
            materialNo = 1;
        }
        if (materialNo == 1)
        {
            jetBody.material = materials[0];
        }
        if (materialNo == 2)
        {
            jetBody.material = materials[1];
        }
       // if (materialNo == 3)
       // {
       //     jetBody.material = materials[2];
      //  }

    }
    public void getInput()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touchStart = Input.touches[0].position;
            }

            else if (Input.touches[0].phase == TouchPhase.Ended)
            {
                touchEnd = Input.touches[0].position;

                Vector2 touchDirection = touchEnd - touchStart;

                if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y))
                {
                    if (touchEnd.x > touchStart.x)
                    {
                        inputDir = "Right";
                        if ((!jet3.isJetMove) || (!ShipController.shipMove))
                        {
                           // materialNo--;
                        }
                        
                    }
                    else if (touchEnd.x < touchStart.x)
                    {
                        inputDir = "Left";
                        if( (!jet3.isJetMove) || (!ShipController.shipMove))
                        {
                           // materialNo++;
                        }
                        //rightArrow.SetActive(false);
                        //leftArrow.SetActive(false);
                    }
                }
                else
                {
                    if (touchEnd.y > touchStart.y)
                    {
                        
                            inputDir = "Up";
                           

                    }
                    else if (touchEnd.y < touchStart.y)
                    {
                        inputDir = "Down";
                    }
                }
            }
        }
    }
}
