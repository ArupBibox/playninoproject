﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeatherChange : MonoBehaviour
{

    public Text timeTxt;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(System.DateTime.Now.Hour);

        //To detect current time
        if (System.DateTime.Now.Hour >= 0 && System.DateTime.Now.Hour < 6)
        {
            timeTxt.text = "MidNight";
        }
        if (System.DateTime.Now.Hour >= 6 && System.DateTime.Now.Hour < 12)
        {
            timeTxt.text = "Morning";
        }
        if (System.DateTime.Now.Hour >= 12 && System.DateTime.Now.Hour < 16)
        {
            timeTxt.text = "AfterNoon";
        }
        if (System.DateTime.Now.Hour >= 16 && System.DateTime.Now.Hour < 18)
        {
            timeTxt.text = "Evening";
        }
        if (System.DateTime.Now.Hour >= 18 && System.DateTime.Now.Hour < 24)
        {
            timeTxt.text = "Night";
        }
    }
}
