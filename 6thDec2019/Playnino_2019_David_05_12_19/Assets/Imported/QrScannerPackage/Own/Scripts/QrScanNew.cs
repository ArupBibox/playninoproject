﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using UnityEngine.UI;
using TMPro;
using Vuforia;


public class QrScanNew : MonoBehaviour
{
    public string scannedQrKitName, userParentKeyInDB, codeParentKeyInDB;
    public string[] kitsName, splittedString;
    public bool codeExist, isCodeFormatValid, gotCodeParentKey, loadKit;
    public Text toastTxt;
    public TMP_Text scanAgainPopupTxt;
    public int codeLength, loopCount, totalUsedCodeCount;
    //public GameObject tryAgainButton, downloadPanel;

    //Made as static variable to access between two scenes
    public static bool codeVerified;

    public bool isGoingBack;

    //To Access Script
    SimpleDemo simpleDemoScripAccess;
    LoadingAnim loadingAnimScriptAccess;
    ProfileHandler profileHandlerScriptAccess;
    PopUpHandler popupHandlerScriptAccess;
    public DownloadAndLoad downloadScriptAccess;
    // Start is called before the first frame update
    void Start()
    {
        simpleDemoScripAccess = FindObjectOfType<SimpleDemo>() as SimpleDemo;
        loadingAnimScriptAccess = FindObjectOfType<LoadingAnim>() as LoadingAnim;
        profileHandlerScriptAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        popupHandlerScriptAccess = FindObjectOfType<PopUpHandler>() as PopUpHandler;
       // downloadScriptAccess = FindObjectOfType<DownloadAndLoad>() as DownloadAndLoad;
        userParentKeyInDB = PlayerPrefs.GetString("UserLoggedInParentName");

        kitsName = new string[] { "Starterkit1", "Starterkit2", "Starterkit3" };
        codeLength = 5;
        gotCodeParentKey = false;
        loadKit = false;
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

        FirebaseDatabase.DefaultInstance.GetReference("QrCodes").Child("UsedQrCodes").ValueChanged += DetectTotalCount;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackBttnClick();
        }
    }

    void DetectTotalCount(object sender, ValueChangedEventArgs argsCount)
    {
        totalUsedCodeCount = Convert.ToInt32(argsCount.Snapshot.ChildrenCount.ToString());
    }

    public void CodeFormatValidate()
    {
        loopCount = 0;

        loadingAnimScriptAccess.PlayLoadingAnimation();

        //First 5 Char of QR Code
        splittedString[0] = simpleDemoScripAccess.scannedCode.Substring(0, codeLength);
        //Remaining char of QR code
        splittedString[1] = simpleDemoScripAccess.scannedCode.Substring(codeLength, simpleDemoScripAccess.scannedCode.Length - codeLength);

        if(splittedString[0].Length == 5)
        {
            for(int i = 0; i < kitsName.Length; i++)
            {
                loopCount++;
                if(splittedString[1] == kitsName[i])
                {
                    isCodeFormatValid = true;
                    scannedQrKitName = kitsName[i];
                    CheckCodeInFireBase();
                }
                else if(loopCount == kitsName.Length && isCodeFormatValid == false)
                {
                    loadingAnimScriptAccess.StopLoadingAnimation();
                    isCodeFormatValid = false;
                    Debug.Log("code not valid");
                    scanAgainPopupTxt.text = "Invalid QR Code";
                    simpleDemoScripAccess.ShowTryAgainPopUp();
                }
            }
        }
    }

    void CheckCodeInFireBase()
    {
        DatabaseReference userRef = FirebaseDatabase.DefaultInstance.GetReference("QrCodes");

        userRef.Child("UnusedQrCodes").OrderByChild("code").EqualTo(splittedString[0]).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...
                loadingAnimScriptAccess.StopLoadingAnimation();
                scanAgainPopupTxt.text = "Oops !! Something went wrong.";
                simpleDemoScripAccess.ShowTryAgainPopUp();
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                // Do something with snapshot...

                if (snapshot.Exists)
                {
                    Debug.Log("codeExist in unused code");

                    //Search for parent name
                    foreach (DataSnapshot myChild in snapshot.Children)
                    {
                        //parent name
                        string parentKey = myChild.Key;
                        codeParentKeyInDB = parentKey;
                        gotCodeParentKey = true;
                    }
                    //loadingAnimScriptAccess.StopLoadingAnimation();
                    StoreDataUnderUser();
                    //Invoke("OpenKit1", 2f);
                    
                }
                else
                {
                    Debug.Log("codeNotExist in unused code");

                    //This checks in usedCodes
                    userRef.Child("UsedQrCodes").OrderByChild("code").EqualTo(splittedString[0]).GetValueAsync().ContinueWith(task1 =>
                    {
                        if (task1.IsFaulted)
                        {
                            // Handle the error...
                            loadingAnimScriptAccess.StopLoadingAnimation();
                            scanAgainPopupTxt.text = "Oops !! Something went wrong.";
                            simpleDemoScripAccess.ShowTryAgainPopUp();
                        }
                        else if (task1.IsCompleted)
                        {
                            DataSnapshot snapshot1 = task1.Result;
                            // Do something with snapshot...

                            if (snapshot1.Exists)
                            {
                                loadingAnimScriptAccess.StopLoadingAnimation();
                                scanAgainPopupTxt.text = "Code Already In Use";
                                simpleDemoScripAccess.ShowTryAgainPopUp();
                                Debug.Log("codeExist in used code");
                            }
                            else
                            {
                                loadingAnimScriptAccess.StopLoadingAnimation();
                                scanAgainPopupTxt.text = "Invalid QR Code";
                                simpleDemoScripAccess.ShowTryAgainPopUp();
                                Debug.Log("codeNotExist in used code");
                            }
                        }
                    });
                }
            }
        });

    }

    void StoreDataUnderUser()
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        mDatabaseRef.Child("Users").Child(userParentKeyInDB.ToString()).Child("KitActivation").Child(splittedString[1].ToString()).Child("activeStatus").SetValueAsync("true");

        PlayerPrefs.SetString("UserKit1ActiveStatus", "true");
        PlayerPrefs.Save();

        MoveUnusedCodeToUsedCodeInDB();
    }

    void MoveUnusedCodeToUsedCodeInDB()
    {
        //To get the last count at the last frame
        int currentUsedCodeCount = totalUsedCodeCount;

        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        if (gotCodeParentKey)
        {
            //This write the scanned code in used qr code directory
            mDatabaseRef.Child("QrCodes").Child("UsedQrCodes").Child(currentUsedCodeCount.ToString()).Child("code").SetValueAsync(splittedString[0].ToString());

            //This remove the scanned code from unused qr code directory
            mDatabaseRef.Child("QrCodes").Child("UnusedQrCodes").Child(codeParentKeyInDB.ToString()).Child("code").RemoveValueAsync();

            codeVerified = true;
           /* profileHandlerScriptAccess.fetchUserDetails = true;
            profileHandlerScriptAccess.FetchUserDetails();*/

            //loadingAnimScriptAccess.StopLoadingAnimation();

            //DownloadKit();
            //SceneManager.UnloadSceneAsync("qrCodeScanner");
            //SceneManager.LoadScene("StarterKit1UI_v1.0.7", LoadSceneMode.Additive);
            loadKit = true;
            //downloadScriptAccess.DownloadBundle();

            ////To close/stop camera properly
            //simpleDemoScripAccess.ChangeScene();

            //// If we call this function without invoke it crashing the app
            Invoke("OpenKit1", 2f);
        }
        else
        {
            Invoke("MoveUnusedCodeToUsedCodeInDB", 1f);
        }
    }

    void OpenKit1()
    {
        loadingAnimScriptAccess.StopLoadingAnimation();

        SceneManager.UnloadSceneAsync("qrCodeScanner");

        VuforiaRuntime.Instance.InitVuforia();
        Screen.orientation = ScreenOrientation.Landscape;
        //KitsHandler.circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);

        SceneManager.LoadSceneAsync("StarterKit1_v1", LoadSceneMode.Additive);
    }

    void DownloadKit()
    {
        SceneManager.UnloadSceneAsync("qrCodeScanner");
        SceneManager.LoadSceneAsync("AssestsBundleDownload",LoadSceneMode.Additive);
    }

    public void BackBttnClick()
    {
        isGoingBack = true;
        popupHandlerScriptAccess.mainUI.SetActive(true);
        simpleDemoScripAccess.ChangeScene();
    }
    
    #region ToastMessage

    void showToast(string text, int duration)
    {
        StartCoroutine(showToastCOR(text, duration));
    }

    private IEnumerator showToastCOR(string text, int duration)
    {
        Color orginalColor = toastTxt.color;

        toastTxt.text = text;
        toastTxt.enabled = true;

        //Fade in
        yield return fadeInAndOut(toastTxt, true, 0.5f);

        //Wait for the duration
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        //Fade out
        yield return fadeInAndOut(toastTxt, false, 0.5f);

        toastTxt.enabled = false;
        toastTxt.color = orginalColor;
    }

    IEnumerator fadeInAndOut(Text targetText, bool fadeIn, float duration)
    {
        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0f;
            b = 1f;
        }
        else
        {
            a = 1f;
            b = 0f;
        }

        Color currentColor = Color.clear;
        float counter = 0f;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            targetText.color = new Color(currentColor.r, currentColor.g, currentColor.b, alpha);
            yield return null;
        }
    }

    #endregion


}
