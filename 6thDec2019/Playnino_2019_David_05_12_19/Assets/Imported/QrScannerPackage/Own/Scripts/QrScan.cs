using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Wizcorp.Utils.Logger;

public class QrScan : MonoBehaviour {

	private IScanner BarcodeScanner;
	public Text TextHeader;
	public RawImage Image;
	public AudioSource Audio;
	public GameObject scanner, scannerLine;
	public bool isQrFounded; //QR Code found Check
    public bool isCameraStopped;

    //public screenSlideConrollr slideScriptAccess;

	// Disable Screen Rotation on that screen
	void Awake()
	{
		/*Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;*/
	}
	
	void Start () {

        isQrFounded = false;
	}
	
	void Update () {
		if (BarcodeScanner == null)
		{
			return;
            Debug.Log("BarCodeNull");
		}
		BarcodeScanner.Update();
	}

    public void StartQrProcess()
    {
        Debug.Log("ProcessStarted");
        isQrFounded = false;
        isCameraStopped = false;

        // Create a basic scanner
        BarcodeScanner = new Scanner();
        BarcodeScanner.Camera.Play();

        // Display the camera texture through a RawImage
        BarcodeScanner.OnReady += (sender, arg) => {
            // Set Orientation & Texture
            Image.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
            Image.transform.localScale = BarcodeScanner.Camera.GetScale();
            Image.texture = BarcodeScanner.Camera.Texture;

            // Keep Image Aspect Ratio
            var rect = Image.GetComponent<RectTransform>();
            var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);
        };

        // Track status of the scanner
        BarcodeScanner.StatusChanged += (sender, arg) => {
            TextHeader.text = "Status: " + BarcodeScanner.Status;
        };
        Invoke("StartScan", 0.5f);
    }

	public void StartScan()
	{
		if (BarcodeScanner == null)
		{
			Log.Warning("No valid camera - Start Again");
			return;
		}
		//scanner.SetActive(true);
		scannerLine.GetComponent<Animator>().speed = 0.7f;

		//StartScanning
		BarcodeScanner.Scan((barCodeType, barCodeValue) =>
		{
			BarcodeScanner.Stop();
            BarcodeScanner.Camera.Stop();
			TextHeader.text = "Found: " + barCodeType + "/" + barCodeValue;
			scannerLine.GetComponent<Animator>().speed = 0;

            //If its URL Use below code to open browser
            //Application.OpenURL(barCodeValue);

            isQrFounded = true;
			Audio.Play();
            //Goes back to BeakScene
            StopCamera();

            #if UNITY_ANDROID || UNITY_IOS
            Handheld.Vibrate();
            #endif
		});
	}
	public void StopCamera()
	{
        // Try to stop the camera before loading another scene
        StartCoroutine(StopCamera1(() => {
            //SceneManager.LoadScene("BLEWorking 1");
            isCameraStopped = true;
            //slideScriptAccess.ScanningQrBackBttnClick();

        }));
	}
    public IEnumerator StopCamera1(Action callback)
    {
        // Stop Scanning
        if (!isQrFounded)
        {
            BarcodeScanner.Stop();
            scannerLine.GetComponent<Animator>().speed = 0;
            BarcodeScanner.Camera.Stop();

            Image = null;
            BarcodeScanner.Destroy();
            BarcodeScanner = null;

            // Wait a bit
            yield return new WaitForSeconds(0.1f);

            callback.Invoke();
        }
        else
        {
            Image = null;
            BarcodeScanner.Destroy();
            BarcodeScanner = null;

            // Wait a bit
            yield return new WaitForSeconds(0.1f);

            callback.Invoke();
        }
        
        

        
    }
}
