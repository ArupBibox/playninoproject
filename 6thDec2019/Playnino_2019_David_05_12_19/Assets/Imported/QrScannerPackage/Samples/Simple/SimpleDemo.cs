using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Wizcorp.Utils.Logger;
using DG.Tweening;
using Vuforia;

public class SimpleDemo : MonoBehaviour {

	private IScanner BarcodeScanner;
	public Text TextHeader;
	public RawImage Image;
	public AudioSource Audio;
	public GameObject scanner,scannerLine;

    bool openKitScene;

    public string scannedCode;
    public RectTransform tryAgainPopUpPanel;
    Vector2 centerAnchorPos, bottomAnchorPos;
    float duration;

    public FireBaseRetrieveData fbDB_ScriptAccess;
    QrScanNew qrScanNewScriptAccess;
    //LoadingAnim loadingAnimScriptAccess;

    // Disable Screen Rotation on that screen
    void Awake()
	{
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
    }

    void Start ()
    {
        fbDB_ScriptAccess = FindObjectOfType<FireBaseRetrieveData>();
        qrScanNewScriptAccess = FindObjectOfType<QrScanNew>() as QrScanNew;
        //loadingAnimScriptAccess = FindObjectOfType<LoadingAnim>() as LoadingAnim;

        duration = 0.25f;
        centerAnchorPos.Set(0f, 0f);
        bottomAnchorPos.Set(0, -1280f);

        tryAgainPopUpPanel.DOAnchorPos(bottomAnchorPos, duration);

        // Create a basic scanner
        BarcodeScanner = new Scanner();
		BarcodeScanner.Camera.Play();

		// Display the camera texture through a RawImage
		BarcodeScanner.OnReady += (sender, arg) => {
			// Set Orientation & Texture
			Image.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
			Image.transform.localScale = BarcodeScanner.Camera.GetScale();
			Image.texture = BarcodeScanner.Camera.Texture;

			// Keep Image Aspect Ratio
			var rect = Image.GetComponent<RectTransform>();
			var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
			rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);
		};

		// Track status of the scanner
		BarcodeScanner.StatusChanged += (sender, arg) => {
			//TextHeader.text = "Status: " + BarcodeScanner.Status;
		};
        //Invoke("ClickStart", 0.5f);
	}

	/// <summary>
	/// The Update method from unity need to be propagated to the scanner
	/// </summary>
	void Update()
	{
		if (BarcodeScanner == null)
		{
			return;
		}
		BarcodeScanner.Update();

        /*if (openKitScene)
        {
            SceneManager.UnloadSceneAsync("qrCodeScanner");

            VuforiaRuntime.Instance.InitVuforia();
            Screen.orientation = ScreenOrientation.Landscape;
            //KitsHandler.circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);

            SceneManager.LoadSceneAsync("StarterKit1_v1", LoadSceneMode.Additive);

            openKitScene = false;
        }*/
	}

	#region UI Buttons

	public void ClickStart()
	{
		if (BarcodeScanner == null)
		{
			Log.Warning("No valid camera - Click Start");
			return;
		}
		scanner.SetActive(true);
		scannerLine.GetComponent<Animator>().speed = 0.7f;
		// Start Scanning
		BarcodeScanner.Scan((barCodeType, barCodeValue) => {
			BarcodeScanner.Stop();
			//TextHeader.text = "Found: " + barCodeType + " / " + barCodeValue;
			scannerLine.GetComponent<Animator>().speed = 0;
            //Application.OpenURL(barCodeValue);

            //Assign scanned value to string
            scannedCode = barCodeValue;
            // Feedback
            //Audio.Play();
            //ItHasFound();
            // CheckCodeInFireBase();

            qrScanNewScriptAccess.CodeFormatValidate();

			#if UNITY_ANDROID || UNITY_IOS
			Handheld.Vibrate();
			#endif
		});

	}

    void CheckCodeInFireBase()
    {
        for (int i = 0; i < fbDB_ScriptAccess.qr_Values.Length; i++)
        {
            //Verify code through database

            if (scannedCode == fbDB_ScriptAccess.qr_Values[i])
            {
               // codeVerified = true;
                CodeChecked();
            }
            else
            {
               // codeVerified = false;
                CodeChecked();
            }
        }
        
    }
    void CodeChecked()
    {
        /*if (codeVerified)
        {
            ChangeScene();
        }
        else
        {
            ShowTryAgainPopUp();
        }*/
        
    }
    public void ShowTryAgainPopUp()
    {
        tryAgainPopUpPanel.DOAnchorPos(centerAnchorPos, duration);
        Debug.Log("try again");
    }
    
	public void ScanAgainBttnClick()
	{
        tryAgainPopUpPanel.DOAnchorPos(bottomAnchorPos, duration);
        StartCoroutine(WaitLittle());
	}
	IEnumerator WaitLittle()
	{
		scanner.SetActive(false);
		yield return new WaitForSeconds(0.5f);
		ClickStop();
		ClickStart();
	}
    public void ClickStop()
    {
        if (BarcodeScanner == null)
        {
            Log.Warning("No valid camera - Click Stop");
            return;
        }

        // Stop Scanning
        BarcodeScanner.Stop();
    }


    public void ChangeScene()
    {
        // Try to stop the camera before loading another scene
        StartCoroutine(StopCamera(() => {

            if (qrScanNewScriptAccess.isGoingBack)
            {
                SceneManager.UnloadSceneAsync("qrCodeScanner");
            }
            else
            {
                //openKitScene = true;
                //Invoke("OpenKit1", 1f);
            }
            

            /* if(qrScanNewScriptAccess.loadKit)
             {
                 SceneManager.LoadSceneAsync("DownloadScene", LoadSceneMode.Additive);
             }*/
        }));
    }
    /// <summary>
    /// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
    /// Trying to stop the camera in OnDestroy provoke random crash on Android
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator StopCamera(Action callback)
    {
        // Stop Scanning
        Image = null;
        BarcodeScanner.Destroy();
        BarcodeScanner = null;

        // Wait a bit
        yield return new WaitForSeconds(0.1f);

        callback.Invoke();
    }
    #endregion

    void OpenKit1()
    {
        //loadingAnimScriptAccess.StopLoadingAnimation();
        SceneManager.UnloadSceneAsync("qrCodeScanner");

        VuforiaRuntime.Instance.InitVuforia();
        Screen.orientation = ScreenOrientation.Landscape;
        KitsHandler.circuitNumber = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint") - 1, 1, 4);

        SceneManager.LoadSceneAsync("StarterKit1_v1", LoadSceneMode.Additive);
    }
}
