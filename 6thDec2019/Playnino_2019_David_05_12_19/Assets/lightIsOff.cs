﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightIsOff : MonoBehaviour {
    public GameObject directlight;
    
    float inten;
	// Use this for initialization
	void Start () {
        inten = GetComponent<Light>().intensity;
	}
	
	// Update is called once per frame
	void Update () {
        if (directlight.GetComponent<Light>().intensity == 0)
        {
            Debug.Log("1");
            GetComponent<Light>().intensity = inten;
        }
        else if(directlight.GetComponent<Light>().intensity==1)
        {
            Debug.Log("2");
            GetComponent<Light>().intensity = 0;
        }
	}
}
