﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class missileExplode : MonoBehaviour
{
    public GameObject[] explosions;
    jet3 jetHandler;
    // Start is called before the first frame update
    void Start()
    {
        jetHandler = FindObjectOfType<jet3>() as jet3;
    }

    // Update is called once per frame
    void Update()
    {
        if (jetHandler.explode)
        {
            if (!explosions[0].activeSelf)
            {
                explosions[0].SetActive(true);
                jetHandler.ExplosionSound.enabled = true;
                Debug.Log("explode");
            }
            if (!explosions[1].activeSelf)
            {
                explosions[1].SetActive(true);
                jetHandler.ExplosionSound.enabled = true;
            }
            //other.gameObject.SetActive(false);
           
            jetHandler.missileTime = 0;
            gameObject.SetActive(false);

        }
    }
   /* private void OnTriggerEnter(Collider other)
    {
       // if((other.gameObject.name=="pole1") ||(other.gameObject.name == "pole2"))
       if(jetHandler.explode)
        {
            if (!explosions[0].activeSelf)
            {
                explosions[0].SetActive(true);
                jetHandler.ExplosionSound.enabled = true;
            }
            if (!explosions[1].activeSelf)
            {
                explosions[1].SetActive(true);
                jetHandler.ExplosionSound.enabled = true;
            }
            //other.gameObject.SetActive(false);
            gameObject.SetActive(false);
            jetHandler.missileTime = 0;

        }
    }
    */
}
