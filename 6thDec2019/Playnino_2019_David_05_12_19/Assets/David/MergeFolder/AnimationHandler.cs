﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    public Animator blueImgAnimator, getStartImgAnimator, launchingSoonAnimator, subscribePopUpAnimator;
    public GameObject welcomeScreen, slideBttn;
    bool introDisplayed, startTimer, isPreBookedAnim;
    float timer;

    

    // Start is called before the first frame update
    void Start()
    {
        welcomeScreen.SetActive(false);
        slideBttn.SetActive(false);
        ShowUserDetail();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (!blueImgAnimator.GetCurrentAnimatorStateInfo(0).IsName("GetStartedBlue") && !introDisplayed)
        {
            //StartCoroutine(DisableIntroPanel());
            ShowUserDetail();
        }*/

        /*if (startTimer)
        {
            timer += Time.deltaTime;
        }
        if(timer >= 6 && timer < 14)
        {
            ReturnGetStartedBttnAnim();
        }
        if(timer >= 14)
        {
            ShowGetStartedBttnAnim();
        }*/
    }

    void ShowUserDetail()
    {
        welcomeScreen.SetActive(true);
        slideBttn.SetActive(true);
        introDisplayed = true;

        /*Invoke("ShowGetStartedBttnAnim", 1.5f);

        startTimer = true;*/
    }

    void ShowGetStartedBttnAnim()
    {
        getStartImgAnimator.Play("GetStartedBttn");
    }
    void ReturnGetStartedBttnAnim()
    {
        getStartImgAnimator.Play("GetStartedBttnReverse");
    }

    public void ShowCommimgSoonAnim()
    {
        launchingSoonAnimator.Play("LaunchingSoonAnimation");
        Invoke("CloseCommimgSoonAnim", 2f);
    }
    void CloseCommimgSoonAnim()
    {
        launchingSoonAnimator.Play("LaunchingSoonAnimationReverse");
    }

    public void SubscribeBttnClick()
    {
        subscribePopUpAnimator.Play("SubscribePanel");
        isPreBookedAnim = false;

        /* SceneManager.LoadScene("ABtesting_NewMarker", LoadSceneMode.Additive);

         mainSceneCanvas.SetActive(false);
         orientationChangeScript.ChangeToLandscape();*/

    }
    public void PreBookBttnClick()
    {
        subscribePopUpAnimator.Play("PreBooked");
        Invoke("ShowPrebookedMsg", 0.30f);
        isPreBookedAnim = true;
    }
    void ShowPrebookedMsg()
    {
        subscribePopUpAnimator.Play("PreBookedMsgShow");
    }
    public void SubscribeBackBttnClick()
    {

        if (isPreBookedAnim)
        {
            subscribePopUpAnimator.Play("SubscribePreBookedClose");
        }
        else
        {
            subscribePopUpAnimator.Play("SubscribePanelReverse");
        }
    }
}
