﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class LocationHandler : MonoBehaviour
{
    bool permissionAsked = false;
    public float latitude=0;
    public float longitude = 0;
    bool gotLocation =false;
    public ProfileHandler pHandle;
    public int locationAccess;
    private void Start()
    {
        //PlayerPrefs.SetFloat("location", 0);
        locationAccess = PlayerPrefs.GetInt("locationStored");

        pHandle = FindObjectOfType<ProfileHandler>() as ProfileHandler;
    }
    
    public void GetLocationBttnClick()
    {
        Debug.Log("Running Functions to get location.....");

        permissionAsked = false;
        //StartCoroutine(GetLocation());
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            permissionAsked = true;
            Permission.RequestUserPermission(Permission.FineLocation);
        }
        else
        {
            StartCoroutine(GetLocation());
        }

    }
    IEnumerator GetLocation()
    {
        Debug.Log("called");
        print("printed");
        // First, check if user has location service enabled
      //  if (!Input.location.isEnabledByUser)
        //{
          //  Debug.Log("loaction not enaled by user");
          //  yield break;

        //}

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
           // PlayerPrefs.SetInt("locationStored", 1);
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
            print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
            latitude = Input.location.lastData.latitude;
            longitude = Input.location.lastData.longitude;
            ///code to store loaction to database goes here
            ///
            
            pHandle.UpdateLocationInDB();
            PlayerPrefs.SetInt("locationStored",1);
            PlayerPrefs.Save();

        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
        Debug.Log("stopped");
    }
    private void Update()
    {
        if (permissionAsked)
        {
            if (Permission.HasUserAuthorizedPermission(Permission.FineLocation))
            {
                Debug.Log("got permission");
                StartCoroutine(GetLocation());
                permissionAsked = false;
            }
        }
    }
}
