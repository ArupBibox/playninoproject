﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Android;
using Vuforia;

public class PopUpHandler : MonoBehaviour
{

    private IEnumerator coroutine;

    public Animator leafletpopUpAnimator, subscribePopUpAnimator, EmailStatusPopUpAnimator, SignOutPopUpAnimator;
    public TMP_InputField emailInputTxt;
    public GameObject mainUI,loadingPanel;
    public bool isSceneBack, isPreBookedAnim, isArSceneOpened;
    public GameObject crossImg, tickImg;
    public TMP_Text emailStatusHeadTxt, emailStatusSubTxt;

    //For Do tween
    public RectTransform leafLetQuesPanel, emailPanel;
    Vector2 centerAnchorPos, rightAnchorPos, leftAnchorPos;
    float duration;

    //For Scene load
    AsyncOperation loadSceneAsync;
    public bool isSceneLoaded, loadArScene;
    public bool sceneAlreadyLoaded;
    bool sceneLoadingInBackground;

    //Script Acces
    OrientationChange orientationChangeScript;
    KitsHandler kitsHandlerScriptAccess;

    public Button arDemoButton,kitOneButton, getStartedBttn;
    // Start is called before the first frame update
    void Start()
    {
        duration = 0.25f;
        centerAnchorPos.Set(0f, 0f);
        leftAnchorPos.Set(-800, 0f);
        rightAnchorPos.Set(800, 0f);
        /*topAnchorPos.Set(0, 1920);
        bottomAnchorPos.Set(0, -1920);*/

        loadingPanel.SetActive(false);

        InitPosition();
        orientationChangeScript = FindObjectOfType<OrientationChange>() as OrientationChange;
        kitsHandlerScriptAccess = FindObjectOfType<KitsHandler>() as KitsHandler;
        
        //emailInputTxt.text = PlayerPrefs.GetString("UserLoggedInEmailId");

        isArSceneOpened = false;
        sceneLoadingInBackground = false;
        sceneAlreadyLoaded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isSceneBack)
        {
            isSceneBack = false;
            mainUI.SetActive(true);
            /*kitOneButton.interactable = true;
            arDemoButton.interactable = true;*/
            getStartedBttn.interactable = true;

            loadingPanel.SetActive(false);
            Screen.orientation = ScreenOrientation.Portrait;

            //LoadArSceneInBackground();
        }
        if (sceneLoadingInBackground)
        {
            if (loadSceneAsync.progress >= 0.9f && !isSceneLoaded)
            {

                loadSceneAsync.allowSceneActivation = false;
                if (loadArScene)
                {
                    loadSceneAsync.allowSceneActivation = true;
                    //mainSceneCanvas.SetActive(false);
                    isSceneLoaded = true;
                    //loadingScene.SetActive(false);

                    Screen.orientation = ScreenOrientation.Landscape;

                }
            }

        }
        
    }

    void InitPosition()
    {
        leafLetQuesPanel.DOAnchorPos(centerAnchorPos, duration);
        emailPanel.DOAnchorPos(rightAnchorPos, duration);
    }

    public void LoadArSceneInBackground()
    {
        sceneLoadingInBackground = true;
       if(!sceneAlreadyLoaded)
        {
            loadSceneAsync = SceneManager.LoadSceneAsync("arDemo", LoadSceneMode.Additive);
            sceneAlreadyLoaded = true;

        }
    }

    public void SubscribeBttnClick()
    {
        subscribePopUpAnimator.Play("SubscribePanel");
        isPreBookedAnim = false;

       /* SceneManager.LoadScene("ABtesting_NewMarker", LoadSceneMode.Additive);

        mainSceneCanvas.SetActive(false);
        orientationChangeScript.ChangeToLandscape();*/

    }
    public void PreBookBttnClick()
    {
        subscribePopUpAnimator.Play("PreBooked");
        Invoke("ShowPrebookedMsg", 0.30f);
        isPreBookedAnim = true;
    }
    void ShowPrebookedMsg()
    {
        subscribePopUpAnimator.Play("PreBookedMsgShow");
    }
    public void SubscribeBackBttnClick()
    {
        
        if (isPreBookedAnim)
        {
            subscribePopUpAnimator.Play("SubscribePreBookedClose");
        }
        else
        {
            subscribePopUpAnimator.Play("SubscribePanelReverse");
        }
    }

    public void DemoBttnClick()
    {

        leafletpopUpAnimator.Play("LeaftLetAnim");

    }


    public void LeafLetBackBttnClick()
    {
        leafletpopUpAnimator.Play("LeaftLetAnimReverse");

        Invoke("InitPosition", 1f);
    }

    public void LeafletYesBttnClick(int fromScene)
    {
        kitsHandlerScriptAccess.bttnClick = 0;

        coroutine = CheckPermission(fromScene);
        StartCoroutine(coroutine);
    }

    IEnumerator CheckPermission(int fromScene)
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera) ||
            !Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
            /*yield return new WaitForSeconds(.3f);
            if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            }*/
            yield return new WaitForSeconds(.3f);
            ChangeScene(fromScene);
        }
        else
        {
            ChangeScene(fromScene);
        }
    }

    public void ChangeScene(int fromScene)
    {
        if (Permission.HasUserAuthorizedPermission(Permission.Camera) /*&&
            Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)*/)
        {
            VuforiaRuntime.Instance.InitVuforia();
            LoadArSceneInBackground();

            mainUI.SetActive(false);
            loadingPanel.SetActive(true);
            
            isArSceneOpened = true;
            Invoke("OpenArScene", 1.3f);
            //OpenArScene();

            /*if (fromScene == 1)
            {
                arDemoButton.interactable = false;
                SceneManager.LoadScene("ObbExtractorScene", LoadSceneMode.Additive);
            }
            else
            {
                SceneManager.UnloadSceneAsync("ObbExtractorScene");
                arDemoButton.interactable = false;
                
                //LoadScene();

                VuforiaRuntime.Instance.InitVuforia();
                LoadArSceneInBackground();

                mainUI.SetActive(false);
                loadingPanel.SetActive(true);


                isArSceneOpened = true;
                //Invoke("OpenArScene", 1.3f);
                OpenArScene();
                
            }*/
        }
        else
        {
            getStartedBttn.interactable = true;
            //arDemoButton.interactable = true;
        }
            
    }
    /*void LoadScene()
    {
        if (Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            VuforiaRuntime.Instance.InitVuforia();
            LoadArSceneInBackground();

            mainUI.SetActive(false);
            loadingPanel.SetActive(true);

            
            isArSceneOpened = true;
            //Invoke("OpenArScene", 1.3f);
            OpenArScene();

        }
        else
        {
            arDemoButton.interactable = true;

        }
    }*/
    public void LeafletNoBttnClick()
    {
        leafLetQuesPanel.DOAnchorPos(leftAnchorPos, duration);
        emailPanel.DOAnchorPos(centerAnchorPos, duration);
        
    }
    public void EmailBackBttnClick()
    {
        emailPanel.DOAnchorPos(rightAnchorPos, duration);
        leafLetQuesPanel.DOAnchorPos(centerAnchorPos, duration);
    }
    /*public void EmailSubmitBttnClick()
    {
        //LeafLetBackBttnClick();
        EmailBackBttnClick();

    }*/
    public void EmailSentPopUp()
    {
        crossImg.SetActive(false);
        tickImg.SetActive(true);

        emailStatusHeadTxt.text = "SUCCESSFULLY SENT !";
        emailStatusHeadTxt.color = new Color32(91, 173, 47, 255);

        emailStatusSubTxt.text = "Download and scan the leaflet to experience the AR magic";
        emailStatusSubTxt.color = new Color32(110, 197, 64, 255);
        emailStatusSubTxt.fontSize = 20;

        EmailStatusPopUpAnimator.Play("EmailStatusPanel");
    }
    public void EmailNotSentPopUp()
    {
        tickImg.SetActive(false);
        crossImg.SetActive(true);

        emailStatusHeadTxt.text = "E-MAIL NOT SENT !";
        emailStatusHeadTxt.color = new Color32(219, 20, 27, 255);

        emailStatusSubTxt.text = "Please Try Again";
        emailStatusSubTxt.color = new Color32(236, 32, 40, 255);
        emailStatusSubTxt.fontSize = 25;

        EmailStatusPopUpAnimator.Play("EmailStatusPanel");
    }
    public void EmailStatusPopUpBackBttnClick()
    {
        EmailStatusPopUpAnimator.Play("EmailStatusPanelReverse");
    }

    public void SignOutBttnClick()
    {
        SignOutPopUpAnimator.Play("SignOutPopUpAnim");
    }
    public void SignOutYesBttnClick()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
    }
    public void SignOutBackBttnClick()
    {
        SignOutPopUpAnimator.Play("SignOutPopUpAnimReverse");
    }

    void OpenArScene()
    {

        loadArScene = true;
    }
}
