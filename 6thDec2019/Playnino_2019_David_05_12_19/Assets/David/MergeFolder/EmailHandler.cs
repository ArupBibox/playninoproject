﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class EmailHandler : MonoBehaviour
{
    public TMP_InputField userEmail;

    public Button leafletSubmitBttn;
    bool isEmailValid;

    //For Toast
    /* public Image toastImg;
     public Text toastTxt;*/

    private string loggedInUserNumber, loggedInUserName;

    //For script access
    ToastMessage toastAccess;
    PopUpHandler popUpScriptAccess;
    ZohoCRM zohoCrmScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        toastAccess = GameObject.FindObjectOfType<ToastMessage>() as ToastMessage;
        popUpScriptAccess = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        zohoCrmScriptAccess = FindObjectOfType<ZohoCRM>() as ZohoCRM;

        loggedInUserNumber = PlayerPrefs.GetString("UserLoggedInNumber");
        loggedInUserName = PlayerPrefs.GetString("UserLoggedInName");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool ValidateEmail(string address)
    {
        //var atCharacter:String[];
        //var dotCharacter:String[];

        string[] atCharacter;
        string[] dotCharacter;
        // this splits the input address by the @ symbol. If it finds the @ symbol, it throws it away,
        // and puts what comes before the @ symbol into atCharacter[0], and what comes after the @ symbol
        // into atCharacter[1]
        atCharacter = address.Split("@"[0]);

        //now we check that the returned array is exactly 2 members long, that means there was only 1 @ symbol
        if (atCharacter.Length == 2)
        {
            //here we split the second member returned above by the dot character, and shove the returned info
            // into the dotCharacter array.
            dotCharacter = atCharacter[1].Split("."[0]);
            // now we check the length of the dotCharacter array. If it is greater than or equal to 2, we know
            // we have at least one dot, maybe more than one.
            if (dotCharacter.Length >= 2)
            {
                //Character before @ check
                if (atCharacter[0].Length >= 1)
                {
                    //Character before dot check
                    if (dotCharacter[0].Length >= 1)
                    {
                        // this last check makes sure that there is actual data after the last dot.
                        if (dotCharacter[dotCharacter.Length - 1].Length <= 1)
                        {
                            // fail
                            Debug.Log("Not Valid char after dot");
                            isEmailValid = false;
                            return false;
                        }
                        else
                        {
                            // if we get to here, you have a valid email address format
                            Debug.Log("valid");
                            isEmailValid = true;
                            return true;
                        }
                    }
                    else
                    {
                        Debug.Log("No char before dot");
                        isEmailValid = false;
                        return false;
                    }

                }
                else
                {
                    Debug.Log("No char before @");
                    isEmailValid = false;
                    return false;
                }

            }
            else
            {
                // fail
                Debug.Log("No dot");
                isEmailValid = false;
                return false;
            }
        }
        else
        {
            // fail
            Debug.Log("no @");
            isEmailValid = false;
            return false;
        }

    }

    public void SendLeafLetBttnClick()
    {
        ValidateEmail(userEmail.text);
        leafletSubmitBttn.interactable = false;

        string secondaryEmailValue = "'Secondary_Email':'" + userEmail.text + "'";
        string leadAppStageValue = "'App_Stage':'Sample Request'";

        if (userEmail.text.Length != 0)
        {
            //StartCoroutine(GetLeaftLetRequest("https://tpluschallenge.com/xyz.php?q=" + userEmail.text));

            if (isEmailValid)
            {
                popUpScriptAccess.LeafLetBackBttnClick();
                zohoCrmScriptAccess.UpdateLeadInCRM(secondaryEmailValue + "," + leadAppStageValue);
                leafletSubmitBttn.interactable = true;
                if (zohoCrmScriptAccess.isLeadUpdated)
                {
                    popUpScriptAccess.EmailSentPopUp();
                    popUpScriptAccess.EmailBackBttnClick();
                    zohoCrmScriptAccess.isLeadUpdated = false;
                }
                else
                {
                    popUpScriptAccess.EmailNotSentPopUp();
                    zohoCrmScriptAccess.isLeadUpdated = false;
                }
            }
            else
            {
                toastAccess.showToast("Invalid E-mail Address", 1);
                leafletSubmitBttn.interactable = true;
            }
        }
        else
        {
            toastAccess.showToast("Enter Your E-mail id", 1);
            leafletSubmitBttn.interactable = true;
        }
        
    }
    IEnumerator GetLeaftLetRequest(string uri)
    {
        UnityWebRequest uwr = UnityWebRequest.Get(uri);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            popUpScriptAccess.LeafLetBackBttnClick();
            Debug.Log("Error While Sending: " + uwr.error);
            toastAccess.showToast("Network Error", 2);
            leafletSubmitBttn.interactable = true;
        }
        else
        {
            popUpScriptAccess.LeafLetBackBttnClick();

            Debug.Log("Received: " + uwr.downloadHandler.text);
            toastAccess.showToast("Email Sent", 4);
            leafletSubmitBttn.interactable = true;
        }
    }

    public void ImIntrestedBttnClick()
    {
        //StartCoroutine(IntrestedRequest("https://tpluschallenge.com/support.php?n=" + loggedInUserNumber + "&m=" + loggedInUserName));
        //StartCoroutine(IntrestedRequest("https://tpluschallenge.com/support.php?n=" + "8870395228" + "&m=" + "Hai"));

        string leadAppStageValue = "'App_Stage':'Interested'";

        zohoCrmScriptAccess.UpdateLeadInCRM(leadAppStageValue);
    }
    IEnumerator IntrestedRequest(string uri)
    {
        UnityWebRequest uwr = UnityWebRequest.Get(uri);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
            toastAccess.showToast("Network Error", 2);
        }
        else
        {
            Debug.Log("Received: " + uwr.downloadHandler.text);
            toastAccess.showToast("Playnino team will get in touch with you soon", 2);
        }
    }
    public void PreBookBttnClick()
    {
        //StartCoroutine(PreBookRequest("https://tpluschallenge.com/prebookMail.php?q=" + loggedInUserNumber + "&m=" + loggedInUserName));

        string leadAppStageValue = "'App_Stage':'Pre-book'";

        zohoCrmScriptAccess.UpdateLeadInCRM(leadAppStageValue);
    }
    IEnumerator PreBookRequest(string uri)
    {
        UnityWebRequest uwr = UnityWebRequest.Get(uri);
        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError)
        {
            Debug.Log("Error While Sending: " + uwr.error);
            toastAccess.showToast("Network Error", 2);
        }
        else
        {
            Debug.Log("Received: " + uwr.downloadHandler.text);
        }
    }

   /* #region ToastMessage

    void showToast(string text, int duration)
    {
        StartCoroutine(showToastCOR(text, duration));
    }

    private IEnumerator showToastCOR(string text, int duration)
    {
        Color orginalImgColor = toastImg.color;
        Color orginalTxtColor = toastTxt.color;

        toastImg.enabled = true;

        toastTxt.text = text;
        toastTxt.enabled = true;

        //Fade in
        yield return fadeInAndOut1(toastImg, toastTxt, true, 0.5f);

        //Wait for the duration
        float counter = 0;
        while (counter < duration)
        {
            counter += Time.deltaTime;
            yield return null;
        }

        //Fade out
        yield return fadeInAndOut1(toastImg, toastTxt, false, 0.5f);

        toastImg.enabled = false;
        toastImg.color = orginalImgColor;

        toastTxt.enabled = false;
        toastTxt.color = orginalTxtColor;
    }

    IEnumerator fadeInAndOut1(Image targetImg, Text targetTxt, bool fadeIn, float duration)
    {
        //Set Values depending on if fadeIn or fadeOut
        float a, b;
        if (fadeIn)
        {
            a = 0f;
            b = 1f;
        }
        else
        {
            a = 1f;
            b = 0f;
        }

        Color currentImgColor = toastImg.color;
        Color currentTxtColor = Color.clear;
        float counter = 0f;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(a, b, counter / duration);

            targetImg.color = new Color(currentImgColor.r, currentImgColor.g, currentImgColor.b, alpha);
            targetTxt.color = new Color(currentTxtColor.r, currentTxtColor.g, currentTxtColor.b, alpha);
            yield return null;
        }
    }

    #endregion*/

}
