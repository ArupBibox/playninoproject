﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationChange : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ChangeToPortrait();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void ChangeToPortrait()
    {
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.orientation = ScreenOrientation.Portrait;
        

    }
    public void ChangeToLandscape()
    {
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        Screen.orientation = ScreenOrientation.Landscape;
    }
}
