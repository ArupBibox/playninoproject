﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class HandleAboutUs : MonoBehaviour {
    readonly string phno1 = "+919739012640";
    //private string phno2 = "08025743535";
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void gotoWebsite()
    {
        Application.OpenURL("http://www.playnino.com/");
    }

    public void sendEmail()
    {
        string email = "info@playnino.com";
        string subject = MyEscapeURL("Product Inquiry");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void callThisNumber()
    {
        /*string name = EventSystem.current.currentSelectedGameObject.transform.parent.name;
        string phoneno;
        Debug.Log("CLicked element is: " + name);
        if(name== "PhoneNo1")
        {
            phoneno = phno1;
        }
        else
        {
            phoneno = phno2;
        }*/
        Application.OpenURL("tel://"+ phno1);
    }

    public void openGoogleMap()
    {
        Application.OpenURL("http://maps.google.com/maps?q=Evobi+Automations+pvt+ltd");
    }
    public void infographicButton()
    {
        //PlayerPrefs.DeleteKey("installed");
        SceneManager.LoadScene("infograDummy");
    }

    public void openAppStorePage()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.evobi.playnino");
    }
}
