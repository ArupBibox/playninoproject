﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;


public class HamburgerHandler : MonoBehaviour
{
    string appName, appLink;
    readonly string phoneNum = "08025743579";

    // Start is called before the first frame update
    void Start()
    {
        appName = "Play Nino";
        appLink = "https://play.google.com/store/apps/details?id=com.evobi.playnino&hl=en";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShareAppBttnClick()
    {
        //execute the below lines if being run on a Android device
#if UNITY_ANDROID
        //Refernece of AndroidJavaClass class for intent
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        //Refernece of AndroidJavaObject class for intent
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
        //call setAction method of the Intent object created
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        //set the type of sharing that is happening
        intentObject.Call<AndroidJavaObject>("setType", "text/plain");

        //add data to be passed to the other activity i.e., the data to be sent
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), appName);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), appLink);

        //get the current activity
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
        //start the activity by sending the intent data
        currentActivity.Call("startActivity", intentObject);
#endif
    }

    public void CallUsBttnClick()
    {
        /*string name = EventSystem.current.currentSelectedGameObject.transform.parent.name;
        string phoneno;
        Debug.Log("CLicked element is: " + name);
        if (name == "PhoneNo1")
        {
            phoneno = phno1;
        }
        else
        {
            phoneno = phno2;
        }*/
        Application.OpenURL("tel://" + phoneNum);
    }
   
    /*public void SignOutBttnClick()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0);
    }*/
}
