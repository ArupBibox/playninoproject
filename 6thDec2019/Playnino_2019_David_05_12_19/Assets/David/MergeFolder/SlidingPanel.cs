﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SlidingPanel : MonoBehaviour
{
    //For DoTween
    public RectTransform introPanel, homePanel, aboutUspanel, hamburgerPanel, rewardsPanel;
    //Vector2 centerAnchorPos, leftAnchorPos, rightAnchorPos;
    Vector2 centerAnchorPos, rightAnchorPos, leftAnchorPos, topAnchorPos, bottomAnchorPos;
    float duration;

    public GameObject hamburgerBttn, blueImg;
    public float speed;
    public bool moveBlueImg, isReachedExp, isKit1Reached, hamOpened;

    //For exit app
    public GameObject mainUI;
    public int exitCounter, beforePreviousScreen, previousScreen, currntScreen;
    bool startTimer, msgToasted, inOtpScreen, isBackBttnClicked,isInMainScene;
    float timeLeft;
    //Script Access
    private ToastMessage toastAccess;
    private ProfileHandler profileAccess;
    public LocationHandler locationHandle;
    PopUpHandler popUpSricptAccess;
    VersionUpdateNotification versionUpdateNotificationScript;
    public ScrollRect scrollRectComponent;
    FillBlueLine fillBlueLineScript;

    public Button getStartedBttn;

    public bool project1Reached, project2Reached, project3Reached, kit2Reached;
    private void Awake()
    {
        /*//Panel Anchor Pos
        duration = 0.25f;

        centerAnchorPos.Set(0f, 0f);
        leftAnchorPos.Set(-802f, 0f);
        rightAnchorPos.Set(802f, 0f);
        /*topAnchorPos.Set(0, 1280f);
        bottomAnchorPos.Set(0, -1280f);*/

        
        
    }
    // Start is called before the first frame update
    void Start()
    {
        isInMainScene = true;
        duration = 0.25f;
        centerAnchorPos.Set(0f, 0f);
        leftAnchorPos.Set(-1080, 0f);
        rightAnchorPos.Set(1080, 0f);
        topAnchorPos.Set(0, 1920);
        bottomAnchorPos.Set(0, -1920);

        PanelArrangeOrder();

        speed = 15f;

        toastAccess = FindObjectOfType<ToastMessage>() as ToastMessage;
        profileAccess = FindObjectOfType<ProfileHandler>() as ProfileHandler;
        locationHandle = FindObjectOfType<LocationHandler>() as LocationHandler;
        popUpSricptAccess = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        fillBlueLineScript = FindObjectOfType<FillBlueLine>() as FillBlueLine;

        versionUpdateNotificationScript = FindObjectOfType<VersionUpdateNotification>() as VersionUpdateNotification;

    }

    // Update is called once per frame
    void Update()
    {
        if (moveBlueImg)
        {
            MoveBlueImg();
        }

        if(mainUI.activeSelf)
        {
            isInMainScene = true;
        }
        else
        {
            isInMainScene = false;
        }
        //For Exit App
        if (isInMainScene)//!popUpSricptAccess.isArSceneOpened)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                isBackBttnClicked = true;
                //beforePreviousScreen = previousScreen;
                if (beforePreviousScreen == previousScreen && previousScreen == currntScreen && beforePreviousScreen == currntScreen && currntScreen != 0)
                {
                    beforePreviousScreen = 0;
                    previousScreen = 0;
                    currntScreen = 0;
                    PanelArrangeOrder();
                }
                else if (currntScreen == 0)
                {
                    exitCounter++;
                    startTimer = true;
                }
                else if (currntScreen == 1)
                {
                    PanelArrangeOrder();
                }
                else if(currntScreen == 2)
                {
                    if(previousScreen == 0)
                    {
                        PanelArrangeOrder();
                    }
                    else if(previousScreen == 1)
                    {
                        SlideToHomePanel();
                    }
                    else if(previousScreen == 3)
                    {
                        AboutUsPanelClick();
                    }

                    previousScreen = beforePreviousScreen;
                }
                else if (currntScreen == 3)
                {
                    if (previousScreen == 0)
                    {
                        PanelArrangeOrder();
                    }
                    else if(previousScreen == 1)
                    {
                        SlideToHomePanel();
                    }
                    else if(previousScreen == 2)
                    {
                        RewardsBttnClick();
                    }

                    previousScreen = beforePreviousScreen;
                }
                if (hamOpened)
                {
                    HamburgerBackBttnClick();
                }
                
            }
        }
       

        if (startTimer)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft <= 0)
            {
                startTimer = false;
                exitCounter = 0;
                timeLeft = 3f;
                msgToasted = false;
            }
        }
        if (exitCounter == 1 && !msgToasted)
        {
            toastAccess.showToast("Press again to exit", 1);
            msgToasted = true;
        }
        if (exitCounter == 2)
        {
            startTimer = false;
            exitCounter = 0;
            timeLeft = 3f;
            msgToasted = false;
            Application.Quit();
            Debug.Log("app closed");
        }
    }
    void PanelArrangeOrder()
    {
        introPanel.DOAnchorPos(centerAnchorPos, duration);
        homePanel.DOAnchorPos(rightAnchorPos, duration);
        hamburgerPanel.DOAnchorPos(leftAnchorPos, duration);
        rewardsPanel.DOAnchorPos(rightAnchorPos, duration);
        aboutUspanel.DOAnchorPos(rightAnchorPos, duration);

        /*scorePanel.DOAnchorPos(leftAnchorPos, duration);
        profilePanel.DOAnchorPos(rightAnchorPos, duration);*/

        currntScreen = 0;
    }
    public void GetStartedBttnClick()
    {
        getStartedBttn.interactable = false;
        popUpSricptAccess.LeafletYesBttnClick(1);

        /*if (locationHandle.locationAccess == 0)
        {
            if(PlayerPrefs.GetString("LoggingIn") != "true")//signin
                    locationHandle.GetLocationBttnClick();
            Invoke("MoveToScrollPanel", 1f);
        }
        else
        {
            MoveToScrollPanel();
        }
        scrollRectComponent.verticalNormalizedPosition = 0;*/
    }
    void MoveToScrollPanel()
    {
        versionUpdateNotificationScript.CheckForceUpdate();
        //popUpSricptAccess.LoadArSceneInBackground();///it loads the scene

        introPanel.DOAnchorPos(leftAnchorPos, duration);
        homePanel.DOAnchorPos(centerAnchorPos, duration);

        /*if (!moveBlueImg && !isReachedExp)
        {
            moveBlueImg = true;
        }*/
        /* if(!moveBlueImg)
         {
             int currentPoint = PlayerPrefs.GetInt("currentUnlockedPoint");
             if (currentPoint == 0 && !isReachedExp)
                 moveBlueImg = true;
             else if (currentPoint == 1 && !isKit1Reached)
                 moveBlueImg = true;
             else if (currentPoint == 2 && !project1Reached)
                 moveBlueImg = true;
             else if (currentPoint == 3 && !project2Reached)
                 moveBlueImg = true;
             else if (currentPoint == 4 && !project3Reached)
                 moveBlueImg = true;
             else if (currentPoint == 5 && !kit2Reached)
                 moveBlueImg = true;
         }*/
        fillBlueLineScript.grow = true;
        int currentPoint = Mathf.Clamp(PlayerPrefs.GetInt("currentUnlockedPoint"), 0, fillBlueLineScript.checkPoints.Length-1);
        Debug.Log(currentPoint);
        Debug.Log(PlayerPrefs.GetInt("currentUnlockedPoint"));
        if(currentPoint!=0)
            fillBlueLineScript.canceled = false;

        beforePreviousScreen = previousScreen;//0
        previousScreen = currntScreen;//0
        currntScreen = 1;
    }
    void SlideToHomePanel()
    {
        introPanel.DOAnchorPos(leftAnchorPos, duration);
        rewardsPanel.DOAnchorPos(rightAnchorPos, duration);
        aboutUspanel.DOAnchorPos(rightAnchorPos, duration);
        homePanel.DOAnchorPos(centerAnchorPos, duration);
        HamburgerBackBttnClick();

        beforePreviousScreen = previousScreen;//0
        previousScreen = currntScreen;//0
        currntScreen = 1;
    }

    public void RewardsBttnClick()
    {

        introPanel.DOAnchorPos(leftAnchorPos, duration);
        homePanel.DOAnchorPos(leftAnchorPos, duration);
        //hamburgerPanel.DOAnchorPos(leftAnchorPos, duration);
        aboutUspanel.DOAnchorPos(rightAnchorPos, duration);
        rewardsPanel.DOAnchorPos(centerAnchorPos, duration);
        /*if (previousScreen == 3)
        {
            aboutUspanel.DOAnchorPos(leftAnchorPos, duration);


        }*/
        HamburgerBackBttnClick();
        if (!isBackBttnClicked)
        {
            beforePreviousScreen = previousScreen;
        }
        
        previousScreen = currntScreen;//1
        currntScreen = 2;
        isBackBttnClicked = false;
    }
    public void AboutUsPanelClick()
    {
        introPanel.DOAnchorPos(leftAnchorPos, duration);
        homePanel.DOAnchorPos(leftAnchorPos, duration);
        rewardsPanel.DOAnchorPos(rightAnchorPos, duration);
        aboutUspanel.DOAnchorPos(centerAnchorPos, duration);
        /*if (previousScreen == 2)
        {
            rewardsPanel.DOAnchorPos(leftAnchorPos, duration);
        }*/
        HamburgerBackBttnClick();

        if (!isBackBttnClicked)
        {
            beforePreviousScreen = previousScreen;
        }
        previousScreen = currntScreen;
        
        currntScreen = 3;

        isBackBttnClicked = false;
    }
    public void HamburgerBttnClick()
    {
        hamburgerPanel.DOAnchorPos(centerAnchorPos, duration);
        hamburgerBttn.SetActive(false);
        hamOpened = true;
        
        //previousScreen = currntScreen;
        //currntScreen = 4;

        //profileAccess.UpdateArScoreInDB();
    }
    public void HamburgerBackBttnClick()
    {
        hamburgerPanel.DOAnchorPos(leftAnchorPos, duration);
        hamburgerBttn.SetActive(true);
        hamOpened = false;

        
        /*currntScreen = previousScreen;
        previousScreen = beforePreviousScreen;*/
    }
    
    void MoveBlueImg()
    {
        blueImg.transform.Translate(0,speed,0);
    }
}
