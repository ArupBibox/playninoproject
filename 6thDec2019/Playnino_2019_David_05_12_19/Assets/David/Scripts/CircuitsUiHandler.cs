﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Vuforia;

public class CircuitsUiHandler : MonoBehaviour
{
    public Button nxtBttn;
    public TMP_Text projNumTxt;
    public UnityEngine.UI.Image projNumImg;

    public GameObject scanAnimObject;

    public int curProjNum;
    //public bool isProjNumSetted;
    //public bool isNxtBttnShown;

    //ScriptAccess
    TextFileRead textFileReadScriptAccess;
    UIManager uiManagerScriptAccess;
    CircuitsTargetController circuitsTargetControllerAccess;
    DefaultTrackableEventHandler defaultTrackableEventHandlerAccess;

    private void Start()
    {
        textFileReadScriptAccess = FindObjectOfType<TextFileRead>();
        uiManagerScriptAccess = FindObjectOfType<UIManager>();
        circuitsTargetControllerAccess = FindObjectOfType<CircuitsTargetController>();
        defaultTrackableEventHandlerAccess = FindObjectOfType<DefaultTrackableEventHandler>();

        ProjectNameOn_Off(false);
        ShowNextBttn(false);

       // ScanCircle_On_Off(true);
    }

    private void Update()
    {

        //Debug.Log(scanAnimObject.activeSelf);
        /*if (!isProjNumSetted)
        {
            curProjNum = circuitsTargetControllerAccess.projectNumber;

            if(curProjNum == 0)
            {
                projNumImg.gameObject.SetActive(false);
                ShowNextBttn(false);
            }
            else
            {
                projNumImg.gameObject.SetActive(true);
                projNumTxt.text = "PROJECT " + curProjNum.ToString();
            }
            isProjNumSetted = true;
        }*/
    }

    public void ShowNextBttn(bool val)
    {
        //if (!isNxtBttnShown)
        //{
            /*for (int i = 0; i < navBttnsArry.Length; i++)
            {
                navBttnsArry[i].gameObject.SetActive(val);
            }*/
            nxtBttn.gameObject.SetActive(val);
           // isNxtBttnShown = val;
        //}
    }

    public void ProjectNameOn_Off(bool val)
    {
        projNumTxt.text = "PROJECT " + curProjNum.ToString();
        projNumImg.gameObject.SetActive(val);
    }

    /*void projNavBttnsOn_Off(bool val)
    {
        for (int i = 0; i < navBttnsArry.Length; i++)
        {
            navBttnsArry[i].gameObject.SetActive(val);
        }
    }*/

    #region Button Click Response

    public void ReloadBttnClick()
    {
        Debug.Log("ReloadBttnClick");
        ScanCircle_On_Off(false);
        //uiManagerScriptAccess.ScanCircle.SetActive(false);
        TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
        circuitsTargetControllerAccess.CircuitsOn_Off(0, false);
        ShowNextBttn(false);
        ProjectNameOn_Off(false);
        
        DefaultTrackableEventHandler.TrackImageName = null;
        //defaultTrackableEventHandlerAccess.LossVuforiaTrack();

        textFileReadScriptAccess.CloseQuestionPanel();

        uiManagerScriptAccess.playAgain();

        Restart();
    }
    void Restart()
    {
        //uiManagerScriptAccess.ScanCircle.SetActive(true);
        TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
        circuitsTargetControllerAccess.isTargetDetected = false;

    }

    public void NextBttnClick()
    {
        if(curProjNum != 0)
        {
            textFileReadScriptAccess.ShowQuestion(curProjNum - 1);
            ShowNextBttn(false);
        }
            
    }
    /*public void PreviousBttnClick()
    {
        if (curProjNum > 0)
        {
            (curProjNum == 1)
            {
                navBttnsArry[0].gameObject.SetActive(false);
            }
            //nxtBttn.gameObject.SetActive(false);
            curProjNum--;
        }
    }*/

    public void ScanCircle_On_Off(bool val)
    {
        scanAnimObject.SetActive(val);
    }
    public void CloseBttnClick()
    {
        textFileReadScriptAccess.CloseQuestionPanel();
        ReloadBttnClick();
    }

    #endregion
}
