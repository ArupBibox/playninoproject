﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountryCode : MonoBehaviour
{
    public string[] countryNames = { "IN", "AU", "BH", "BD", "BT", "CA", "CN", "DE", "HK", "ID", "JP", "KW", "MY", "NP", "PH", "SG", "LK", "TH", "AE", "GB", "US", "VN", "TW", "MM" };
    public string[] countryTelCodes = { "+91", "+61", "+973", "+880", "+975", "+1", "+86", "+49", "+852", "+62", "+81", "+965", "+60", "+977", "+63", "+65", "+94", "+66", "+971", "+44", "+1", "+84", "+886", "+95" };
    public string userCountryName, userCountryTelCode;
    public bool isCountryCodeSetted;
    public TMP_Dropdown countryCodeDD1, countryCodeDD2;
    public List<string> countryTelCodesList;

    // Start is called before the first frame update
    void Start()
    {
        CreateList();
    }
    
    void CreateList()
    {
        countryTelCodesList = new List<string>();
        for (int i = 0; i < countryTelCodes.Length; i++)
        {
            countryTelCodesList.Add(countryTelCodes[i]);
        }
        countryCodeDD1.AddOptions(countryTelCodesList);
        countryCodeDD2.AddOptions(countryTelCodesList);

        FetchUserCountry();
    }

    void FetchUserCountry()
    {
#if UNITY_ANDROID
        using (AndroidJavaClass cls = new AndroidJavaClass("java.util.Locale"))
        {
            if (cls != null)
            {
                using (AndroidJavaObject locale = cls.CallStatic<AndroidJavaObject>("getDefault"))
                {
                    if (locale != null)
                    {
                        //var localeVal = locale.Call<string>("getLanguage") + "_" + locale.Call<string>("getCountry") + "-" + locale.Call<string>("getISO3Country");
                        var localeVal = locale.Call<string>("getCountry");
                        Debug.Log("Android lang: " + localeVal);
                        userCountryName = localeVal.ToString();

                        SetCountryCode();
                    }
                    else
                    {
                        Debug.Log("locale null");
                    }
                }
            }
            else
            {
                Debug.Log("cls null");
            }
        }
#endif
    }

    void SetCountryCode()
    {
        for (int i = 0; i < countryNames.Length; i++)
        {
            if (countryNames[i] == userCountryName)
            {
                userCountryTelCode = countryTelCodes[i];

                countryCodeDD1.value = i;
                countryCodeDD2.value = i;
            }
        }
    }
}
