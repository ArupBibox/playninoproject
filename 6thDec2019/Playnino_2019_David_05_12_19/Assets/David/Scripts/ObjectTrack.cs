﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTrack : MonoBehaviour
{
    public enum ObjectType
    {
        NAME,
        NUMBER,
        EMAIL
    }
    
    public ObjectType _ObjectType;
}
