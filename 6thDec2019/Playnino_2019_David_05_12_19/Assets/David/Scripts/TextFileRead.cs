﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

public class TextFileRead : MonoBehaviour
{
   public List<QuestionsOptList> quesOptList = new List<QuestionsOptList>();
    

    public TextAsset kit1QuesTxtfile, kit1MultiOptTxtFile;
    string kit1QuesLongString, kit1AnsLongString, kit1MultiOptLongString;
    List<string> kit1QuesEeachLine, kit1AnsEeachLine, kit1MultiOptEeachLine;

    //string[] ques1OptArr, ques2OptArr, ques3OptArr;
    //int ques1AnsInt, ques2AnsInt, ques3AnsInt;

    public Text questionTxt, resultTxt;
    public Button[] optsBttns = new Button[4];

    int currentQuesNum, currentQuesCrtAns;
    bool isAnsCrt;

    public GameObject quesPanel;

    Kit1StatusHandler kit1StatusHandlerScriptAccess;
    CircuitsUiHandler circuitsUiHandlerScriptAccess;
    
    // Start is called before the first frame update
    void Start()
    {
        kit1StatusHandlerScriptAccess = FindObjectOfType<Kit1StatusHandler>();
        circuitsUiHandlerScriptAccess = FindObjectOfType<CircuitsUiHandler>();

        //quesList[0].quesOpts = "hii";

        quesPanel.SetActive(false);
        //resultTxt.text = "";

        ReadDataFromTxt();
    }

    public void ReadDataFromTxt()
    {
        kit1QuesLongString = kit1QuesTxtfile.text;
        kit1QuesEeachLine = new List<string>();
        kit1QuesEeachLine.AddRange(kit1QuesLongString.Split("\n"[0]));

        /*kit1AnsLongString = kit1AnsTxtfile.text;
        kit1AnsEeachLine = new List<string>();
        kit1AnsEeachLine.AddRange(kit1AnsLongString.Split("\n"[0]));*/

        kit1MultiOptLongString = kit1MultiOptTxtFile.text;
        kit1MultiOptEeachLine = new List<string>();
        kit1MultiOptEeachLine.AddRange(kit1MultiOptLongString.Split("\n"[0]));
        

        for (int i = 0; i < kit1MultiOptEeachLine.Count; i++)
        {
            quesOptList.Add(new QuestionsOptList());// Assign total question size
        }

        for (int mcq = 0; mcq < kit1MultiOptEeachLine.Count; mcq++)
        {
            string[] sList = kit1MultiOptEeachLine[mcq].Split(',');// Splitting string with "," and assign it into an array
            int stringLength = sList.Length;// Calculate Splitted string length

            //Debug.Log(sList[stringLength - 1]);
            int ansVal = Convert.ToInt32(sList[stringLength-1]);// taking the last index(Crt answer) and passing into int
            //Debug.Break();
            quesOptList[mcq].questionAnswerVal = ansVal;

            for (int j = 0; j < stringLength-1 ; j++)
            {
                quesOptList[mcq].quesOptsArr.Add(sList[j]);// Push splitted string into list with proper index
            }
        }
            //quesOptList[i].quesOptsArr[i] = kit1MultiOptEeachLine[i].Split(',').ToString();
            /*quesList.Add(new Questions());
            quesList[i].questionsOptList.quesOpts1=
            */
            /*switch (i)
            {
                case 0:
                    
                    ques1OptArr = kit1MultiOptEeachLine[i].Split(',');
                    break;
                case 1:
                    ques2OptArr = kit1MultiOptEeachLine[i].Split(',');
                    break;
                case 2:
                    ques3OptArr = kit1MultiOptEeachLine[i].Split(',');
                    break;
            }*/
      //  }




        //currentQuesAns = Convert.ToInt32(ques1OptArr[ques1OptArr.Length - 1].Trim());


       /* //Regex replace multiple white spaces to single space and trim removes front and end white spaces.
        questionTxt.text = Regex.Replace(kit1QuesEeachLine[0], @"\s+", " ").Trim();
        currentQuesNum = 0;

        PushAnswers(currentQuesNum);*/
    }

    public void ShowQuestion(int projNum)
    {
        quesPanel.SetActive(true);

        //Regex replace multiple white spaces to single space and trim removes front and end white spaces.
        questionTxt.text = Regex.Replace(kit1QuesEeachLine[projNum], @"\s+", " ").Trim();
        currentQuesNum = projNum;

        PushAnswers(currentQuesNum);
    }


    void OptionButtonAllOn()
    {
        for (int i = 0; i < optsBttns.Length; i++)
        {
            optsBttns[i].gameObject.SetActive(true);
        }
    }

    void OptionButtonOff(int nButton)
    {
        OptionButtonAllOn();// First Turn on every button;
        for (int i = nButton; i < optsBttns.Length; i++)
        {
            optsBttns[i].gameObject.SetActive(false);
        }
    }
        
    void PushAnswers(int Val)
    {
        int quesNum = Val;
        int currentQuesOptLength = quesOptList[quesNum].quesOptsArr.Count;

        currentQuesCrtAns = quesOptList[quesNum].questionAnswerVal;

        switch (currentQuesOptLength)
        {
            case 2:
                OptionButtonOff(currentQuesOptLength);
                break;
            case 3:
                OptionButtonOff(currentQuesOptLength);
                break;
            case 4:
                OptionButtonAllOn();
                break;
                
        }
        for (int i = 0; i < currentQuesOptLength; i++)
        {
            optsBttns[i].GetComponentInChildren<Text>().text = quesOptList[quesNum].quesOptsArr[i].Trim();
        }


       // currentQuesCrtAns = Convert.ToInt32(quesOptList[quesNum].quesOptsArr[currentQuesOptLength].Trim());

        /*switch (Val)
        {
            case 0:
                for (int i = 0; i < ques1OptArr.Length; i++)
                {
                    ques1OptArr[i] = ques1OptArr[i].Trim();
                    if (i < ques1OptArr.Length - 1)
                        optsBttns[i].GetComponentInChildren<Text>().text = ques1OptArr[i];
                }
                currentQuesCrtAns = Convert.ToInt32(ques1OptArr[ques1OptArr.Length - 1].Trim());
                break;
            case 1:
                for (int i = 0; i < ques2OptArr.Length; i++)
                {
                    ques2OptArr[i] = ques2OptArr[i].Trim();
                    if (i < ques2OptArr.Length - 1)
                        optsBttns[i].GetComponentInChildren<Text>().text = ques2OptArr[i];
                }
                currentQuesCrtAns = Convert.ToInt32(ques2OptArr[ques2OptArr.Length - 1].Trim());
                break;
            case 2:
                for (int i = 0; i < ques3OptArr.Length; i++)
                {
                    ques3OptArr[i] = ques3OptArr[i].Trim();
                    if (i < ques3OptArr.Length - 1)
                        optsBttns[i].GetComponentInChildren<Text>().text = ques3OptArr[i];
                }
                currentQuesCrtAns = Convert.ToInt32(ques3OptArr[ques3OptArr.Length - 1].Trim());
                break;
        }*/

    }

    public void OptionBttnClicked(int optVal)
    {
        if(optVal == currentQuesCrtAns)
        {
            isAnsCrt = true;
        }
        else
        {
            isAnsCrt = false;
        }
        Act_DeAct_Button(false); // To button interactable false/true
        ShowResult(optVal);
    }


    void Act_DeAct_Button(bool Val)
    {
        /*if (Val)
        {*/
            for (int i = 0; i < optsBttns.Length; i++)
            {
                optsBttns[i].interactable = Val;
            }
        /*}
        else
        {
            for (int i = 0; i < optsBttns.Length; i++)
            {
                optsBttns[i].interactable = Val;
            }
        }*/
        
    }
    
    void ShowResult(int optVal)
    {
        if (isAnsCrt)
        {
            optsBttns[optVal - 1].image.color = Color.green;
            optsBttns[optVal - 1].gameObject.GetComponentInChildren<Text>().color = Color.white;

            /*resultTxt.text = "Correct";
            resultTxt.color = Color.green;*/

            kit1StatusHandlerScriptAccess.RightAnswerClick();
        }
        else
        {
            optsBttns[optVal - 1].image.color = Color.red;
            optsBttns[optVal - 1].gameObject.GetComponentInChildren<Text>().color = Color.white;

            /*resultTxt.text = "Wrong";
            resultTxt.color = Color.red;*/

            kit1StatusHandlerScriptAccess.WrongAnswerClick();
        }
        
        //Invoke("HideResult", 1f);
    }
    void HideResult()
    {
        resultTxt.text = "";
        Act_DeAct_Button(true);
        //ShowNextQuestion();
        //CloseQuestionPanel();
        
    }

    public void CloseQuestionPanel()
    {
        quesPanel.SetActive(false);

        //circuitsUiHandlerScriptAccess.ReloadBttnClick();

        Act_DeAct_Button(true);

        ColorUtility.TryParseHtmlString("#323232", out Color initColor);

        for (int i = 0; i < optsBttns.Length; i++)
        {
            optsBttns[i].image.color = Color.white;
            
            optsBttns[i].gameObject.GetComponentInChildren<Text>().color = initColor; //Color.black;
        }
    }

    void ShowNextQuestion()
    {
        currentQuesNum++;

        if (currentQuesNum < kit1QuesEeachLine.Count)
        {
            //Regex replace multiple white spaces to single space and trim removes front and end white spaces.
            questionTxt.text = Regex.Replace(kit1QuesEeachLine[currentQuesNum], @"\s+", " ").Trim();
            PushAnswers(currentQuesNum);
        }
        else
        {
            resultTxt.text = "Finished";
            resultTxt.color = Color.green;
            Act_DeAct_Button(false);

        }
        
    }
}


/*[System.Serializable]
public class Questions
{
    public bool isMCQ;
    public bool MCQAnswerIndex;
    public QuestionsOptList questionsOptList;
    
}*/

[System.Serializable]
public class QuestionsOptList
{
    public int questionAnswerVal;
    public List<string> quesOptsArr= new List<string>();
    /*public string quesOpts2;
    public string quesOpts3;
    public string quesOpts4;*/
}





