﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;
using DG.Tweening;
using TMPro;
using System;

public class ProfileHandler : MonoBehaviour
{
    //public Text userNameTxt, userMobNumberTxt;
    public TMP_Text userNameTxt, userMobNumberTxt, userEmailIdTxt, welcomeUserNameTxt, welcomeNinotsScoreTxt, rewardsNinotsScoreTxt, hamUserNameTxt;

    private string loggedInUserNumber, loggedInUserName, loggedInUserEmailId;
    public int ninotsScore;

    public string userParentKeyInDB;

    string appName, appLink;

    public bool gotParentKey, fetchUserDetails;

    //public string kit1ActiveStatus;

    //For script access
    LocationHandler locationHandle;
    PopUpHandler popUpScriptAccess;
    ToastMessage toastAccess;
    ZohoCRM zohoCrmScriptAccess;
    VersionUpdateNotification versionUpdateScriptAccess;

    // Start is called before the first frame update
    void Start()
    {
        /*//Panel Anchor Pos
        duration = 0.25f;

        centerAnchorPos.Set(0f, 0f);
        leftAnchorPos.Set(-800f, 0f);
        rightAnchorPos.Set(800f, 0f);
        /*topAnchorPos.Set(0, 1280f);
        bottomAnchorPos.Set(0, -1280f);*/

        locationHandle = FindObjectOfType<LocationHandler>() as LocationHandler;
        popUpScriptAccess = FindObjectOfType<PopUpHandler>() as PopUpHandler;
        toastAccess = FindObjectOfType<ToastMessage>();
        zohoCrmScriptAccess = FindObjectOfType<ZohoCRM>() as ZohoCRM;
        versionUpdateScriptAccess = FindObjectOfType<VersionUpdateNotification>() as VersionUpdateNotification;

        appName = "Play Nino";
        appLink = "https://play.google.com/store/apps/details?id=com.evobi.playnino&hl=en";

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://playnino-ca8a5.firebaseio.com/");

        // Get the root reference location of the database.
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

        loggedInUserNumber = PlayerPrefs.GetString("UserLoggedInNumber");

        gotParentKey = false;

        LoggedInUserDbParentNumFetch();

        //Rewards Panel
        userMobNumberTxt.text = loggedInUserNumber;

        if (PlayerPrefs.GetString("LoggingIn") == "true" && PlayerPrefs.GetString("DetailsFetched") != "true")
        {
            fetchUserDetails = true;
            FetchUserDetails();

            
        }
        else if(PlayerPrefs.GetString("LoggingIn") != "true")
        {
            loggedInUserName = PlayerPrefs.GetString("UserLoggedInName");
            loggedInUserEmailId = PlayerPrefs.GetString("UserLoggedInEmailId");
            zohoCrmScriptAccess.userLeadRecordID = PlayerPrefs.GetString("userLeadRecordID");
            //kit1ActiveStatus = PlayerPrefs.GetString("UserKit1ActiveStatus");

            //For Welcome panel
            welcomeUserNameTxt.text = loggedInUserName;

            //For Hamburger Panel
            hamUserNameTxt.text = loggedInUserName;

            //For Rewards Panel
            
            userNameTxt.text = loggedInUserName;
            userEmailIdTxt.text = loggedInUserEmailId;

            //ninotsScore = PlayerPrefs.GetInt("TotalArScore");
            ninotsScore = PlayerPrefs.GetInt("TotalScore");
            welcomeNinotsScoreTxt.text = ninotsScore.ToString();
            rewardsNinotsScoreTxt.text = ninotsScore.ToString();
        }
        else if(PlayerPrefs.GetString("DetailsFetched") == "true")
        {
            loggedInUserName = PlayerPrefs.GetString("UserLoggedInName");
            loggedInUserEmailId = PlayerPrefs.GetString("UserLoggedInEmailId");
            zohoCrmScriptAccess.userLeadRecordID = PlayerPrefs.GetString("userLeadRecordID");
            //kit1ActiveStatus = PlayerPrefs.GetString("UserKit1ActiveStatus");

            //For Welcome panel
            welcomeUserNameTxt.text = loggedInUserName;

            //For Hamburger Panel
            hamUserNameTxt.text = loggedInUserName;

            //For Rewards Panel

            userNameTxt.text = loggedInUserName;
            userEmailIdTxt.text = loggedInUserEmailId;
            //ninotsScore = PlayerPrefs.GetInt("TotalScore");
            welcomeNinotsScoreTxt.text = ninotsScore.ToString();
            rewardsNinotsScoreTxt.text = ninotsScore.ToString();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
        ninotsScore = PlayerPrefs.GetInt("TotalScore");

        welcomeNinotsScoreTxt.text = ninotsScore.ToString();
        rewardsNinotsScoreTxt.text = ninotsScore.ToString();
    }

    void FirebaeOneTimeRetriveData()
    {
        //To fetch the user name
        
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Users");

        reference.OrderByChild("phoneNumber").EqualTo(loggedInUserNumber).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                // Do something with snapshot...

                //Search for parent name
                foreach (DataSnapshot myChild in snapshot.Children)
                {
                    //parent name
                    string parentKey = myChild.Key;
                    //String value = myChild.Value.ToString();

                    //get the child value
                    loggedInUserName = reference.Child(parentKey).Child("userName").GetValueAsync().Result.Value.ToString();
                    userNameTxt.text = loggedInUserName;
                    PlayerPrefs.SetString("UserLoggedInName", loggedInUserName);
                    PlayerPrefs.Save();

                }

            }
        });
    }

    void LoggedInUserDbParentNumFetch()
    {
        //To fetch the user parent name

        
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Users");

        reference.OrderByChild("phoneNumber").EqualTo(loggedInUserNumber).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...

                Debug.Log("Parent Fetch Failed");
                if (!gotParentKey)
                {
                    Invoke("LoggedInUserDbNum", 2);
                }
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;

                //Search for parent name
                foreach (DataSnapshot myChild in snapshot.Children)
                {
                    //parent name
                    string parentKey = myChild.Key;
                    
                    PlayerPrefs.SetString("UserLoggedInParentName", parentKey);
                    PlayerPrefs.Save();

                    userParentKeyInDB = parentKey;
                    //userParentKeyInDB = PlayerPrefs.GetString("UserLoggedInParentName");

                    gotParentKey = true;
                }

            }
        });
    }

    public void FetchUserDetails()
    {
        //To fetch the user details one time
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.GetReference("Users");
        int scoreFromDB;

        if (gotParentKey && fetchUserDetails)
        {
            reference.Child(userParentKeyInDB).GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...

                    Invoke("FetchUserDetails", 2);
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    // Do something with snapshot...

                    loggedInUserName = snapshot.Child("userName").Value.ToString();
                    loggedInUserEmailId = snapshot.Child("emailID").Value.ToString();
                     
                    string tempLeadRecordId = snapshot.Child("CRM").Child("leadRecordId").Value.ToString();
                    zohoCrmScriptAccess.userLeadRecordID = tempLeadRecordId;

                    /*if (snapshot.Child("KitActivation").Child("Starterkit1").Child("activeStatus").Exists)
                    {
                        kit1ActiveStatus = snapshot.Child("KitActivation").Child("Starterkit1").Child("activeStatus").Value.ToString();
                    }
                    else
                    {
                        //To set kit active status
                        reference.Child(userParentKeyInDB).Child("KitActivation").Child("Starterkit1").Child("activeStatus").SetValueAsync("false");
                        kit1ActiveStatus = "false";
                    }*/

                    if (snapshot.Child("TotalScore").Exists)
                    {
                        string tempString = snapshot.Child("TotalScore").Value.ToString();
                        scoreFromDB = Convert.ToInt32(tempString);
                        ninotsScore = scoreFromDB;
                    }
                    else
                    {
                        reference.Child(userParentKeyInDB).Child("TotalScore").SetValueAsync(0);
                        scoreFromDB = 0;
                        ninotsScore = scoreFromDB;
                    }
                    

                    welcomeUserNameTxt.text = loggedInUserName;
                    hamUserNameTxt.text = loggedInUserName;
                    welcomeNinotsScoreTxt.text = ninotsScore.ToString();
                    rewardsNinotsScoreTxt.text = ninotsScore.ToString();

                    userNameTxt.text = loggedInUserName;
                    userEmailIdTxt.text = loggedInUserEmailId;

                    popUpScriptAccess.emailInputTxt.text = loggedInUserEmailId;

                    if (snapshot.Child("Level").Exists)
                    {
                        string levelString = snapshot.Child("Level").Value.ToString();
                        int level = Convert.ToInt32(levelString);
                        PlayerPrefs.SetInt("currentUnlockedPoint", level);
                        PlayerPrefs.Save();
                    }
                    else
                    {
                        reference.Child(userParentKeyInDB).Child("Level").SetValueAsync(0);
                        PlayerPrefs.SetInt("currentUnlockedPoint", 0);
                        PlayerPrefs.Save();
                    }
                    

                    /*if (snapshot.Child("KitActivation").Child("Starterkit1").Child("currentProject").Exists)
                    {
                        string currentProjectString = snapshot.Child("KitActivation").Child("Starterkit1").Child("currentProject").Value.ToString();
                        int cProject = Convert.ToInt32(currentProjectString);
                        PlayerPrefs.SetInt("currentProject", cProject);
                        PlayerPrefs.Save();
                    }
                    else
                    {

                    }*/
                    

                    //To save ar score in local
                    //PlayerPrefs.SetInt("TotalArScore", scoreFromDB);
                    PlayerPrefs.SetInt("TotalScore", scoreFromDB);
                    //To save logged in user name
                    PlayerPrefs.SetString("UserLoggedInName", loggedInUserName);
                    //To save logged in user EmailId
                    PlayerPrefs.SetString("UserLoggedInEmailId", loggedInUserEmailId);
                    //To save crm lead record id
                    PlayerPrefs.SetString("userLeadRecordID", tempLeadRecordId);

                    //To save kit active status
                    //PlayerPrefs.SetString("UserKit1ActiveStatus", kit1ActiveStatus);

                    PlayerPrefs.Save();

                    /*string tempStringForScannedAr;
                    int tempValue;
                    tempStringForScannedAr = snapshot.Child("AR").Child("ScannedAR").Child("ship").Value.ToString();
                    tempValue = Convert.ToInt32(tempStringForScannedAr);
                    if (tempValue == 1)
                    {
                        PlayerPrefs.SetInt("ShipTrackedState", 1);
                        PlayerPrefs.Save();
                    }

                    tempStringForScannedAr = snapshot.Child("AR").Child("ScannedAR").Child("plane").Value.ToString();
                    tempValue = Convert.ToInt32(tempStringForScannedAr);
                    if (tempValue == 1)
                    {
                        PlayerPrefs.SetInt("PlaneTrackedState", 1);
                        PlayerPrefs.Save();
                    }

                    tempStringForScannedAr = snapshot.Child("AR").Child("ScannedAR").Child("brain").Value.ToString();
                    tempValue = Convert.ToInt32(tempStringForScannedAr);
                    if (tempValue == 1)
                    {
                        PlayerPrefs.SetInt("BrainTrackedState", 1);
                        PlayerPrefs.Save();
                    }*/

                    /*if (versionUpdateScriptAccess.isAppUpdated)
                    {
                        //App version Set
                        PlayerPrefs.SetString("SavedAppVersion", Application.version);
                        PlayerPrefs.Save();

                        versionUpdateScriptAccess.isAppUpdated = false;
                    }*/

                    PlayerPrefs.SetString("DetailsFetched", "true");
                    PlayerPrefs.Save();
                    fetchUserDetails = false;
                    

                   /* foreach (DataSnapshot myChild in snapshot.Children)
                    {
                        toastAccess.showToast("fetching details", 1);
                        string parentKey = myChild.Key;

                        loggedInUserName = reference.Child(parentKey).Child("userName").GetValueAsync().Result.Value.ToString();
                        loggedInUserEmailId = reference.Child(parentKey).Child("emailId").GetValueAsync().Result.Value.ToString();

                        string tempString = reference.Child(userParentKeyInDB).Child("score").GetValueAsync().Result.Value.ToString();
                        ninotsScore = Convert.ToInt32(tempString);

                        welcomeNinotsScoreTxt.text = ninotsScore.ToString();
                        rewardsNinotsScoreTxt.text = ninotsScore.ToString();

                        userNameTxt.text = loggedInUserName;
                        userEmailIdTxt.text = loggedInUserEmailId;

                        popUpScriptAccess.emailInputTxt.text = loggedInUserEmailId;

                        //To save ar score in local
                        PlayerPrefs.SetInt("TotalArScore", ninotsScore);
                        //To save logged in user name
                        PlayerPrefs.SetString("UserLoggedInName", loggedInUserName);
                        //To save logged in user EmailId
                        PlayerPrefs.SetString("UserLoggedInEmailId", loggedInUserEmailId);

                        PlayerPrefs.SetString("DetailsFetched", "true");
                        PlayerPrefs.Save();

                        Debug.Log("detailFetched");

                        toastAccess.showToast("detailFetched", 1);
                    }*/
                        

                }
            });
        }
        else if(fetchUserDetails && !gotParentKey)
        {
            Invoke("FetchUserDetails", 1f);
        }
    }

    /*public void UpdateArScoreInDB()
    {

        Debug.Log("DB");
        if (gotParentKey)
        {

            Invoke("UpdateScoreInDB", 1f);
        }
        else
        {
            Invoke("UpdateArScoreInDB", 5f);
        }


    }*/

    public void UpdateTotalScoreInDB()///this function will be called from the above function. this is only for making a delay.
    {
        if (gotParentKey)
        {
            DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
            mDatabaseRef.Child("Users").Child(userParentKeyInDB).Child("TotalScore").SetValueAsync(PlayerPrefs.GetInt("TotalScore"));
        }
        else
        {
            Invoke("UpdateTotalScoreInDB", 2f);
        }
    }
    public void SaveLevelInDB()
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        if (gotParentKey)
        {
            int level = PlayerPrefs.GetInt("currentUnlockedPoint");
            mDatabaseRef.Child("Users").Child(userParentKeyInDB).Child("Level").SetValueAsync(level);

        }
        else
        {
            Invoke("SaveLevelInDB", 2f);
        }
    }
    /*public void ChangeCurrentProjectInDB(int value)
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        if (gotParentKey)
        {
            mDatabaseRef.Child("Users").Child(userParentKeyInDB).Child("KitActivation").Child("Starterkit1").Child("currentProject").SetValueAsync(value);

        }
        else
        {
            Invoke("UpdateArScannedTargetShip", 2f);
        }
    }

    public void UpdateArScannedTargetShip()
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        if (gotParentKey)
        {
            mDatabaseRef.Child("Users").Child(userParentKeyInDB).Child("AR").Child("ScannedAR").Child("ship").SetValueAsync(1);
            zohoCrmScriptAccess.UpdateLeadInCRM("'Yacht':true");
        }
        else
        {
            Invoke("UpdateArScannedTargetShip", 2f);
        }
    }
    public void UpdateArScannedTargetPlane()
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        if (gotParentKey)
        {
            mDatabaseRef.Child("Users").Child(userParentKeyInDB).Child("AR").Child("ScannedAR").Child("plane").SetValueAsync(1);
            zohoCrmScriptAccess.UpdateLeadInCRM("'Plane':true");
        }
        else
        {
            Invoke("UpdateArScannedTargetPlane", 2f);
        }
    }
    public void UpdateArScannedTargetBrain()
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        if (gotParentKey)
        {
            mDatabaseRef.Child("Users").Child(userParentKeyInDB).Child("AR").Child("ScannedAR").Child("brain").SetValueAsync(1);
            zohoCrmScriptAccess.UpdateLeadInCRM("'Brain':true");
        }
        else
        {
            Invoke("UpdateArScannedTargetBrain", 2f);
        }
    }*/
    public void UpdateLocationInDB()
    {
        DatabaseReference mDatabaseRef = FirebaseDatabase.DefaultInstance.RootReference;

        // gotParentKey = true;///testing.........
        if (gotParentKey)
        {
            string temp = locationHandle.latitude+ "," + locationHandle.longitude;
            mDatabaseRef.Child("Users").Child(userParentKeyInDB).Child("location").SetValueAsync(temp);
          //mDatabaseRef.Child("Users").Child("2").Child("location").SetValueAsync("gotkey");//testing.......
        }
        else
        {
            Invoke("UpdateLocationInDB", 3f);
        }



    }
   
}
