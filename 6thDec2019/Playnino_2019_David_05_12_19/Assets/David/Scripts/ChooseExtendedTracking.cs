﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ChooseExtendedTracking : MonoBehaviour
{
    PositionalDeviceTracker deviceTracker;
    bool vuforiaInitialized;
    public bool isExtended;

    void Awake()
    {
        VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaInitialized);
    }
    private void Update()
    {
        deviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();
        if (deviceTracker.IsActive)
        {
            isExtended = true;
        }
        else
        {
            isExtended = false;
        }
    }
    void OnVuforiaInitialized()
    {
        vuforiaInitialized = true;
        //StartCoroutine(StopTracker());
    }
    public void EnableExtendedTrack()
    {
        if (vuforiaInitialized)
            StartCoroutine(StartTracker());
    }
    private IEnumerator StartTracker()
    {
        deviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();

        while (deviceTracker.IsActive == true)
            yield return new WaitForEndOfFrame();
        deviceTracker.Start();
    }

    public void DisableExtendedTrack()
    {
        if (vuforiaInitialized)
            StartCoroutine(StopTracker());
    }

    private IEnumerator StopTracker()
    {
        deviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();

        while (deviceTracker.IsActive == false)
            yield return new WaitForEndOfFrame();
        deviceTracker.Stop();
    }
}
