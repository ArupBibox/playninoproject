﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRotation : MonoBehaviour
{
    public float rotSpeed = 180f;
    public GameObject platform;
    // Start is called before the first frame update
    void Start()
    {
        platform.GetComponent<rotate>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDrag()
    {
        platform.GetComponent<rotate>().enabled = false;
        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
        // transform.RotateAround(Vector3.right, rotY);
          transform.Rotate(Vector3.up, -rotX, Space.Self);
        //  transform.RotateAroundLocal(Vector3.right, -rotX);
       // transform.Rotate( Vector3.up, 20 * Time.deltaTime);
    }
}
