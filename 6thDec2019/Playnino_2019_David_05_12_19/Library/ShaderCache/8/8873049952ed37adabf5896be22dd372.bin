�A                       2  #ifdef VERTEX
#version 300 es

uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _Size;
uniform 	vec4 _SunDir;
in highp vec4 in_POSITION0;
in highp vec4 in_TANGENT0;
in highp vec3 in_NORMAL0;
out highp vec2 vs_TEXCOORD1;
out highp vec2 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
vec4 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
float u_xlat9;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat0;
    vs_TEXCOORD5.y = u_xlat0.z;
#ifdef UNITY_ADRENO_ES3
    vs_TEXCOORD5.y = min(max(vs_TEXCOORD5.y, 0.0), 1.0);
#else
    vs_TEXCOORD5.y = clamp(vs_TEXCOORD5.y, 0.0, 1.0);
#endif
    u_xlat0.xy = in_POSITION0.xz / _Size.xz;
    vs_TEXCOORD1.xy = u_xlat0.xy * vec2(5.0, 5.0);
    vs_TEXCOORD5.x = in_TANGENT0.w;
    u_xlat0.x = dot(in_NORMAL0.xyz, in_NORMAL0.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * in_NORMAL0.zxy;
    u_xlat9 = dot(in_TANGENT0.xyz, in_TANGENT0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat1.xyz = vec3(u_xlat9) * in_TANGENT0.yzx;
    u_xlat2.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.zxy * u_xlat1.yzx + (-u_xlat2.xyz);
    u_xlat1.xyz = _WorldSpaceCameraPos.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * _WorldSpaceCameraPos.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * _WorldSpaceCameraPos.zzz + u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat1.xyz = u_xlat1.xyz + (-in_POSITION0.xyz);
    vs_TEXCOORD2.y = dot(u_xlat0.xyz, u_xlat1.xyz);
    vs_TEXCOORD4.y = dot(u_xlat0.xyz, _SunDir.xyz);
    vs_TEXCOORD2.x = dot(in_TANGENT0.xyz, u_xlat1.xyz);
    vs_TEXCOORD2.z = dot(in_NORMAL0.xyz, u_xlat1.xyz);
    vs_TEXCOORD3.xyz = in_NORMAL0.xyz;
    vs_TEXCOORD4.x = dot(in_TANGENT0.xyz, _SunDir.xyz);
    vs_TEXCOORD4.z = dot(in_NORMAL0.xyz, _SunDir.xyz);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
uniform 	vec4 _Time;
uniform 	vec4 _SinTime;
uniform 	mediump vec4 _SurfaceColorLod1;
uniform 	mediump vec4 _WaterColorLod1;
uniform mediump sampler2D _Bump;
uniform mediump sampler2D _WaterTex;
uniform mediump sampler2D _Foam;
in highp vec2 vs_TEXCOORD1;
in highp vec2 vs_TEXCOORD5;
in highp vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD4;
layout(location = 0) out mediump vec4 SV_Target0;
vec4 u_xlat0;
mediump vec3 u_xlat16_0;
vec2 u_xlat1;
mediump vec3 u_xlat16_1;
mediump vec3 u_xlat16_2;
mediump vec2 u_xlat16_3;
mediump vec3 u_xlat16_4;
float u_xlat15;
mediump float u_xlat16_15;
float u_xlat16;
mediump float u_xlat16_17;
void main()
{
    u_xlat0.xy = _Time.xy * vec2(0.0299999993, 0.0399999991) + vs_TEXCOORD1.xx;
    u_xlat0.zw = _SinTime.xy * vec2(0.200000003, 0.5) + vs_TEXCOORD1.yy;
    u_xlat16_1.xyz = texture(_Bump, u_xlat0.xz).xyz;
    u_xlat16_0.xyz = texture(_Bump, u_xlat0.yw).xyz;
    u_xlat16_1.xyz = u_xlat16_1.xyz * vec3(2.0, 2.0, 2.0) + vec3(-1.0, -1.0, -1.0);
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(2.0, 2.0, 2.0) + u_xlat16_1.xyz;
    u_xlat16_2.xyz = u_xlat16_0.xyz + vec3(-1.0, -1.0, -1.0);
    u_xlat16_17 = dot(u_xlat16_2.xyz, u_xlat16_2.xyz);
    u_xlat16_17 = inversesqrt(u_xlat16_17);
    u_xlat16_2.xyz = vec3(u_xlat16_17) * u_xlat16_2.xyz;
    u_xlat0.x = dot(vs_TEXCOORD2.xyz, vs_TEXCOORD2.xyz);
    u_xlat0.x = inversesqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * vs_TEXCOORD2.xyz;
    u_xlat16_17 = dot(u_xlat16_2.xyz, u_xlat0.xyz);
    u_xlat16_15 = (-u_xlat16_17) + 1.0;
    u_xlat16_15 = u_xlat16_15 * u_xlat16_15;
    u_xlat16_15 = u_xlat16_15 * u_xlat16_15;
    u_xlat16_15 = u_xlat16_15 * 0.939999998 + 0.0599999987;
    u_xlat1.x = (-_Time.y) * 0.0500000007 + vs_TEXCOORD1.x;
    u_xlat1.y = vs_TEXCOORD1.y;
    u_xlat16_3.xy = u_xlat1.xy + u_xlat1.xy;
    u_xlat16_1.xyz = texture(_WaterTex, u_xlat16_3.xy).xyz;
    u_xlat16_4.xyz = u_xlat16_1.xyz * _WaterColorLod1.xyz;
    u_xlat16_1.xyz = (-u_xlat16_1.xyz) * _WaterColorLod1.xyz + _SurfaceColorLod1.xyz;
    u_xlat16_1.xyz = vec3(u_xlat16_15) * u_xlat16_1.xyz + u_xlat16_4.xyz;
    u_xlat16_15 = texture(_Foam, vs_TEXCOORD1.xy).x;
    u_xlat16_15 = u_xlat16_15 + -0.5;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_15 = min(max(u_xlat16_15, 0.0), 1.0);
#else
    u_xlat16_15 = clamp(u_xlat16_15, 0.0, 1.0);
#endif
    u_xlat16 = vs_TEXCOORD5.x * 1.79999995;
    u_xlat16_17 = u_xlat16_15 * u_xlat16;
#ifdef UNITY_ADRENO_ES3
    u_xlat16_17 = min(max(u_xlat16_17, 0.0), 1.0);
#else
    u_xlat16_17 = clamp(u_xlat16_17, 0.0, 1.0);
#endif
    u_xlat16_1.xyz = u_xlat16_1.xyz + vec3(u_xlat16_17);
    u_xlat15 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = (-vs_TEXCOORD4.xyz) * vec3(u_xlat15) + u_xlat0.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat16_2.xyz);
    u_xlat0.x = max(u_xlat0.x, 0.0);
    u_xlat0.x = log2(u_xlat0.x);
    u_xlat0.x = u_xlat0.x * 250.0;
    u_xlat0.x = exp2(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx + u_xlat16_1.xyz;
    SV_Target0.xyz = u_xlat0.xyz;
    SV_Target0.w = 0.800000012;
    return;
}

#endif
                                